# GPU Primality Testing

This project was spawned out of my interest for maths, specifically primes.
Also GPU computing.

This project uses C# and the ComputeSharp library, which allows for code execution in DX 12 shaders in C#. Therefore we can utilize the GPU for calculating a huge amount of prime numbers really fast.

## Running the application

### Pre-requisites:

- Visual Studio 2022 (might be able to downgrade or upgrade as well)
- .NET 6

### Steps:

1. Clone the project
2. Open the project with Visual Studio 2022
3. Debug the application.
