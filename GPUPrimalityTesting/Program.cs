﻿using ComputeSharp;
using GPUPrimalityTesting;
using System.Runtime.InteropServices;
using System.Text;

int divider = ushort.MaxValue + 1;
int step = (int)(((long)int.MaxValue + 1) / divider);

Console.WriteLine("Allocating memory...");

string[] strings = Enumerable.Range(0, divider).Select(s => "").ToArray();

Console.WriteLine("Writing primes...");

Parallel.For(0, divider, i => {
    int[] array = Enumerable.Range(i * step, step).ToArray();
    using ReadWriteBuffer<int> buffer = Gpu.Default.AllocateReadWriteBuffer(array);
    Gpu.Default.For(buffer.Length, new PrimalityTester(buffer));
    buffer.CopyTo(array);

    for (int j = 0; j < array.Length; j++) {
        if (array[j] == 1)
            strings[i] += $"{(i * step) + j}\n";
    }
});

Console.WriteLine("Writing primes to a file...");

using StreamWriter sw = new("primes.txt");

foreach (string s in strings) {
    sw.Write(s);
}

sw.Flush();

Console.WriteLine("Primes written!");
Console.ReadLine();
