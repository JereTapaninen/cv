﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Net.Login {
    public enum LoginResponse {
        TryAgainCountFailures = -1,
        Continue = 0,
        TryAgain = 1,
        SuccessfulLogin = 2,
        InvalidUsernameOrPassword = 3,
        AccountDisabled = 4,
        AccountLoggedIn = 5,
        ServerUpdated = 6,
        WorldFull = 7,
        ServerOffline = 8,
        LoginLimitExceeded = 9,
        BadSessionId = 10,
        RejectedSession = 11,
        MembersRequired = 12,
        MiscError = 13,
        ServerBeingUpdated = 14,
        PlayerLoggedInTryAgain = 15,
        LoginAttemptsExceeded = 16,
        InMembersAreaAsFree = 17,
        InvalidServer = 20,
        HopTimeout = 21,
        Unexpected = 22 // or anything, really
    }
}
