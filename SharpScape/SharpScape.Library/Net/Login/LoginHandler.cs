﻿using System;
using System.Net.Sockets;
using SharpScape.Library.Common;
using SharpScape.Library.Common.Exceptions;
using System.Numerics;
using System.IO;
using SharpScape.Library.Game.Entity;
using ISAAC.Library;
using System.Linq;
using SharpScape.Library.Game;
using System.Collections.Generic;

namespace SharpScape.Library.Net.Login {
    public class LoginHandler {
        private const int LoginPacketId = 14;
        private const int SeedBitshiftAmount = 32;
        private const int EncoderSeedOffset = 50;

        private readonly BigInteger RSA_MODULUS = BigInteger.Parse("104491769878744214552327916539299463496996457081116392641740420337580247359457531212713234798435269852381858199895582444770363103378419890508986198319599243102737368616946490728678876018327788000439596635223141886089230154991381365099178986572201859664528128354742213167942196819984139030533812106754541601427");
        private readonly BigInteger RSA_EXPONENT = BigInteger.Parse("60599709927999604905700283074075621535832298407602909067050214324591452792136683802017251754259849192812146824871924219308664736457374966789226657090366163438668361486273661119889727044594679526697477360216141989053223520684400208813776138325305181882593010856241272041142533590779910844335986101437613222337");

        private World World { get; }

        public LoginHandler(World world)
            => World = world;

        public Either<Player, LoginException> Login(TcpClient client) {
            var loginLogger = new Logger("LoginHandler", ConsoleColor.Yellow);
            loginLogger.WriteLine($"Logging new player {client.Client.RemoteEndPoint} in...");

            var stream = client.GetStream();

            Either<Player, LoginException> authenticateAndTryLogin(MemoryStream packetStream) {
                var clientSeed = packetStream.ReadLongLE();
                var seedReceived = packetStream.ReadLongLE();

                int[] decoderSeed = CreateDecoderSeed(clientSeed, seedReceived);
                IsaacCipher decodingCipher = new(decoderSeed);
                int[] encoderSeed = CreateEncoderSeed(decoderSeed);
                IsaacCipher encodingCipher = new(encoderSeed);

                _ = packetStream.ReadIntLE(); // user id?

                var username = packetStream.ReadRSString();
                var password = packetStream.ReadRSString();

                /*var collection = World.Database.GetCollection<LoginDetail>("LoginDetail");
                var matchingDocument = collection
                    .AsQueryable()
                    .ToList()
                    .FirstOrDefault(collection => collection.Username.Equals(username, StringComparison.OrdinalIgnoreCase));

                 if (matchingDocument != default(LoginDetail)) {
                    if (!matchingDocument.ComparePassword(password)) {
                        stream.WriteByte((byte)LoginResponse.InvalidUsernameOrPassword);
                        return new LoginException($"User {username} tried to log in with invalid password!");
                    }
                } else {
                    new LoginDetail(World.Database, username, password)
                        .SaveSync();
                }*/

                stream.WriteByte((byte)LoginResponse.SuccessfulLogin);
                stream.WriteByte(2);

                return Player.Create(
                    World,
                    client,
                    username,
                    decodingCipher,
                    encodingCipher
                );
            }

            Either<Player, LoginException> decodeAuthenticationBlockAndTryAuthenticate() {
                var bufferLength = stream.ReadByte();
                List<byte> cryptedBuffer = new();
                for (var bufferIndex = 0; bufferIndex < bufferLength; bufferIndex++) {
                    cryptedBuffer.Add((byte)stream.ReadByte());
                }

                var encodedBigInt = new BigInteger(cryptedBuffer.SwitchEndianness().ToArray());
                var decodedBigInt = BigInteger.ModPow(encodedBigInt, RSA_EXPONENT, RSA_MODULUS);

                using var packetStream = new MemoryStream(decodedBigInt.ToByteArray().SwitchEndianness().ToArray());

                var securityNumber = packetStream.ReadByte();
                return securityNumber.Validate(
                    value => value == 10,
                    () => authenticateAndTryLogin(packetStream),
                    value => new LoginException($"Invalid security number: {value}")
                );
            }

            Either<Player, LoginException> readClientInfoAndTryDecodeAuthenticationBlock() {
                _ = stream.ReadShortLE(); // client version
                _ = stream.ReadByte(); // low memory mode ?

                return decodeAuthenticationBlockAndTryAuthenticate();
            }

            Either<Player, LoginException> validateLoginSequenceAndTryReadClientInfo() {
                _ = stream.ReadByte(); // login packet size

                var expected255 = stream.ReadByte();
                return expected255.Validate(
                    value => value == 255,
                    readClientInfoAndTryDecodeAuthenticationBlock,
                    value => new LoginException($"Expected 255 in login sequence, instead got {expected255}")
                );
            }

            Either<Player, LoginException> continueLogin() {
                long serverSessionKey = TrueRandom.Next();
                var sessionKey = LEBitConverter.GetBytes(serverSessionKey);

                stream.WriteByte((byte)LoginResponse.Continue);
                stream.Write(sessionKey);

                var connectStatus = (ConnectStatus)stream.ReadByte();
                return connectStatus
                    .Validate(
                        value => value == ConnectStatus.Connect || value == ConnectStatus.Reconnect,
                        validateLoginSequenceAndTryReadClientInfo,
                        value => new LoginException($"Unhandled connect status in login sequence: {value}")
                    );
            }

            var loginPacketId = stream.ReadByte();
            return loginPacketId.Validate(
                value => value == LoginPacketId,
                continueLogin,
                value => new LoginException($"Expected {LoginPacketId} as loginPacketId, instead got {value}")
            );
        }

        private static int[] CreateEncoderSeed(int[] decoderSeed)
            => decoderSeed.Select(value => value + EncoderSeedOffset).ToArray();

        private static int[] CreateDecoderSeed(long clientSeed, long serverSeed)
            => new int[] {
                (int)(clientSeed >> SeedBitshiftAmount),
                (int)clientSeed,
                (int)(serverSeed >> SeedBitshiftAmount),
                (int)serverSeed
            };
    }
}
