﻿using BCryptor = BCrypt.Net.BCrypt;

namespace SharpScape.Library.Net.Login {
    public sealed class LoginDetail {
        private const int SaltRounds = 13;

        public string Id { get; set; }
        public string Username { get; set; }
        public string Hash { get; set; }
        public string Salt { get; set; }

        private LoginDetail(string username, string salt, string hash) {
            Username = username;
            Salt = salt;
            Hash = hash;
        }

        private static string SaltAndHash(string salt, string password)
            => BCryptor.HashPassword(password, salt);

        public bool ComparePassword(string password)
            => Hash == SaltAndHash(Salt, password);

        public static LoginDetail Create(string username, string password) {
            var salt = BCryptor.GenerateSalt(SaltRounds);
            return new(username, salt, SaltAndHash(salt, password));
        }
    }
}
