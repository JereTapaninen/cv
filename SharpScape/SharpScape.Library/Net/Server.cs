﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Diagnostics;
using SharpScape.Library.Net.Packets;
using SharpScape.Library.Common.Exceptions;
using SharpScape.Library.Common;
using SharpScape.Library.Net.Login;
using SharpScape.Library.Game.Entity;
using System.Collections.Concurrent;
using System.Collections.Immutable;
using SharpScape.Library.Game;

namespace SharpScape.Library.Net {
    public sealed class Server {
        private Logger Logger { get; } = new("Server", ConsoleColor.Green);
        private ManualResetEvent ManualReset { get; } = new(false);
        private TcpListener Listener { get; }
        private Task ListeningTask { get; set; }
        private World World { get; }
        private List<Task> Listeners { get; } = new();

        public CancellationTokenSource Cancellation { get; } = new();
        public EndPoint LocalEndPoint => Listener.LocalEndpoint;
        public bool Running => !ListeningTask.IsCompleted && !Listeners.Any(listener => !listener.IsCompleted);

        public Server(ServerConfiguration configuration) {
            Listener = new TcpListener(
                IPAddress.Parse(configuration.IPAddress),
                configuration.Port
            );

            World = new(this);
        }

        public void Start() {
            ListeningTask = ListenForConnections();
        }

        public async Task Stop() {
            Cancellation.Cancel();
            await ListeningTask;
            await World.CleaningTask;
            //await World.Close();
            World.Dispose();
            await Task.WhenAll(Listeners);
            Logger.ClearBacklog();
        }

        private Task ListenForConnections()
            => Task.Run(() => {
                Listener.Start();

                Logger.WriteLine($"Server listening on {LocalEndPoint}");

                while (!Cancellation.IsCancellationRequested) {
                    while (Listener.Pending()) {
                        _ = ManualReset.Reset();

                        _ = Listener.BeginAcceptTcpClient(new(AcceptClient), Listener);

                        _ = ManualReset.WaitOne();
                    }

                    Thread.Sleep(10);
                }

                Listener.Stop();
            });

        private void AcceptClient(IAsyncResult ar) {
            _ = ManualReset.Set();

            var listener = (TcpListener)ar.AsyncState;
            var client = listener.EndAcceptTcpClient(ar);

            Logger.WriteLine($"Connection received from {client.Client.RemoteEndPoint}");

            World.LoginHandler
                .Login(client)
                .Match(
                    player => World.AddPlayer(player),
                    error => {
                        client.Close();
                        Logger.WriteLine($"Error during login: {error.Message}");
                    }
                );
        }
    }
}
