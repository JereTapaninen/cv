﻿using SharpScape.Library.Common;
using SharpScape.Library.Game.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ISAAC.Library;
using SharpScape.Library.Game.Entity.Skill;

namespace SharpScape.Library.Net.Packets {
    public sealed class PacketSender {
        private Player Player { get; }
        private PacketEncoder Encoder { get; }
        private NetworkStream Stream { get; }
        private Logger Logger { get; }

        public PacketSender(Player player, IsaacCipher isaac, NetworkStream stream) {
            Player = player;
            Encoder = new PacketEncoder(isaac);
            Logger = new($"Packet Sender: {player.Username}");
            Stream = stream;
        }

        public PacketSender Send(Packet packet) {
            var encoded = Encoder.Encode(packet);
            Stream.Write(encoded);
            Logger.WriteLine($"Packet {packet.OpCode} sent!");
            return this;
        }

        public PacketSender SendSkill(PlayerSkill skill) {
            var messagePacket = new WritablePacket(PacketType.UpdateSkill)
                .WriteByte((byte)skill.Type)
                .WriteInt(skill.Level)
                .WriteInt(skill.MaxLevel)
                .WriteInt(skill.Experience);
            return Send(messagePacket);
        }

        public PacketSender SendMessage(string message) {
            var messagePacket = new WritablePacket(PacketType.SendMessage)
                .WriteRSString(message);
            return Send(messagePacket);
        }

        public PacketSender SendMapRegion() {
            var messagePacket = new WritablePacket(PacketType.LoadMapRegion)
                .WriteShort(128)
                .WriteShort(0);
            return Send(messagePacket);
        }

        public PacketSender SendTabInterface(byte tabId, short interfaceId) {
            var messagePacket = new WritablePacket(PacketType.SendSidebarInterface)
                .WriteShort(interfaceId)
                .WriteByte((byte)(tabId + 128));
            return Send(messagePacket);
        }

        public PacketSender SendTabs() {
            for (byte tabId = 0; tabId < GameConstants.TabInterfaces.Length; tabId++) {
                var intf = GameConstants.TabInterfaces[tabId];

                if (tabId == 6)
                    intf = 39000;

                _ = SendTabInterface(tabId, (short)intf);
            }

            return this;
        }

        public PacketSender SendDetails() {
            var messagePacket = new WritablePacket(PacketType.InitializePlayer)
                .WriteByte(1 + 128)
                .WriteShort(Player.Index);
            return Send(messagePacket);
        }
    }
}
