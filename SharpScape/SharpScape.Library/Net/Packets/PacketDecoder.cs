﻿using ISAAC.Library;
using SharpScape.Library.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Net.Packets {
    public class PacketDecoder {
        private IsaacCipher ISAAC { get; }

        public PacketDecoder(IsaacCipher isaac)
            => ISAAC = isaac;

        public Packet Decode(IEnumerable<byte> bytes) {
            using var packetStream = new MemoryStream(bytes.ToArray());
            var encryptedOpCode = packetStream.ReadByte();
            var opCode = (encryptedOpCode - ISAAC.getNextKey()) & 0xFF;

            var encryptedSize = packetStream.ReadByte();
            var size = (encryptedSize - ISAAC.getNextKey()) & 0xFF;

            return new Packet((PacketType)opCode, packetStream.ReadToEnd());
        }
    }
}
