﻿using SharpScape.Library.Common;
using SharpScape.Library.Common.Exceptions;
using SharpScape.Library.Game.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Net.Packets {
    public class Packet {
        public PacketType OpCode { get; }
        protected List<byte> Buffer { get; set; } = new();

        public byte[] Data => Buffer.ToArray();

        public Packet(PacketType opCode)
            => OpCode = opCode;

        public Packet(PacketType opCode, IEnumerable<byte> buffer) {
            OpCode = opCode;
            Buffer = buffer.ToList();
        }
    }
}
