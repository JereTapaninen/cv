﻿using SharpScape.Library.Game;
using SharpScape.Library.Game.Entity;
using System;

namespace SharpScape.Library.Net.Packets.PacketHandlers {
    public sealed class SilencedPacketHandler : ReadablePacket {
        public SilencedPacketHandler(Packet basePacket)
            : base(basePacket) { }

        public override void Handle(World world, Player player) { }
    }
}
