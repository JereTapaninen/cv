﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Net.Packets {
    public enum PacketType {
        Idle = 0,
        SendSidebarInterface = 71,
        LoadMapRegion = 73,
        PlayerUpdate = 81,
        UpdateSkill = 134,
        InitializePlayer = 249,
        SendMessage = 253
    }
}
