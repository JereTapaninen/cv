﻿using SharpScape.Library.Common;
using SharpScape.Library.Common.Exceptions;
using SharpScape.Library.Game;
using SharpScape.Library.Game.Entity;
using SharpScape.Library.Net.Packets.PacketHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Net.Packets {
    public abstract class ReadablePacket {
        protected Packet UnderlyingPacket { get; }

        public PacketType OpCode => UnderlyingPacket.OpCode;

        public ReadablePacket(Packet basePacket)
            => UnderlyingPacket = basePacket;

        public abstract void Handle(World world, Player player);

        public static Either<ReadablePacket, PacketException> GetPacketHandler(Packet basePacket)
            => basePacket.OpCode switch {
                PacketType.Idle => new SilencedPacketHandler(basePacket),
                _ => new PacketException($"Unknown packet received: {basePacket.OpCode} ({(int)basePacket.OpCode})"),
            };
    }
}
