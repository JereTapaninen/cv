﻿using SharpScape.Library.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Net.Packets {
    public sealed class WritablePacket : Packet {
        private int BitPositionOffset { get; set; }
        private int BitPosition => (Buffer.Count * 8) + BitPositionOffset;

        private static readonly int[] BIT_MASK = {
            0, 0x1, 0x3, 0x7,
            0xf, 0x1f, 0x3f,
            0x7f, 0xff, 0x1ff,
            0x3ff, 0x7ff, 0xfff,
            0x1fff, 0x3fff, 0x7fff,
            0xffff, 0x1ffff, 0x3ffff,
            0x7ffff, 0xfffff, 0x1fffff,
            0x3fffff, 0x7fffff, 0xffffff,
            0x1ffffff, 0x3ffffff, 0x7ffffff,
            0xfffffff, 0x1fffffff, 0x3fffffff,
            0x7fffffff, -1
        };

        public WritablePacket(PacketType opCode)
            : base(opCode) { }

        public WritablePacket WriteBits(int bits, int value) {
            var bytes = (int)Math.Ceiling(bits / 8D) + 1;
            var newBuffer = Buffer.Concat(new byte[bytes]).ToList();

            var bytePos = BitPosition >> 3;
            var bitOffset = 8 - (BitPosition & 7);
            BitPositionOffset += bits;

            for (; bits > bitOffset; bitOffset = 8) {
                newBuffer[bytePos] &= (byte)~BIT_MASK[bitOffset];
                newBuffer[bytePos++] |= (byte)((value >> (bits - bitOffset)) & BIT_MASK[bitOffset]);
                bits -= bitOffset;
            }

            newBuffer[bytePos] &= (byte)(bits == bitOffset ?
                ~BIT_MASK[bitOffset] :
                ~(BIT_MASK[bits] << (bitOffset - bits)));
            newBuffer[bytePos] |= (byte)(bits == bitOffset ?
                value & BIT_MASK[bitOffset] :
                (value & BIT_MASK[bits]) << (bitOffset - bits));

            Buffer = newBuffer;

            return this;
        }

        public WritablePacket WriteBit(bool flag)
            => WriteBits(1, flag ? 1 : 0);

        public WritablePacket WriteRSString(string message) {
            Buffer.AddRange(Encoding.ASCII.GetBytes(message).Append((byte)10));
            return this;
        }

        public WritablePacket WriteInt(int value) {
            Buffer.AddRange(LEBitConverter.GetBytes(value));
            return this;
        }

        public WritablePacket WriteShort(short value) {
            Buffer.AddRange(LEBitConverter.GetBytes(value));
            return this;
        }

        public WritablePacket WriteByte(byte value) {
            Buffer.Add(value);
            return this;
        }
    }
}
