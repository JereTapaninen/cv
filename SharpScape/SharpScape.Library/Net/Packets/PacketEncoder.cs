﻿using SharpScape.Library.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISAAC.Library;

namespace SharpScape.Library.Net.Packets {
    public sealed class PacketEncoder {
        private IsaacCipher ISAAC { get; }

        public PacketEncoder(IsaacCipher isaac)
            => ISAAC = isaac;

        public byte[] Encode(Packet packet) {
            int encodedOpCode = (int)packet.OpCode + ISAAC.getNextKey();
            var encodedSize = LEBitConverter.GetBytes((short)packet.Data.Length);
            var encodedData = packet.Data;

            return Array.Empty<byte>()
                .Append((byte)encodedOpCode)
                .Concat(encodedSize)
                .Concat(encodedData)
                .ToArray();
        }
    }
}
