﻿using SharpScape.Library.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Game.Entity {
    public abstract class Entity : DatabaseEntity {
        [Key]
        public Guid Id { get; set; }
        public Position Position { get; protected set; }

        protected Entity() { }
    }
}
