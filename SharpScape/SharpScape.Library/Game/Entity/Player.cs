﻿using SharpScape.Library.Common;
using SharpScape.Library.Net.Packets;
using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using ISAAC.Library;
using System.Linq;
using System.Collections.Generic;
using SharpScape.Library.Game.Entity.Skill;
using System.ComponentModel.DataAnnotations.Schema;
using SharpScape.Library.Database;
using Microsoft.EntityFrameworkCore;

namespace SharpScape.Library.Game.Entity {
    public sealed class Player : Entity, IDisposable {
        [NotMapped]
        private Logger Logger { get; set; }
        [NotMapped]
        private World World { get; set; }
        [NotMapped]
        private TcpClient Client { get; set; }
        [NotMapped]
        private PacketDecoder PacketDecoder { get; set; }

        public ICollection<PlayerSkill> Skills { get; set; }
        [NotMapped]
        public PacketSender PacketSender { get; private set; }
        [NotMapped]
        public short Index => World.GetIndexOfPlayer(this);
        public string Username { get; set; }
        [NotMapped]
        public bool Connected => Client.Connected;

        private Player(string username) : base()
            => Username = username;

        public static Player Create(
            World world,
            TcpClient client,
            string username,
            IsaacCipher decodingCipher,
            IsaacCipher encodingCipher
        ) {
            var player = new Player(username) {
                Logger = new($"Player {username}"),
                World = world,
                Client = client,
                PacketDecoder = new(decodingCipher),
            };
            player.PacketSender = new(player, encodingCipher, player.Client.GetStream());

            player.Load();

            return player;
        }

        private Player Load() {
            using var db = new DatabaseContext();
            var player = db.Players.ToList().Find(p => p.Username == Username);

            var loadedPlayer = player.IsDefault() ?
                InitializePlayer() :
                InitializePlayerFromDB(player);

            loadedPlayer.Save();
            loadedPlayer.ListenForInput();
            loadedPlayer.OnLogin();

            return loadedPlayer;
        }

        private Player InitializePlayerFromDB(Player fromDb) {
            Skills = fromDb.Skills;
            Id = fromDb.Id;
            Position = fromDb.Position;
            return this;
        }

        private Player InitializePlayer() {
            PlayerSkill.CreateAll()
                .Select(skill => skill.Initialize(this))
                .TrySelect(skill => skill.SetBothLevels(1, 1))
                .Match(
                    success: skills => Skills = skills.ToList(),
                    failure: Logger.WriteException
                );
            Skills
                .GetSpecific(skill => skill.Type == SkillType.Hitpoints)
                .Match(
                    success: hitpoints => hitpoints.SetBothLevels(10, 10),
                    failure: exception => exception
                ).Match(
                    failure: Logger.WriteException
                );
            return this;
        }

        private void OnLogin()
            => Task.Run(() => {
                _ = PacketSender
                    .SendMapRegion()
                    .SendDetails()
                    .SendTabs()
                    .SendMessage($"Welcome to SharpScape, {Username}!");
                var updatePacket = new WritablePacket(PacketType.PlayerUpdate);
                _ = PacketSender.Send(updatePacket.WriteBit(false));
            });

        private void ListenForInput()
            => Task.Run(() => {
                var stream = Client.GetStream();

                while (!World.Cancellation.IsCancellationRequested) {
                    try {
                        if (stream.DataAvailable) {
                            var data = stream.ReadToEnd();
                            var decodedPacket = PacketDecoder.Decode(data.ToArray());
                            ReadablePacket.GetPacketHandler(decodedPacket)
                                .Match(
                                    packetHandler => packetHandler.Handle(World, this),
                                    error => Logger.WriteLine(error.Message)
                                );
                        }
                    } catch (ObjectDisposedException e) {
                        Logger.WriteLine(e.Message);
                        break;
                    }

                    Thread.Sleep(10);
                }
            });

        public override void Save() {
            using var db = new DatabaseContext();
            var player = db.Players.Find(Id);
            if (player != null)
                db.Entry(player).State = EntityState.Modified;
            else
                _ = db.Players.Add(this);
            _ = db.SaveChanges();
        }

        public void Dispose()
            => Client.Dispose();
    }
}
