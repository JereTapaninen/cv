﻿using SharpScape.Library.Common;
using SharpScape.Library.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Game.Entity.Skill {
    public class PlayerSkill {
        private static readonly int[] ExperienceTable = {
            0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523,
            3973, 4470, 5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833, 16456, 18247,
            20224, 22406, 24815, 27473, 30408, 33648, 37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127,
            83014, 91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254, 224466, 247886,
            273742, 302288, 333804, 368599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445,
            899257, 992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808, 1986068, 2192818, 2421087,
            2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
            7944614, 8771558, 9684577, 10692629, 11805606, 13034431
        };

        private const int MaxExperience = 200000000;

        [Key]
        public int Id { get; set; }
        public SkillType Type { get; set; }
        public int InterfaceId { get; set; }
        public int ButtonId { get; set; }
        public Player Player { get; set;  }

        private int experience;
        [NotMapped]
        public int Experience {
            get => experience;
            private set {
                experience = value;
                _ = Player.PacketSender.SendSkill(this);
            }
        }

        private int level;
        [NotMapped]
        public int Level {
            get => level;
            private set {
                level = value;
                _ = Player.PacketSender.SendSkill(this);
            }
        }

        [NotMapped]
        public int MaxLevel =>
            ExperienceTable.Sort().Aggregate(0, (acc, curr) =>
                Experience >= curr ?
                    acc + 1 :
                    acc
            );

        private PlayerSkill(SkillType type, int interfaceId, int buttonId) {
            Type = type;
            InterfaceId = interfaceId;
            ButtonId = buttonId;
        }

        public PlayerSkill Initialize(Player player) {
            Player = player;
            Experience = 0;
            return this;
        }

        public Either<PlayerSkill, SkillException> SetExperience(int experience)
            => experience.Validate<int, PlayerSkill, SkillException>(
                value => value >= ExperienceTable.Min() && value <= MaxExperience,
                value => {
                    Experience = value;
                    return this;
                },
                value => new SkillException(
                    $"Experience out of bounds for skill: {value}. Minimum is: {ExperienceTable.Min()}, while maximum is: {MaxExperience}."
                )
            );

        public Either<PlayerSkill, SkillException> SetMaxLevel(int maxLevel)
            => maxLevel.Validate(
                value => value >= 1,
                value => SetExperience(ExperienceTable.Sort()[Math.Min(ExperienceTable.Length, value) - 1]),
                _ => new SkillException(
                    $"MaxLevel is too low, the minimum MaxLevel is 1!"
                )
            );

        public Either<PlayerSkill, SkillException> SetLevel(int level)
            => level.Validate<int, PlayerSkill, SkillException>(
                value => value >= 0,
                value => {
                    Level = value;
                    return this;
                },
                _ => new SkillException(
                    $"Level is too low, the minimum level is 0!"
                )
            );

        public Either<PlayerSkill, SkillException> SetBothLevels(int lvl, int maxLvl)
            => lvl.Validate(
                value => value >= 0,
                newLevel => maxLvl.Validate<int, PlayerSkill, SkillException>(
                    value => value >= 1,
                    newMaxLevel => {
                        // bypass the properties so we can update both later
                        level = newLevel;
                        experience = ExperienceTable.Sort()[Math.Min(ExperienceTable.Length, newMaxLevel) - 1];
                        _ = Player.PacketSender.SendSkill(this);
                        return this;
                    },
                    _ => new SkillException(
                        $"MaxLevel is too low, the minimum MaxLevel is 1!"
                    )
                ),
                _ => new SkillException(
                    $"Level is too low, the minimum level is 0!"
                )
            );

        public static PlayerSkill[] CreateAll()
            => new PlayerSkill[] {
                new(SkillType.Attack, 6247, 52004),
                new(SkillType.Defence, 6253, 52010),
                new(SkillType.Strength, 6206, 52007),
                new(SkillType.Hitpoints, 6216, 52001),
                new(SkillType.Ranged, 4443, 52013),
                new(SkillType.Prayer, 6242, 52019),
                new(SkillType.Magic, 6211, 52016),
                new(SkillType.Cooking, 6226, 24222),
                new(SkillType.Woodcutting, 4272, 24228),
                new(SkillType.Fletching, 6231, 24227),
                new(SkillType.Fishing, 6258, 24219),
                new(SkillType.Firemaking, 4282, 24225),
                new(SkillType.Crafting, 6263, 24224),
                new(SkillType.Smithing, 6221, 24216),
                new(SkillType.Mining, 4416, 24213),
                new(SkillType.Herblore, 6237, 24218),
                new(SkillType.Agility, 4277, 24215),
                new(SkillType.Thieving, 4261, 24221),
                new(SkillType.Slayer, 12122, 24230),
                new(SkillType.Farming, 5267, 24231),
                new(SkillType.Runecrafting, 4267, 24229),
                new(SkillType.Construction, 7267, -1),
                new(SkillType.Hunter, 8267, 24232)
            };

        public override string ToString()
            => new StringBuilder()
                .AppendLine($"Skill {Type} ({(int)Type})")
                .AppendLine($"Interface Id: {InterfaceId}, Button Id: {ButtonId}")
                .AppendLine($"Current Experience: {Experience}")
                .AppendLine($"Max Experience: {ExperienceTable.Max()}")
                .AppendLine($"Current Level: {Level}")
                .AppendLine($"Max Level: {MaxLevel}")
                .ToString();
    }
}
