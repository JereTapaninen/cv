﻿using SharpScape.Library.Game.Entity;
using SharpScape.Library.Net;
using SharpScape.Library.Net.Login;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpScape.Library.Game {
    public sealed class World : IDisposable {
        public Task CleaningTask { get; }
        public CancellationTokenSource Cancellation => Server.Cancellation;
        public LoginHandler LoginHandler { get; }

        private Server Server { get; }
        private SynchronizedCollection<Player> Players { get; } = new();

        public World(Server server) {
            Server = server;
            CleaningTask = CleanPlayers();

            LoginHandler = new(this);
        }

        /// <summary>
        /// Cleans players from player list periodically
        /// @TODO: Find a better way to clean players
        /// </summary>
        /// <returns>A task which completes when server cancellation token is set</returns>
        private Task CleanPlayers()
            => Task.Run(() => {
                while (!Cancellation.IsCancellationRequested) {
                    Players
                        .Where(player => !player.Connected)
                        .ToList()
                        .ForEach(RemovePlayer);

                    Thread.Sleep(1000);
                }
            });

        private void RemovePlayer(Player player) {
            var removed = Players.Remove(player);

            if (removed)
                Debug.WriteLine("Cleared Player {0}, as they disconnected.", player.Username);
        }

        public void AddPlayer(Player player)
            => Players.Add(player);

        public short GetIndexOfPlayer(Player player)
            => (short)Players.IndexOf(player);

        public void Dispose() {
            foreach (var player in Players) {
                player.Dispose();
            }
        }
    }
}
