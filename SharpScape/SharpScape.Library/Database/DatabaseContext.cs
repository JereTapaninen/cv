﻿using Microsoft.EntityFrameworkCore;
using SharpScape.Library.Common;
using SharpScape.Library.Game.Entity;
using SharpScape.Library.Net.Login;
using System.IO;

namespace SharpScape.Library.Database {
    public sealed class DatabaseContext : DbContext {
        public DbSet<Player> Players { get; set; }
        public DbSet<LoginDetail> LoginDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options) {
            if (!Directory.Exists(Utils.DatabaseDirectory))
                _ = Directory.CreateDirectory(Utils.DatabaseDirectory);

            _ = options.UseSqlite($"Data Source={Utils.FullDatabasePath}");
        }

        public void Replace<TEntity>(TEntity oldEntity, TEntity newEntity) where TEntity : class {
            ChangeTracker.TrackGraph(oldEntity, e => e.Entry.State = EntityState.Deleted);
            ChangeTracker.TrackGraph(newEntity, e => e.Entry.State = e.Entry.IsKeySet ? EntityState.Modified : EntityState.Added);
        }
    }
}
