﻿using System;

namespace SharpScape.Library.Common {
    public class Either<TL, TR> {
        public static implicit operator Either<TL, TR>(TL left) => new(left);
        public static implicit operator Either<TL, TR>(TR right) => new(right);

        private readonly TL left;
        private readonly TR right;
        private readonly bool isLeft;

        public Either(TL left) {
            this.left = left;
            isLeft = true;
        }

        public Either(TR right) {
            this.right = right;
            isLeft = false;
        }

        public T Match<T>(Func<TL, T> success, Func<TR, T> failure)
            => isLeft ? success(left) : failure(right);

        public void Match(Action<TR> failure) {
            if (!isLeft)
                failure(right);
        }

        public void Match(Action<TL> success, Action<TR> failure) {
            if (isLeft)
                success(left);
            else
                failure(right);
        }
    }
}
