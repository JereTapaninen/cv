﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Common {
    public static class LEBitConverter {
        public static IEnumerable<byte> GetBytes(long value)
            => BitConverter.GetBytes(value).SwitchEndianness();

        public static IEnumerable<byte> GetBytes(short value)
            => BitConverter.GetBytes(value).SwitchEndianness();

        public static IEnumerable<byte> GetBytes(int value)
            => BitConverter.GetBytes(value).SwitchEndianness();
    }
}
