﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Common {
    public static class Utils {
        private const string DatabaseFile = "Server.db";

        public static string DatabaseDirectory
            => $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\SharpScape\\";
        // @TODO: make this customizable
        public static string FullDatabasePath
            => $"{DatabaseDirectory}{DatabaseFile}";
    }
}
