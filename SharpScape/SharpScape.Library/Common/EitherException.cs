﻿using SharpScape.Library.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Common {
    public class EitherException<TL, TR> : Either<TL, TR> where TR : new() {
        public static implicit operator EitherException<TL, TR>(TL left) => new(left);
        public static implicit operator EitherException<TL, TR>(TR right) => new(right);

        public EitherException(TL left) : base(left) { }

        public EitherException(TR right) : base(right) { }
    }
}
