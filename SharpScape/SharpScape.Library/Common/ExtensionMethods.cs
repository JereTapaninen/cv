﻿using SharpScape.Library.Common.Exceptions;
using SharpScape.Library.Game.Entity.Skill;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SharpScape.Library.Common {
    public static class ExtensionMethods {
        public static short ReadShortLE(this Stream stream)
            => BitConverter.ToInt16(stream.ReadBytesLE(sizeof(short)).ToArray());

        public static int ReadIntLE(this Stream stream)
            => BitConverter.ToInt32(stream.ReadBytesLE(sizeof(int)).ToArray());

        public static long ReadLongLE(this Stream stream)
            => BitConverter.ToInt64(stream.ReadBytesLE(sizeof(long)).ToArray());

        public static void Write(this Stream stream, IEnumerable<byte> bytes)
            => stream.Write(bytes.ToArray());

        public static IEnumerable<byte> ReadBytesLE(this Stream stream, int count)
            => new byte[count].Select(_ => (byte)stream.ReadByte()).SwitchEndianness();

        public static IEnumerable<byte> ReadToEnd(this Stream stream, int endSign = -1) {
            List<byte> rest = new();
            int nextByte;
            while ((nextByte = stream.ReadByte()) != endSign) {
                rest.Add((byte)nextByte);
            }
            return rest;
        }

        public static IEnumerable<int> ReadIntsLE(this Stream stream, int count)
            => new int[count].Select(_ => stream.ReadIntLE());

        public static IEnumerable<byte> SwitchEndianness(this IEnumerable<byte> byteArray)
            => byteArray.Reverse().ToArray();

        public static string ReadRSString(this Stream stream)
            => Encoding.ASCII.GetString(stream.ReadToEnd(10).ToArray());

        public static Either<TL, TR> Validate<TY, TL, TR>(
            this TY value,
            Func<TY, bool> validator,
            Func<TY, Either<TL, TR>> validFunc,
            Func<TY, TR> invalidFunc
        )
            => validator(value) ?
                validFunc(value) :
                invalidFunc(value);

        public static Either<TL, TR> Validate<TY, TL, TR>(
            this TY value,
            Func<TY, bool> validator,
            Func<Either<TL, TR>> validFunc,
            Func<TY, TR> invalidFunc
        )
            => validator(value) ?
                validFunc() :
                invalidFunc(value);

        public static Either<TL, TR> Validate<TY, TL, TR>(
            this TY value,
            Func<TY, bool> validator,
            Func<Either<TL, TR>> validFunc,
            Func<TR> invalidFunc
        )
            => validator(value) ?
                validFunc() :
                invalidFunc();

        public static BitArray ToBitArray(this IEnumerable<bool> bits)
            => new(bits.ToArray());

        public static BitArray Enlarge(this BitArray bits, int by)
            => bits.Append(new bool[by].Select(_ => false));

        public static BitArray Append(this BitArray current, IEnumerable<bool> toAdd)
            => current.Cast<bool>().Concat(toAdd).ToBitArray();

        public static int[] Sort(this int[] array) {
            var arr = new int[array.Length];
            array.CopyTo(arr, 0);
            Array.Sort(arr);

            return arr;
        }

        public static Either<PlayerSkill, SkillException> GetSpecific(this IEnumerable<PlayerSkill> collection, Func<PlayerSkill, bool> predicate)
            => collection.FirstOrDefault(predicate)
                .Validate<PlayerSkill, PlayerSkill, SkillException>(
                    skill => !skill.IsDefault(),
                    skill => skill,
                    _ => new SkillException($"Could not find skill from collection!")
                );

        public static bool IsDefault<T>(this T any)
            => EqualityComparer<T>.Default.Equals(any, default);

        public static Either<IEnumerable<TR>, TL> TrySelect<T, TR, TL>(
            this IEnumerable<T> enumerable,
            Func<T, Either<TR, TL>> conditionFunc
        ) {
            List<TR> newItems = new();
            foreach (var itm in enumerable) {
                var failure = default(TL);
                conditionFunc(itm)
                    .Match(
                        success: matchingItem => newItems.Add(matchingItem),
                        failure: exception => failure = exception
                    );

                if (!EqualityComparer<TL>.Default.Equals(failure, default)) {
                    return failure;
                }
            }

            return newItems;
        }

        public static EitherException<TL, TR> ToExceptionHandler<TL, TR>(this Either<TL, TR> either) where TR : new()
            => (EitherException<TL, TR>)either;
    }
}
