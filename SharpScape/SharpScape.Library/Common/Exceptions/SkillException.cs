﻿using System;

namespace SharpScape.Library.Common.Exceptions {
    public sealed class SkillException : Exception, ICustomException {
        public SkillException(string message) : base(message) { }
    }
}
