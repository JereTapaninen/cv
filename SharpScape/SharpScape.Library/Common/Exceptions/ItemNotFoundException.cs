﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Common.Exceptions {
    public sealed class ItemNotFoundException : Exception, ICustomException {
        public ItemNotFoundException(string message) : base(message) { }
    }
}
