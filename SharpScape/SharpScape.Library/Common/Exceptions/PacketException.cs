﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Common.Exceptions {
    public sealed class PacketException : Exception, ICustomException {
        public PacketException(string message)
            : base(message) { }
    }
}
