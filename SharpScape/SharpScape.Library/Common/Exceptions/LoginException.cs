﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Common.Exceptions {
    public sealed class LoginException : Exception, ICustomException {
        public LoginException(string message)
            : base(message) { }
    }
}
