﻿namespace SharpScape.Library.Common {
    public abstract class DatabaseEntity {
        public abstract void Save();
    }
}
