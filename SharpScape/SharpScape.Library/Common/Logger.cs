﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SharpScape.Library.Common {
    public sealed class Logger {
        private static readonly SynchronizedCollection<string> Backlog = new();

        private string ModuleName { get; }
        private ConsoleColor Color { get; }

        public Logger(string moduleName, ConsoleColor color) {
            ModuleName = moduleName;
            Color = color;
        }

        public Logger(string moduleName)
            : this(moduleName, (ConsoleColor)TrueRandom.Next(1, 16)) { }

        public Logger()
            : this(GetCallingAssembly(), (ConsoleColor)TrueRandom.Next(1, 16)) { }

        public void WriteLine(string formattedString) {
            if (Backlog.LastOrDefault() != formattedString) {
                Debug.WriteLine("[{0}] {1} ", ModuleName, formattedString);

                Console.ForegroundColor = Color;
                Console.Write("\n[{0}] {1} ", ModuleName, formattedString);
                Console.ResetColor();
            } else {
                Console.ForegroundColor = Color;
                Console.Write("x");
                Console.ResetColor();
            }

            Backlog.Add(formattedString);
        }

        public void WriteException(Exception exception) {
            if (Backlog.LastOrDefault() != exception.Message) {
                Debug.WriteLine("[{0}] {1} ", ModuleName, exception.Message);

                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\n[{0}] {1} ", ModuleName, exception.Message);
                Console.ResetColor();
            } else {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("x");
                Console.ResetColor();
            }

            Backlog.Add(exception.Message);
        }

        private static string GetCallingAssembly() {
            var frames = new StackTrace().GetFrames();
            return (from f in frames select f.GetMethod().ReflectedType.Name).Distinct().ElementAt(1);
        }

        public static void ClearBacklog()
            => Backlog.Clear();
    }
}
