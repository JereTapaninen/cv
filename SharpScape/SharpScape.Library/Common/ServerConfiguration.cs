﻿namespace SharpScape.Library.Net {
    public sealed class ServerConfiguration {
        public string IPAddress { get; set; }
        public int Port { get; set; }
        public DatabaseConfiguration Database { get; set; }

        public sealed class DatabaseConfiguration {
            public string ConnectionString { get; set; }
            public string Name { get; set; }
        }
    }
}
