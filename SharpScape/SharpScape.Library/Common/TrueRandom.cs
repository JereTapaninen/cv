﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpScape.Library.Common {
    public static class TrueRandom {
        private static readonly Random Rand = new();
        private static readonly object SyncLock = new();

        public static int Next(int min, int max) {
            lock (SyncLock) {
                return Rand.Next(min, max);
            }
        }

        public static long Next() {
            lock (SyncLock) {
                var first = Rand.Next(int.MinValue, int.MaxValue);
                lock (SyncLock) {
                    var second = Rand.Next(int.MinValue, int.MaxValue);

                    return BitConverter.ToInt64(
                        BitConverter.GetBytes(first)
                            .Concat(BitConverter.GetBytes(second))
                            .ToArray()
                    );
                }
            }
        }
    }
}
