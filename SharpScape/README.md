# SharpScape

SharpScape is a C# TCP server for a popular game. It is unfinished, but can handle some specific packages already.


## Running the application

### Pre-requisites:

- Visual Studio 2022
- .NET 5

### Steps:

1. Clone the project
2. Open the project with Visual Studio 2022
3. Debug the application
