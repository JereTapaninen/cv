﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpScape.Library.Common;
using System.IO;

namespace SharpScape.Library.Tests.Common {
    [TestFixture]
    public class ExtensionMethods_Tests {
        [TestCase(10)]
        [TestCase(-5)]
        [TestCase(200)]
        [TestCase(short.MinValue)]
        [TestCase(short.MaxValue)]
        public void ReadShortLE_StreamHasLEShort_ReturnLEShortFromStream(short value) {
            using var stream = new MemoryStream(BitConverter.GetBytes(value).SwitchEndianness().ToArray());
            var result = stream.ReadShortLE();
            Assert.AreEqual(value, result);
        }

        [TestCase(10)]
        [TestCase(-5)]
        [TestCase(200)]
        [TestCase(long.MinValue)]
        [TestCase(long.MaxValue)]
        public void ReadLongLE_StreamHasLELong_ReturnLELongFromStream(long value) {
            using var stream = new MemoryStream(BitConverter.GetBytes(value).SwitchEndianness().ToArray());
            var result = stream.ReadLongLE();
            Assert.AreEqual(value, result);
        }

        [TestCase(10)]
        [TestCase(-5)]
        [TestCase(200)]
        [TestCase(int.MinValue)]
        [TestCase(int.MaxValue)]
        public void ReadIntLE_StreamHasLEInt_ReturnLEIntFromStream(int value) {
            using var stream = new MemoryStream(BitConverter.GetBytes(value).SwitchEndianness().ToArray());
            var result = stream.ReadIntLE();
            Assert.AreEqual(value, result);
        }

        [TestCase(new byte[] { 1, 0, 5, 29, byte.MaxValue, byte.MinValue })]
        [TestCase(new byte[] { 6, 7, 1, 222, byte.MinValue })]
        public void Write_WithByteIEnumerable_WritesAllBytesToStream(IEnumerable<byte> bytes) {
            using var stream = new MemoryStream();
            stream.Write(bytes);
            stream.Position = 0;
            var readBytes = stream.ReadToEnd();
            Assert.AreEqual(bytes, readBytes);
        }

        [TestCase(new byte[] { 1, 0, 5, 29, byte.MaxValue, byte.MinValue }, 4)]
        [TestCase(new byte[] { 6, 7, 1, 222, byte.MinValue }, 2)]
        public void ReadBytesLE_WhenCountIsLessThanStreamLength_ReturnsCountBytesFromStreamInLE(IEnumerable<byte> bytes, int amount) {
            using var stream = new MemoryStream(bytes.ToArray());
            var readBytes = stream.ReadBytesLE(amount);
            var expectedBytes = bytes.Take(amount).SwitchEndianness();
            Assert.AreEqual(expectedBytes, readBytes);
        }

        [TestCase(new byte[] { 1, 0, 5, 29, byte.MaxValue, byte.MinValue }, 411)]
        [TestCase(new byte[] { 6, 7, 1, 222, byte.MinValue }, 2525)]
        public void ReadBytesLE_WhenCountIsMoreThanStreamLength_ReturnsCountBytesFromStreamWhereRestAre255InLE(IEnumerable<byte> bytes, int amount) {
            using var stream = new MemoryStream(bytes.ToArray());
            var readBytes = stream.ReadBytesLE(amount);
            var expectedBytes = bytes
                .Take(amount)
                .Concat(new byte[amount - bytes.Count()].Select(_ => unchecked((byte)-1)))
                .SwitchEndianness();
            Assert.AreEqual(expectedBytes, readBytes);
        }

        [TestCase(new byte[] { 1, 0, 5, 29, byte.MaxValue, byte.MinValue, unchecked((byte)-1), 55 }, -1)]
        [TestCase(new byte[] { 6, 7, 1, 222, byte.MinValue }, 222)]
        [TestCase(new byte[] { 6, 7, 1, 222, byte.MinValue }, -1)]
        public void ReadToEnd_Always_ReadsAllFromStreamToEndSign(IEnumerable<byte> bytes, int endSign) {
            using var stream = new MemoryStream(bytes.ToArray());
            var readBytes = stream.ReadToEnd(endSign);
            var expectedBytes = bytes.Aggregate(
                (List: new List<byte>(), Done: false),
                (acc, curr) =>
                    !acc.Done ? (
                        List: curr == endSign ? acc.List : acc.List.Append(curr).ToList(),
                        Done: curr == endSign
                    ) : acc
            ).List;
            Assert.AreEqual(expectedBytes, readBytes);
        }
    }
}
