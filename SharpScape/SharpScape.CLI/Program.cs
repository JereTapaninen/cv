﻿using System;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Linq;
using System.Text;
using SharpScape.Library.Net;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace SharpScape.CLI {
    public class Program {
        private static Server Server { get; set; } = null;

        public static async Task Main(string[] args) {
            Console.Title = "SharpScape Server CLI";

            StartServer();

            while (true) {
                var input = Console.ReadLine();

                if (input.Equals("exit", StringComparison.OrdinalIgnoreCase)) {
                    await Server.Stop();
                    break;
                } else if (input.Equals("restart", StringComparison.OrdinalIgnoreCase)) {
                    await RestartServer();
                }
            }
        }

        private static void StartServer() {
            using StreamReader streamReader = new("ServerConfiguration.xml");
            XmlSerializer serializer = new(typeof(ServerConfiguration));
            var configuration = (ServerConfiguration)serializer.Deserialize(streamReader);

            Server = new(configuration);
            Server.Start();
        }

        private static async Task RestartServer() {
            Console.WriteLine("\nRestarting server...");
            await Server?.Stop();
            StartServer();
        }
    }
}
