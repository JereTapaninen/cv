﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace VoronoiDiagram;
internal static class TrueRandom {
    private static readonly object SyncLock = new();
    private static readonly Random Rand = new();

    internal static int Next() {
        return Rand.Next();
    }

    internal static int Next(in int min, in int max) {
        lock (SyncLock) {
            return Rand.Next(min, max);
        }
    }
}
