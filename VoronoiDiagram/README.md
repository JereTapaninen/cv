# Voronoi Diagram

Voronoi Diagram is a small testing application that can generate a voronoi diagram. It was created in the process of developing HOI4UMT. Instead of using the GPU, this uses the CPU, and so is slower than the implementation in HOI4UMT.

## Running the application

### Pre-requisites:

- Visual Studio 2022
- .NET 6

### Steps:

1. Clone the project
2. Open the project with Visual Studio 2022
3. Debug the application
