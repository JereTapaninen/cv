using System.Diagnostics;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace VoronoiDiagram;

public partial class MainForm : Form {
    private const int MaxColor = 16777215;
    //private const int Cells = 1000;
    private const int SizeX = 10;
    private const int SizeY = 10;

    public MainForm() {
        InitializeComponent();
    }

    private async Task<Bitmap> CreateVoronoiDiagram() {
        Bitmap bmp = new(VoronoiPicture.Width, VoronoiPicture.Height, PixelFormat.Format24bppRgb);

        int cells = ((bmp.Width / SizeX) + 1) * ((bmp.Height / SizeY) + 1);

        ColorRGB[] colors = ParallelEnumerable
            .Range(0, MaxColor)
            .Select(value => new ColorRGB(value))
            .OrderBy(_ => TrueRandom.Next())
            .Take(cells)
            .ToArray();

        IDictionary<Point2D, ColorRGB> Points = new Dictionary<Point2D, ColorRGB>();

        int i = 0;
        for (int x = 0; x < bmp.Width; x += SizeX) {
            for (int y = 0; y < bmp.Height; y += SizeY) {
                int multiplierX = SizeX / 10;
                int multiplierY = SizeY / 10;
                int randomX = TrueRandom.Next(-3 * multiplierX, 4 * multiplierX);
                int randomY = TrueRandom.Next(-3 * multiplierY, 4 * multiplierY);
                Points.Add(new Point2D(x + randomX, randomY + y), colors[i]);
                i++;
            }
        }

        BitmapData locked = bmp.LockBits(
            new(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadWrite,
            bmp.PixelFormat
        );
        int bytesPerPixel = Image.GetPixelFormatSize(locked.PixelFormat) / 8;
        int width = locked.Width;
        int heightInPixels = locked.Height;

        //byte* firstPixelPtr = (byte*)locked.Scan0;

        IEnumerable<int> a = Enumerable.Range(0, heightInPixels);
        CancellationTokenSource s = new();
        await Parallel.ForEachAsync(a, async (y, s) => {
            for (int x = 0; x < width; x++) {
                Point2D closestPoint = await GetClosestPoint(new(x, y), Points.Keys);
                bool success = Points.TryGetValue(closestPoint, out ColorRGB color);

                if (success) {
                    var (r, g, b) = color;

                    int xInBytes = x * bytesPerPixel;
                    unsafe {
                        byte* currentLine = (byte*)locked.Scan0 + (y * locked.Stride);
                        currentLine[xInBytes] = b;
                        currentLine[xInBytes + 1] = g;
                        currentLine[xInBytes + 2] = r;
                    }
                }
            }
        });

        bmp.UnlockBits(locked);

        GC.Collect();
        GC.Collect();

        return bmp;
    }

    private async void GenerateButton_Click(object sender, EventArgs e) {
        Bitmap bmp = await CreateVoronoiDiagram();
        VoronoiPicture.Image = bmp;
    }

    private static Task<Point2D> GetClosestPoint(Point2D source, IEnumerable<Point2D> points)
        => Task.Run(() => {
            // Euclidean
            Point2D closest = points.OrderBy(destination => GetEuclideanDistance(source, destination)).First();
            // Manhattan
            //PointF closest = points.OrderBy(kv => Math.Abs(localX - kv.X) + Math.Abs(localY - kv.Y)).First();
            return closest;
        });

    private static double GetEuclideanDistance(Point2D source, Point2D destination)
        => Math.Sqrt(
            ((source.X - destination.X) * (source.X - destination.X)) +
            ((source.Y - destination.Y) * (source.Y - destination.Y))
        );
}

internal readonly record struct ColorRGB {
    private const int ConversionFactor = byte.MaxValue + 1;

    internal readonly byte R;
    internal readonly byte G;
    internal readonly byte B;

    internal ColorRGB(int colorValue) {
        B = (byte)(colorValue % ConversionFactor);
        G = (byte)((colorValue - B) / ConversionFactor % ConversionFactor);
        R = (byte)(((colorValue - B) / (ConversionFactor * ConversionFactor)) - (G / ConversionFactor));
    }

    internal void Deconstruct(out byte r, out byte g, out byte b) {
        r = R;
        g = G;
        b = B;
    }
}

internal readonly record struct Point2D(in float X, in float Y);
