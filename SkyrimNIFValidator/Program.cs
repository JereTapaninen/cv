﻿using LanguageExt;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Linq;

namespace SkyrimNIFValidator;

public static class Program {
    private const string TexturePrefix = "textures\\";
    private static readonly ConcurrentQueue<string> DebugLog = new();

    private static (Task, CancellationTokenSource) DebugLoggerThread() {
        CancellationTokenSource debugLoggerCancellationTokenSource = new();

        Task debugLoggerTask = Task.Factory.StartNew(() => {
            while (!debugLoggerCancellationTokenSource.Token.IsCancellationRequested) {
                while (!DebugLog.IsEmpty) {
                    while (DebugLog.TryDequeue(out string? debugLog)) {
                        if (debugLog != null)
                            Console.WriteLine(debugLog);
                    }
                }

                Thread.Sleep(10);
            }
        }, debugLoggerCancellationTokenSource.Token);
        return (debugLoggerTask, debugLoggerCancellationTokenSource);
    }

    public static Option<Process> OpenWithDefaultProgram(ReadOnlySpan<char> path) {
        ProcessStartInfo fileopener = new() {
            FileName = new string(path),
            UseShellExecute = true
        };
        return (Option<Process>)Process.Start(fileopener);
    }

    private static Option<string> GetValidatedPathString(Option<string> value)
        => value.Match(
            value => Directory.Exists(value) ?
                value :
                Option<string>.None,
            () => Option<string>.None
        );

    private static Option<string> GetPathWithFolderDialog() {
        using FolderBrowserDialog folderDialog = new() {
            Description = "Select the Skyrim Data Folder",
            InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86),
            ShowNewFolderButton = false,
            UseDescriptionForTitle = true
        };

        return folderDialog.ShowDialog() == DialogResult.OK ?
            folderDialog.SelectedPath :
            Option<string>.None;
    }

    private static Option<string> AskForPath() {
        Console.WriteLine("\nType the path to your Skyrim Data folder below or press ENTER to browse:");
        Console.Write(">");

        Option<string> input = Console.ReadLine();
        Option<string> validatedInput = input.Match(input =>
            GetValidatedPathString(
                input.Length <= 0 ?
                    GetPathWithFolderDialog() :
                    input
            ), () => Option<string>.None);

        return validatedInput;
    }

    private static Option<string> HandleNoArguments(ReadOnlySpan<char> message, ISettings settings) {
        Console.WriteLine(new string(message));
        Option<string> receivedPath = settings.SkyrimDataFolder != "" ? new string(settings.SkyrimDataFolder) : AskForPath();
        return receivedPath.IsSome ?
            receivedPath :
            HandleNoArguments("Invalid path selected, please try again.", settings);
    }

    private static Option<string> HandleArguments(IReadOnlyCollection<string> arguments) {
        Option<string> firstArgument = arguments.ElementAt(0);
        return firstArgument.Match(argument => GetValidatedPathString(argument), () => Option<string>.None);
    }

    private static ReadOnlySpan<char> ConcatenateTwoSpans(ReadOnlySpan<char> value1, ReadOnlySpan<char> value2) {
        Span<char> concatenated = stackalloc char[value1.Length + value2.Length];
        value1.CopyTo(concatenated);
        value2.CopyTo(concatenated[value1.Length..]);

        ReadOnlySpan<char> concatenatedSpan = concatenated;

        return new ReadOnlySpan<char>(concatenated);
    }

    private static string GetRelativePathOfFile(ReadOnlySpan<char> fullPath, ReadOnlySpan<char> basePath) {
        string fileName = Path.GetFileName(new string(fullPath));
        string directory = Path.GetDirectoryName(new string(fullPath)) ?? "";

        string fixedFullPath = !directory.EndsWith("\\") ?
            directory + "\\" :
            directory;
        string fixedBasePath = !basePath.EndsWith("\\") ?
             basePath + "\\" :
             basePath;

        Uri baseUri = new(fixedBasePath);
        Uri fullUri = new(fixedFullPath);

        Uri relativeUri = baseUri.MakeRelativeUri(fullUri);

        return Path.Join(relativeUri.ToString().Replace("/", "\\") + fileName);
    }

    private static Task<IEnumerable<string>> GetNIFFilesInDirectory(string directory)
        => Task.Run(async () => {
            string[] subDirectories = Directory.GetDirectories(directory);
            string[] nifFilesInSubdirectories = (await Task.WhenAll(subDirectories.Select(GetNIFFilesInDirectory))).SelectMany(d => d).ToArray();
            string[] nifFilesInDirectory = Directory.GetFiles(directory, "*.nif");
            return nifFilesInDirectory.Concat(nifFilesInSubdirectories);
        });

    private static IReadOnlyCollection<string> GetTexturesOfNIFFile(string nifFilePath, string dataFolder, ISettings settings) {
        DebugLog.Enqueue(GetRelativePathOfFile(nifFilePath, dataFolder));

        Encoding encoding = Encoding.UTF8;
        using FileStream stream = File.OpenRead(nifFilePath);
        byte[] buffer = new byte[settings.ByteCount];
        List<string> textures = new();

        while (true) {
            int bytesRead = stream.Read(buffer);

            if (bytesRead == 0)
                break; 

            string block = encoding.GetString(buffer, 0, bytesRead);

            int i = 0;
            while ((i = block.IndexOf(TexturePrefix, i, StringComparison.OrdinalIgnoreCase)) != -1) {
                long startingPosition = stream.Position;

                string fromIndex = block[i..];
                int indexOfDDS = fromIndex.IndexOf(".dds", StringComparison.OrdinalIgnoreCase);

                while (indexOfDDS == -1) {
                    bytesRead = stream.Read(buffer);

                    if (bytesRead == 0)
                        break;

                    fromIndex += encoding.GetString(buffer, 0, bytesRead);

                    indexOfDDS = fromIndex.IndexOf(".dds", StringComparison.OrdinalIgnoreCase);
                }

                stream.Position = startingPosition;

                if (indexOfDDS != -1)
                    textures.Add(Path.Join(dataFolder, fromIndex[..indexOfDDS] + ".dds"));

                i++;
            }
        }

        return textures;
    }

    private static int CreateOutput(IReadOnlyDictionary<string, IReadOnlyCollection<string>> invalidFilesByNIFFilePath, string dataFolder) {
        bool anyInvalidFiles = invalidFilesByNIFFilePath.Any(kvp => kvp.Value.Any());

        int onInvalidFiles() {
            StringBuilder sb = new();
            _ = sb
                .AppendLine("Skyrim NIF Validator Output File")
                .AppendLine("NOTE: Many of these missing textures probably do not cause any harm,")
                .AppendLine("though some of them might cause purple textures in-game.\n")
                .AppendLine("Following missing textures were noticed:\n");

            foreach (KeyValuePair<string, IReadOnlyCollection<string>> kvp in invalidFilesByNIFFilePath) {
                _ = sb
                    .AppendLine("TEXTURE:")
                    .AppendLine($"{GetRelativePathOfFile(kvp.Key, dataFolder)}")
                    .AppendLine("USED BY:");
                foreach (string nifFile in kvp.Value) {
                    _ = sb.AppendLine($"\t{GetRelativePathOfFile(nifFile, dataFolder)}");
                }
                _ = sb.AppendLine("");
            }

            Console.WriteLine($"{invalidFilesByNIFFilePath.Keys.Count()} MISSING TEXTURES NOTICED!");

            string outputFilePath = Path.Join(Application.StartupPath, "output.txt");

            try {
                File.WriteAllText(outputFilePath, sb.ToString());
                Console.WriteLine("The missing files and the plugins that depend on them have been written to output.txt");

                Console.WriteLine("Would you like to open it in your default editor? [y/n]");
                bool input = Console.ReadLine()?.Equals("y", StringComparison.OrdinalIgnoreCase) ?? false;
                if (input)
                    _ = OpenWithDefaultProgram(outputFilePath)
                        .Match(
                            process => Console.WriteLine("Opened in {0}", process.ProcessName),
                            () => Console.WriteLine("Unknown error occurred, could not open output.txt in default file.")
                        );
            } catch (Exception ex) {
                Console.WriteLine("Couldn't output missing texture information to a file due to an unknown error:");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Please report that to the developer :-)");
            }

            Console.WriteLine("Happy modding!");

            return 1;
        }

        static int onNoInvalidFiles() {
            Console.WriteLine("No missing textures found!");
            return 0;
        }

        return anyInvalidFiles ?
            onInvalidFiles() :
            onNoInvalidFiles();
    }

    private static async Task<IReadOnlyCollection<T>> ToListAsync<T>(
        this Task<IEnumerable<T>> enumerableTask
    ) => (await enumerableTask).ToList();

    private static async Task<int> OnValidPath(string validatedPathString, ISettings settings) {
        Console.Clear();
        (Task debugLogger, CancellationTokenSource cancellationTokenSource) = DebugLoggerThread();
        DebugLog.Enqueue("Starting...");

        ConcurrentBag<KeyValuePair<string, string>> d = new();

        IReadOnlyCollection<string> nifFilePaths = await GetNIFFilesInDirectory(validatedPathString).ToListAsync();
        DebugLog.Enqueue($"{nifFilePaths.Count} NIF files found.");

        _ = Parallel.ForEach(nifFilePaths, new ParallelOptions() { MaxDegreeOfParallelism = settings.MaximumConcurrency }, (nifFilePath) => {
            IReadOnlyCollection<string> nifTexturePaths = GetTexturesOfNIFFile(nifFilePath, validatedPathString, settings);
            IReadOnlyCollection<string> invalidNifTexturePaths = nifTexturePaths.Where(file => !File.Exists(file)).ToList();

            foreach (string invalidNifTexturePath in invalidNifTexturePaths) {
                d.Add(new(invalidNifTexturePath, nifFilePath));
            }
        });
        IReadOnlyDictionary<string, IReadOnlyCollection<string>> a = d
            .AsParallel()
            .GroupBy(g => g.Key, g => g.Value)
            .ToDictionary(g => g.Key, g => (IReadOnlyCollection<string>)g.ToList());

        DebugLog.Enqueue("Finishing...");

        cancellationTokenSource.Cancel();
        await debugLogger;
        Console.Clear();

        return CreateOutput(a, validatedPathString);
    }

    private static Task<int> OnInvalidPath()
        => Task.Run(() => {
            Console.WriteLine("Error: invalid path supplied.");
            return 1;
        });

    private static Task<int> OnPathReceived(Option<string> validatedInput, ISettings settings)
        => validatedInput.MatchAsync(
            validatedPathString => OnValidPath(validatedPathString, settings),
            OnInvalidPath
        );

    public static async Task<int> Main(string[] arguments) {
        Console.Title = "Skyrim NIF Validator";

        ISettings settings = JsonSerializer.Deserialize<ISettings>(File.ReadAllText("./Settings.json")) ?? new Settings();

        return await OnPathReceived(
            arguments.Length <= 0 ?
                HandleNoArguments("Welcome to Skyrim NIF Validator (SNV)", settings) :
                HandleArguments(arguments),
            settings
        );
    }
}

public interface ISettings {
    public ReadOnlySpan<char> SkyrimDataFolder { get; }
    public int ByteCount { get; }
    public int MaximumConcurrency { get; }
}

public class Settings : ISettings {
    private string skyrimDataFolder = "";

    [JsonPropertyName("skyrimDataFolder")]
    public ReadOnlySpan<char> SkyrimDataFolder { get => skyrimDataFolder.AsSpan(); init => skyrimDataFolder = new string(value); }
    [JsonPropertyName("byteCount")]
    public int ByteCount { get; init; } = 1024;
    [JsonPropertyName("maximumConcurrency")]
    public int MaximumConcurrency { get; init; } = -1;
}
