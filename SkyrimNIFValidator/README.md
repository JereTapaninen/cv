# Skyrim NIF Validator

Skyrim NIF Validator is a C# CLI program that checks every .NIF file in the Skyrim data/meshes/ folder for missing textures. NIF files are the file type for game 3D models that Bethesda Game Studios uses.

## Running the application

### Pre-requisites:

- Skyrim installed, preferably Skyrim Special Edition
- Visual Studio 2022
- .NET 7

### Steps:

1. Clone the project
2. Open the project with Visual Studio 2022
3. Debug the application
