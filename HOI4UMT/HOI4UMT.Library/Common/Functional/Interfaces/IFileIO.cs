﻿namespace HOI4UMT.Library.Common.Functional.Interfaces;

public interface IFileIO {
    string ReadAllText(string path);
}
