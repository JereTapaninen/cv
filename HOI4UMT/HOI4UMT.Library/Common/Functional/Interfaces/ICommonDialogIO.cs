﻿namespace HOI4UMT.Library.Common.Functional.Interfaces;

public interface ICommonDialogIO {
    DialogResult ShowDialog(CommonDialog commonDialog);
}
