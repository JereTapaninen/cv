using System.Diagnostics;
using System.Drawing.Imaging;
using Tesseract;
using StaticCursor = System.Windows.Forms.Cursor;

namespace OptiNovel.UI;

public partial class MainForm : Form {
    private float TrackbarThreshold { get; set; } = 0.5f;

    private List<ScreenAreaSelectorForm> ScreenAreaSelectors { get; } = new();
    private Rectangle SelectedArea { get; set; } = Rectangle.Empty;
    private Image? CurrentImage { get; set; } = null;
    private bool PickingColor { get; set; } = false;
    private Color CapturedColor { get; set; } = Color.Black;
    private int CurrentMAD { get; set; } = 0;
    private TesseractEngine Engine { get; }

    public MainForm() {
        InitializeComponent();
        Engine = new(@".\tessdata", "jpn", EngineMode.Default);
    }

    private void Updater() {
       while (true) {
            if (CurrentImage != null)
                CurrentImage.Dispose();

            Image? tempImage = Utils.CaptureArea(SelectedArea);
            
            if (tempImage != null) {
                ApplyColorMatrices(ref tempImage);
                if (ExcludeColorsCheckbox.Checked && !PickingColor)
                    tempImage = Filter(tempImage);
                if (CropCheckbox.Checked) {
                    Rectangle cropArea = GetCropArea(tempImage);
                    tempImage = CropImage(tempImage, cropArea);
                }

                CurrentImage = tempImage;
                _ = Invoke(new MethodInvoker(CaptureBox.Refresh));

                using MemoryStream ms = new();
                tempImage.Save(ms, System.Drawing.Imaging.ImageFormat.Tiff);
                using Pix pix = Pix.LoadTiffFromMemory(ms.ToArray());
                using Page page = Engine.Process(pix);
                string result = page.GetText();
                try {
                    _ = Invoke(new MethodInvoker(() => DetectedTextResultLabel.Text = result));
                } catch (ObjectDisposedException) {

                }
            }

            Thread.Sleep(1000 / 60);
        }
    }

    private void ApplyColorMatrices(ref Image image) {
        using Graphics gfx = Graphics.FromImage(image);
        Rectangle imageArea = new Rectangle(0, 0, image.Width, image.Height);

        if (InvertCheckbox.Checked) {
            ColorMatrix colorMatrixInvert = new(new float[][] {
                new float[] {-1, 0, 0, 0, 0},
                new float[] {0, -1, 0, 0, 0},
                new float[] {0, 0, -1, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {1, 1, 1, 0, 1}
            });
            ImageAttributes imageAttributesInvert = new();
            imageAttributesInvert.SetColorMatrix(colorMatrixInvert);
            gfx.DrawImage(image, imageArea, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributesInvert);
        }

        if (GrayscaleCheckbox.Checked) {
            ColorMatrix colorMatrixGrayScale = new(new float[][] {
                new float[] { 0.299f, 0.299f, 0.299f, 0, 0 },
                new float[] { 0.587f, 0.587f, 0.587f, 0, 0 },
                new float[] { 0.114f, 0.114f, 0.114f, 0, 0 },
                new float[] { 0,      0,      0,      1, 0 },
                new float[] { 0,      0,      0,      0, 1 }
            });
            ImageAttributes imageAttributesGrayScale = new();
            imageAttributesGrayScale.SetColorMatrix(colorMatrixGrayScale);
            imageAttributesGrayScale.SetThreshold(TrackbarThreshold);
            gfx.DrawImage(image, imageArea, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributesGrayScale);
        }

        /*
        ColorMatrix colorMatrix = new(new float[][] {
            new float[] { 5, 0, 0, 0, 0 },
            new float[] { 0, 5, 0, 0, 0 },
            new float[] { 0, 0, 5, 0, 0 },
            new float[] { 0, 0, 0, 5, 0 },
            new float[] { 0.005f, 0.005f, 0.005f, 0, 0 },
        });
        ImageAttributes imageAttributes = new();
        imageAttributes.SetColorMatrix(colorMatrix);
        gfx.DrawImage(image, imageArea, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributes);*/
    }

    private Image Filter(Image input) {
        Bitmap bmp = (Bitmap)input;
        BitmapData locked = bmp.LockBits(
            new(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadOnly,
            bmp.PixelFormat
        );
        int bytesPerPixel = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;
        int width = locked.Width;
        int heightInPixels = locked.Height;

        _ = Parallel.For(0, heightInPixels, y => {
            for (int x = 0; x < width; x++) {
                int xInBytes = x * bytesPerPixel;
                unsafe {
                    byte* currentLine = (byte*)locked.Scan0 + (y * locked.Stride);
                    byte currentB = currentLine[xInBytes];
                    byte currentG = currentLine[xInBytes + 1];
                    byte currentR = currentLine[xInBytes + 2];

                    if (Math.Abs((currentB + currentG + currentR) - (CapturedColor.B + CapturedColor.G + CapturedColor.R)) >= CurrentMAD) {
                        currentLine[xInBytes] = 0;
                        currentLine[xInBytes + 1] = 0;
                        currentLine[xInBytes + 2] = 0;
                    } else {
                        currentLine[xInBytes] = byte.MaxValue;
                        currentLine[xInBytes + 1] = byte.MaxValue;
                        currentLine[xInBytes + 2] = byte.MaxValue;
                    }

                }
            }
        });

        bmp.UnlockBits(locked);

        return bmp;
    }

    private Rectangle GetCropArea(Image input) {
        Bitmap bmp = (Bitmap)input;
        BitmapData locked = bmp.LockBits(
            new(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadOnly,
            bmp.PixelFormat
        );
        int bytesPerPixel = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;
        int width = locked.Width;
        int heightInPixels = locked.Height;

        int lowestX = 0;
        int lowestY = 0;
        int highestX = 0;
        int highestY = 0;

        _ = Parallel.For(0, heightInPixels, y => {
            for (int x = 0; x < width; x++) {
                int xInBytes = x * bytesPerPixel;
                unsafe {
                    byte* currentLine = (byte*)locked.Scan0 + (y * locked.Stride);
                    byte currentB = currentLine[xInBytes];
                    byte currentG = currentLine[xInBytes + 1];
                    byte currentR = currentLine[xInBytes + 2];

                    if (currentG != 0 && currentB != 0 && currentR != 0) {
                        lowestX = x < lowestX ? x : lowestX;
                        lowestY = y < lowestY ? y : lowestY;
                        highestX = x > highestX ? x : highestX;
                        highestY = y > highestY ? y : highestY;
                    }

                }
            }
        });

        bmp.UnlockBits(locked);

        return new(lowestX, lowestY, Math.Max(Math.Abs(highestX - lowestX), 1), Math.Max(Math.Abs(highestY - lowestY), 1));
    }

    private Image CropImage(Image image, Rectangle area) {
        Bitmap bmp = new(area.Width, area.Height);
        using Graphics gfx = Graphics.FromImage(bmp);
        gfx.DrawImage(image, new Rectangle(0, 0, area.Width, area.Height), area, GraphicsUnit.Pixel);
        image.Dispose();
        return bmp;
    }

    private void SelectScreenAreaButton_Click(object sender, EventArgs e) {
        ScreenAreaSelectors.Clear();
        ScreenAreaSelectors.AddRange(Screen.AllScreens.Select(screen => new ScreenAreaSelectorForm(screen, OnSelectArea, OnCloseAllSelectors)));
        ScreenAreaSelectors.ForEach(screenAreaSelector => screenAreaSelector.Show());
    }

    private void OnSelectArea(Rectangle area) {
        SelectedArea = area;
        ScreenAreaSelectors.ForEach(screenAreaSelector => screenAreaSelector.Close());
    }

    private void OnCloseAllSelectors() {
        ScreenAreaSelectors.ForEach(screenAreaSelector => screenAreaSelector.Close());
    }

    private void CaptureBox_Paint(object sender, PaintEventArgs e) {
        if (CurrentImage != null) {
            try {
                e.Graphics.DrawImage(CurrentImage, new Rectangle(0, 0, CaptureBox.Width, CaptureBox.Height));
            } catch (ArgumentException) {

            } catch (InvalidOperationException) {

            }
        }
    }

    private void MainForm_Load(object sender, EventArgs e) {
        new Thread(Updater).Start();
    }

    private void GrayscaleCheckbox_CheckedChanged(object sender, EventArgs e) {

    }

    private void GrayscaleThresholdTrackbar_Scroll(object sender, EventArgs e) {
        TrackbarThreshold = GrayscaleThresholdTrackbar.Value / 100f;
    }

    private void PickColorBtn_Click(object sender, EventArgs e) {
        PickingColor = !PickingColor;

        CaptureBox.Cursor = PickingColor ? Cursors.Cross : Cursors.Default;
    }

    private void CaptureBox_MouseUp(object sender, MouseEventArgs e) {
        if (e.Button == MouseButtons.Left && PickingColor) {
            PickingColor = false;
            CaptureBox.Cursor = Cursors.Default;
            StaticCursor.Hide();

            Thread.Sleep(10);

            Point pointOnScreen = CaptureBox.PointToScreen(e.Location);
            Image? img = Utils.CaptureArea(new Rectangle(pointOnScreen.X, pointOnScreen.Y, 1, 1));

            if (img != null) {
                img.Save("someimg.png");
                Bitmap bmp = (Bitmap)img;
                CapturedColor = bmp.GetPixel(0, 0);
                StaticCursor.Show();

                img.Dispose();
                bmp.Dispose();

                Color inverted = Color.FromArgb(CapturedColor.ToArgb() ^ 0xffffff);
                ColorHexValueLabel.Text = "#" + CapturedColor.R.ToString("X2") + CapturedColor.G.ToString("X2") + CapturedColor.B.ToString("X2");
                ColorHexValueLabel.BackColor = CapturedColor;
                ColorHexValueLabel.ForeColor = inverted;
            } else {
                StaticCursor.Show();
                _ = MessageBox.Show("Failed to capture color!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

    private void MADNumericUpDown_ValueChanged(object sender, EventArgs e) {
        CurrentMAD = (int)MADNumericUpDown.Value;
    }
}
