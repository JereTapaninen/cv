﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiNovel.UI;

static internal class Utils {
    internal static Image? CaptureArea(Rectangle area) {
        static Image continueCapture(Rectangle area) {
            Image capture = new Bitmap(area.Width, area.Height, PixelFormat.Format32bppArgb);
            using var gfx = Graphics.FromImage(capture);
            try {
                gfx.CopyFromScreen(area.Left, area.Top, 0, 0, area.Size, CopyPixelOperation.SourceCopy);
            } catch (Win32Exception) {

            }
            return capture;
        }

        return area.Width > 0 && area.Height > 0 ?
            continueCapture(area) :
            null;
    }
}
