﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OptiNovel.UI;
public partial class ScreenAreaSelectorForm : Form {
    private const string SelectableText = "Select an area encompassing the text";
    private const float SelectableTextFontSize = 36f;
    private const int SelectionFactor = 2;
    private const int UpdateIntervalMs = 1000 / 60;

    public delegate void SelectArea(Rectangle area);
    public delegate void CloseAllSelectors();

    private Screen SelectedScreen { get; }
    private Image? CapturedArea { get; }
    private Font TextFont { get; }
    private Brush TextBrush { get; } = Brushes.LightGray;
    private Point TextPosition { get; } = new(10, 10);
    private SelectArea OnSelectArea { get; }
    private CloseAllSelectors OnCloseAllSelectors { get; }
    private bool Selecting { get; set; } = false;
    private Point SelectStart { get; set; } = Point.Empty;
    private Point SelectEnd { get; set; } = Point.Empty;
    private Brush SelectionBrush { get; } = new SolidBrush(Color.FromArgb(
        byte.MaxValue / SelectionFactor,
        byte.MaxValue,
        byte.MaxValue,
        byte.MaxValue
    ));
    private Pen SelectionPen { get; } = new(Color.FromArgb(
        byte.MaxValue / SelectionFactor,
        byte.MaxValue / SelectionFactor,
        byte.MaxValue / SelectionFactor,
        byte.MaxValue / SelectionFactor
    ));
    private Thread UpdateThread { get; set; }
    private CancellationTokenSource CancellationToken { get; } = new();

    public ScreenAreaSelectorForm(Screen screen, SelectArea selectArea, CloseAllSelectors closeAll) {
        InitializeComponent();

        foreach (Screen screen2 in Screen.AllScreens)
            Debug.WriteLine(screen2.WorkingArea);

        SelectedScreen = screen;

        OnCloseAllSelectors = closeAll;
        OnSelectArea = selectArea;

        TextFont = new(Font.FontFamily, SelectableTextFontSize, FontStyle.Bold);

        DesktopBounds = screen.WorkingArea;
        WindowState = FormWindowState.Maximized;

        CapturedArea = Utils.CaptureArea(screen.WorkingArea);
    }

    private void Updater() {
        /*while (!CancellationToken.IsCancellationRequested) {
            try {
                _ = Invoke(new MethodInvoker(() => Refresh()));
            } catch (ObjectDisposedException) {
            }

            Thread.Sleep(UpdateIntervalMs);
        }*/
    }

    private void ScreenAreaSelectorForm_Load(object sender, EventArgs e) {
        if (CapturedArea == null) {
            _ = MessageBox.Show("Could not capture screen area!", "Error while capturing screen!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Close();
            return;
        }

        UpdateThread = new(Updater);
        UpdateThread.Start();

        Opacity = 1;
    }

    private void ScreenAreaSelectorForm_Paint(object sender, PaintEventArgs e) {
        e.Graphics.Clear(Color.White);

        if (CapturedArea != null) {
            e.Graphics.DrawImage(
                CapturedArea,
                new Rectangle(0, 0, Size.Width, Size.Height),
                new Rectangle(0, 0, CapturedArea.Width, CapturedArea.Height),
                GraphicsUnit.Pixel
            );
        }

        e.Graphics.DrawString(SelectableText, TextFont, TextBrush, TextPosition);

        if (Selecting) {
            Point smaller = new(
                Math.Min(SelectEnd.X, SelectStart.X),
                Math.Min(SelectEnd.Y, SelectStart.Y)
            );
            Point larger = new(
                Math.Max(SelectEnd.X, SelectStart.X),
                Math.Max(SelectEnd.Y, SelectStart.Y)
            );

            e.Graphics.FillRectangle(
                SelectionBrush,
                smaller.X,
                smaller.Y,
                larger.X - smaller.X,
                larger.Y - smaller.Y
            );
            e.Graphics.DrawRectangle(
                SelectionPen,
                smaller.X,
                smaller.Y,
                larger.X - smaller.X,
                larger.Y - smaller.Y
            );
        }
    }


    private void ScreenAreaSelectorForm_MouseDown(object sender, MouseEventArgs e) {
        if (e.Button == MouseButtons.Left) {
            Selecting = true;
            SelectStart = e.Location;
        } else if (e.Button == MouseButtons.Right) {
            OnCloseAllSelectors();
        }
    }

    private void ScreenAreaSelectorForm_MouseUp(object sender, MouseEventArgs e) {
        if (e.Button == MouseButtons.Left) {
            Selecting = false;

            Point min = new(
                Math.Min(SelectStart.X, SelectEnd.X),
                Math.Min(SelectStart.Y, SelectEnd.Y)
            );
            Point max = new(
                Math.Max(SelectStart.X, SelectEnd.X),
                Math.Max(SelectStart.Y, SelectEnd.Y)
            );

            Point screenPoint = PointToScreen(new Point(min.X, min.Y));
            OnSelectArea(new Rectangle(screenPoint.X, screenPoint.Y, Math.Abs(min.X - max.X), Math.Abs(min.Y - max.Y)));
        }
    }

    private void ScreenAreaSelectorForm_FormClosing(object sender, FormClosingEventArgs e) {
        CancellationToken.Cancel();
    }

    private void ScreenAreaSelectorForm_MouseMove(object sender, MouseEventArgs e) {
        SelectEnd = e.Location;
    }
}
