﻿namespace OptiNovel.UI;

partial class MainForm {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
        if (disposing && (components != null)) {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.ApplicationLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.DetectedTextResultLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CaptureBox = new OptiNovel.UI.DoubleBufferedPanel();
            this.ImageSettingsLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.InvertCheckbox = new System.Windows.Forms.CheckBox();
            this.GrayscaleCheckbox = new System.Windows.Forms.CheckBox();
            this.GrayscaleThresholdLabel = new System.Windows.Forms.Label();
            this.GrayscaleThresholdTrackbar = new System.Windows.Forms.TrackBar();
            this.ExcludeColorsCheckbox = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.ColorHexValueLabel = new System.Windows.Forms.Label();
            this.PickColorBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.MADNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.CropCheckbox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DetectedTextLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.SelectScreenAreaButton = new System.Windows.Forms.Button();
            this.OpenServer = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ApplicationLayoutPanel.SuspendLayout();
            this.ImageSettingsLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrayscaleThresholdTrackbar)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MADNumericUpDown)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplicationLayoutPanel
            // 
            this.ApplicationLayoutPanel.ColumnCount = 4;
            this.ApplicationLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.ApplicationLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.25126F));
            this.ApplicationLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 238F));
            this.ApplicationLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.74875F));
            this.ApplicationLayoutPanel.Controls.Add(this.DetectedTextResultLabel, 3, 2);
            this.ApplicationLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.ApplicationLayoutPanel.Controls.Add(this.CaptureBox, 0, 2);
            this.ApplicationLayoutPanel.Controls.Add(this.ImageSettingsLayout, 2, 2);
            this.ApplicationLayoutPanel.Controls.Add(this.label2, 2, 0);
            this.ApplicationLayoutPanel.Controls.Add(this.DetectedTextLayout, 0, 3);
            this.ApplicationLayoutPanel.Controls.Add(this.flowLayoutPanel1, 0, 1);
            this.ApplicationLayoutPanel.Controls.Add(this.label3, 3, 1);
            this.ApplicationLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ApplicationLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.ApplicationLayoutPanel.Name = "ApplicationLayoutPanel";
            this.ApplicationLayoutPanel.RowCount = 4;
            this.ApplicationLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.ApplicationLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.ApplicationLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ApplicationLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ApplicationLayoutPanel.Size = new System.Drawing.Size(802, 507);
            this.ApplicationLayoutPanel.TabIndex = 0;
            // 
            // DetectedTextResultLabel
            // 
            this.DetectedTextResultLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DetectedTextResultLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetectedTextResultLabel.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.DetectedTextResultLabel.Location = new System.Drawing.Point(632, 125);
            this.DetectedTextResultLabel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.DetectedTextResultLabel.Name = "DetectedTextResultLabel";
            this.DetectedTextResultLabel.Size = new System.Drawing.Size(167, 351);
            this.DetectedTextResultLabel.TabIndex = 1;
            this.DetectedTextResultLabel.Text = "N/A";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.label1.Location = new System.Drawing.Point(15, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 75);
            this.label1.TabIndex = 0;
            this.label1.Text = "OptiNovel";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CaptureBox
            // 
            this.CaptureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ApplicationLayoutPanel.SetColumnSpan(this.CaptureBox, 2);
            this.CaptureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CaptureBox.Location = new System.Drawing.Point(20, 130);
            this.CaptureBox.Margin = new System.Windows.Forms.Padding(20, 15, 3, 0);
            this.CaptureBox.Name = "CaptureBox";
            this.CaptureBox.Size = new System.Drawing.Size(368, 346);
            this.CaptureBox.TabIndex = 2;
            this.CaptureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.CaptureBox_Paint);
            this.CaptureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CaptureBox_MouseUp);
            // 
            // ImageSettingsLayout
            // 
            this.ImageSettingsLayout.Controls.Add(this.InvertCheckbox);
            this.ImageSettingsLayout.Controls.Add(this.GrayscaleCheckbox);
            this.ImageSettingsLayout.Controls.Add(this.GrayscaleThresholdLabel);
            this.ImageSettingsLayout.Controls.Add(this.GrayscaleThresholdTrackbar);
            this.ImageSettingsLayout.Controls.Add(this.ExcludeColorsCheckbox);
            this.ImageSettingsLayout.Controls.Add(this.flowLayoutPanel2);
            this.ImageSettingsLayout.Controls.Add(this.PickColorBtn);
            this.ImageSettingsLayout.Controls.Add(this.label5);
            this.ImageSettingsLayout.Controls.Add(this.MADNumericUpDown);
            this.ImageSettingsLayout.Controls.Add(this.CropCheckbox);
            this.ImageSettingsLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageSettingsLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.ImageSettingsLayout.Location = new System.Drawing.Point(394, 130);
            this.ImageSettingsLayout.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.ImageSettingsLayout.Name = "ImageSettingsLayout";
            this.ImageSettingsLayout.Size = new System.Drawing.Size(232, 343);
            this.ImageSettingsLayout.TabIndex = 3;
            // 
            // InvertCheckbox
            // 
            this.InvertCheckbox.AutoSize = true;
            this.InvertCheckbox.Location = new System.Drawing.Point(3, 3);
            this.InvertCheckbox.Name = "InvertCheckbox";
            this.InvertCheckbox.Size = new System.Drawing.Size(56, 19);
            this.InvertCheckbox.TabIndex = 1;
            this.InvertCheckbox.Text = "Invert";
            this.InvertCheckbox.UseVisualStyleBackColor = true;
            // 
            // GrayscaleCheckbox
            // 
            this.GrayscaleCheckbox.AutoSize = true;
            this.GrayscaleCheckbox.Location = new System.Drawing.Point(3, 28);
            this.GrayscaleCheckbox.Name = "GrayscaleCheckbox";
            this.GrayscaleCheckbox.Size = new System.Drawing.Size(113, 19);
            this.GrayscaleCheckbox.TabIndex = 0;
            this.GrayscaleCheckbox.Text = "Black And White";
            this.GrayscaleCheckbox.UseVisualStyleBackColor = true;
            this.GrayscaleCheckbox.CheckedChanged += new System.EventHandler(this.GrayscaleCheckbox_CheckedChanged);
            // 
            // GrayscaleThresholdLabel
            // 
            this.GrayscaleThresholdLabel.AutoSize = true;
            this.GrayscaleThresholdLabel.Location = new System.Drawing.Point(3, 50);
            this.GrayscaleThresholdLabel.Name = "GrayscaleThresholdLabel";
            this.GrayscaleThresholdLabel.Size = new System.Drawing.Size(152, 15);
            this.GrayscaleThresholdLabel.TabIndex = 3;
            this.GrayscaleThresholdLabel.Text = "Black And White Threshold:";
            // 
            // GrayscaleThresholdTrackbar
            // 
            this.GrayscaleThresholdTrackbar.Location = new System.Drawing.Point(3, 68);
            this.GrayscaleThresholdTrackbar.Maximum = 100;
            this.GrayscaleThresholdTrackbar.Name = "GrayscaleThresholdTrackbar";
            this.GrayscaleThresholdTrackbar.Size = new System.Drawing.Size(225, 45);
            this.GrayscaleThresholdTrackbar.TabIndex = 2;
            this.GrayscaleThresholdTrackbar.Value = 50;
            this.GrayscaleThresholdTrackbar.Scroll += new System.EventHandler(this.GrayscaleThresholdTrackbar_Scroll);
            // 
            // ExcludeColorsCheckbox
            // 
            this.ExcludeColorsCheckbox.AutoSize = true;
            this.ExcludeColorsCheckbox.Location = new System.Drawing.Point(3, 119);
            this.ExcludeColorsCheckbox.Name = "ExcludeColorsCheckbox";
            this.ExcludeColorsCheckbox.Size = new System.Drawing.Size(104, 19);
            this.ExcludeColorsCheckbox.TabIndex = 4;
            this.ExcludeColorsCheckbox.Text = "Exclude Colors";
            this.ExcludeColorsCheckbox.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label4);
            this.flowLayoutPanel2.Controls.Add(this.ColorHexValueLabel);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 144);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(225, 17);
            this.flowLayoutPanel2.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Color HEX:";
            // 
            // ColorHexValueLabel
            // 
            this.ColorHexValueLabel.AutoSize = true;
            this.ColorHexValueLabel.BackColor = System.Drawing.Color.Black;
            this.ColorHexValueLabel.ForeColor = System.Drawing.Color.White;
            this.ColorHexValueLabel.Location = new System.Drawing.Point(73, 0);
            this.ColorHexValueLabel.Name = "ColorHexValueLabel";
            this.ColorHexValueLabel.Size = new System.Drawing.Size(50, 15);
            this.ColorHexValueLabel.TabIndex = 1;
            this.ColorHexValueLabel.Text = "#000000";
            // 
            // PickColorBtn
            // 
            this.PickColorBtn.Location = new System.Drawing.Point(3, 167);
            this.PickColorBtn.Name = "PickColorBtn";
            this.PickColorBtn.Size = new System.Drawing.Size(75, 23);
            this.PickColorBtn.TabIndex = 2;
            this.PickColorBtn.Text = "Pick color";
            this.PickColorBtn.UseVisualStyleBackColor = true;
            this.PickColorBtn.Click += new System.EventHandler(this.PickColorBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(172, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "Maximum Absolute Difference:";
            // 
            // MADNumericUpDown
            // 
            this.MADNumericUpDown.Location = new System.Drawing.Point(3, 211);
            this.MADNumericUpDown.Maximum = new decimal(new int[] {
            765,
            0,
            0,
            0});
            this.MADNumericUpDown.Name = "MADNumericUpDown";
            this.MADNumericUpDown.Size = new System.Drawing.Size(225, 23);
            this.MADNumericUpDown.TabIndex = 7;
            this.MADNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.MADNumericUpDown.ValueChanged += new System.EventHandler(this.MADNumericUpDown_ValueChanged);
            // 
            // CropCheckbox
            // 
            this.CropCheckbox.AutoSize = true;
            this.CropCheckbox.Location = new System.Drawing.Point(3, 240);
            this.CropCheckbox.Name = "CropCheckbox";
            this.CropCheckbox.Size = new System.Drawing.Size(174, 19);
            this.CropCheckbox.TabIndex = 8;
            this.CropCheckbox.Text = "Crop to first non-black pixel";
            this.CropCheckbox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Location = new System.Drawing.Point(394, 53);
            this.label2.Name = "label2";
            this.ApplicationLayoutPanel.SetRowSpan(this.label2, 2);
            this.label2.Size = new System.Drawing.Size(232, 62);
            this.label2.TabIndex = 4;
            this.label2.Text = "Change the settings until\r\nthe image on the left has a\r\nBLACK BACKGROUND and a\r\nW" +
    "HITE FOREGROUND";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // DetectedTextLayout
            // 
            this.ApplicationLayoutPanel.SetColumnSpan(this.DetectedTextLayout, 3);
            this.DetectedTextLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetectedTextLayout.Location = new System.Drawing.Point(3, 479);
            this.DetectedTextLayout.Name = "DetectedTextLayout";
            this.DetectedTextLayout.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.DetectedTextLayout.Size = new System.Drawing.Size(623, 25);
            this.DetectedTextLayout.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.ApplicationLayoutPanel.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.SelectScreenAreaButton);
            this.flowLayoutPanel1.Controls.Add(this.OpenServer);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(15, 85);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(15, 10, 3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(373, 27);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // SelectScreenAreaButton
            // 
            this.SelectScreenAreaButton.Location = new System.Drawing.Point(3, 3);
            this.SelectScreenAreaButton.Name = "SelectScreenAreaButton";
            this.SelectScreenAreaButton.Size = new System.Drawing.Size(156, 23);
            this.SelectScreenAreaButton.TabIndex = 2;
            this.SelectScreenAreaButton.Text = "Select a screen area...";
            this.SelectScreenAreaButton.UseVisualStyleBackColor = true;
            this.SelectScreenAreaButton.Click += new System.EventHandler(this.SelectScreenAreaButton_Click);
            // 
            // OpenServer
            // 
            this.OpenServer.Location = new System.Drawing.Point(165, 3);
            this.OpenServer.Name = "OpenServer";
            this.OpenServer.Size = new System.Drawing.Size(106, 23);
            this.OpenServer.TabIndex = 3;
            this.OpenServer.Text = "Open Server...";
            this.OpenServer.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(632, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(167, 40);
            this.label3.TabIndex = 0;
            this.label3.Text = "Detected text:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 507);
            this.Controls.Add(this.ApplicationLayoutPanel);
            this.DoubleBuffered = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OptiNovel";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ApplicationLayoutPanel.ResumeLayout(false);
            this.ApplicationLayoutPanel.PerformLayout();
            this.ImageSettingsLayout.ResumeLayout(false);
            this.ImageSettingsLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrayscaleThresholdTrackbar)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MADNumericUpDown)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private TableLayoutPanel ApplicationLayoutPanel;
    private Label label1;
    private DoubleBufferedPanel CaptureBox;
    private FlowLayoutPanel ImageSettingsLayout;
    private CheckBox GrayscaleCheckbox;
    private CheckBox InvertCheckbox;
    private Label label2;
    private Label GrayscaleThresholdLabel;
    private TrackBar GrayscaleThresholdTrackbar;
    private FlowLayoutPanel DetectedTextLayout;
    private Label label3;
    private Label DetectedTextResultLabel;
    private FlowLayoutPanel flowLayoutPanel1;
    private Button SelectScreenAreaButton;
    private Button OpenServer;
    private CheckBox ExcludeColorsCheckbox;
    private FlowLayoutPanel flowLayoutPanel2;
    private Label label4;
    private Label ColorHexValueLabel;
    private Button PickColorBtn;
    private Label label5;
    private NumericUpDown MADNumericUpDown;
    private CheckBox CropCheckbox;
}
