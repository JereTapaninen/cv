# OptiNovel

This project is meant for reading text, specifically Japanese (though every languages might work, given enough tesseract data), from any application or game on-screen. OptiNovel will then launch a HTTP server, which will output the read text from the screen into a webpage acting as the HTTP client. This way the user can use browser extensions like rikaikun to read the text from the webpage.

## Running the application

### Pre-requisites:

- Visual Studio 2022 (might be able to downgrade or upgrade as well)
- .NET 6
- Tesseract data for the wanted language, Japanese recommended (cannot be included due to copyright)

### Steps:

1. Clone the project
2. Paste your language tesseract data to OptiNovel.UI/tessdata folder
3. Open the project with Visual Studio 2022
4. Debug the application.
