# Sorters

Sorters is a C# WPF application that can be used to test out different sorting algorithms, like bubblesort and such, visually.

## Running the application

### Pre-requisites:

- Visual Studio 2022
- .NET 5

### Steps:

1. Clone the project
2. Open the project with Visual Studio 2022
3. Debug the application
