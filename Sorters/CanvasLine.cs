﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace Sorters {
    public sealed class CanvasLine : INotifyPropertyChanged {
        private int height;
        /// <summary>
        /// AKA Value
        /// </summary>
        public int Height {
            get => height;
            set {
                if (height != value) {
                    height = value;
                    PropertyChanged.Invoke(this, new(nameof(Height)));
                }
            }
        }

        private Brush stroke = Brushes.White;
        public Brush Stroke {
            get => stroke;
            set {
                if (stroke != null) {
                    stroke = value;
                    PropertyChanged.Invoke(this, new(nameof(Stroke)));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public CanvasLine()
            => PropertyChanged += new((object sender, PropertyChangedEventArgs e) => { });
    }
}
