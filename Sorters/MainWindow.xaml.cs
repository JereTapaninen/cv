﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System;
using Sorters.Algorithm;
using System.Threading;

namespace Sorters {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged {
        private const uint LineCount = 100;

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly ISortingAlgorithm BogoSort = new BogoSort();
        private readonly ISortingAlgorithm SelectionSort = new SelectionSort();
        private readonly ISortingAlgorithm BubbleSort = new BubbleSort();
        private readonly ISortingAlgorithm InsertionSort = new InsertionSort();
        private readonly ISortingAlgorithm PancakeSort = new PancakeSort();

        private Brush canvasBackground = Brushes.Black;
        public Brush CanvasBackground {
            get => canvasBackground;
            set {
                if (canvasBackground != value) {
                    canvasBackground = value;
                    PropertyChanged.Invoke(this, new(nameof(CanvasBackground)));
                }
            }
        }

        private volatile bool sorting;
        public bool Sorting {
            get => sorting;
            set {
                if (sorting != value) {
                    sorting = value;
                    PropertyChanged.Invoke(this, new(nameof(Sorting)));
                }
            }
        }

        private volatile bool sortThreadRunning = false;
        public bool SortThreadRunning {
            get => sortThreadRunning;
            set {
                if (sortThreadRunning != value) {
                    sortThreadRunning = value;
                    PropertyChanged.Invoke(this, new(nameof(SortThreadRunning)));
                }
            }
        }

        public Thread SortThread { get; set; }

        private int pass;
        public int Pass {
            get => pass;
            set {
                if (pass != value) {
                    pass = value;
                    PropertyChanged.Invoke(this, new(nameof(Pass)));
                }
            }
        }

        // Interval between passes, in ms
        public int Interval = 10;

        public ObservableCollection<CanvasLine> Lines { get; set; } = new();

        public MainWindow() {
            InitializeComponent();

            CreateLines();
            Lines.Shuffle();
        }

        private void CreateLines() {
            for (int lineIndex = 0; lineIndex < LineCount; lineIndex++) {
                Lines.Add(new CanvasLine() {
                    Height = lineIndex // set height to line index for now, jumble up later
                });
            }
        }

        private void StartSort(ISortingAlgorithm sortingAlgorithm) {
            Pass = 0;
            Sorting = true;
            Lines.Shuffle();

            SortThread = new Thread(() => {
                SortThreadRunning = true;

                while (Sorting && !Dispatcher.Invoke(() => Lines.Sorted())) {
                    Dispatcher.Invoke(() => sortingAlgorithm.Pass(Lines));

                    Pass++;

                    Thread.Sleep(Interval);
                }

                sortingAlgorithm.Clear();

                SortThreadRunning = false;
            });
            SortThread.Start();
        }

        private void StartBogoSort(object sender, RoutedEventArgs e)
            => StartSort(BogoSort);

        private void StartSelectionSort(object sender, RoutedEventArgs e)
            => StartSort(SelectionSort);

        private void StartBubbleSort(object sender, RoutedEventArgs e)
            => StartSort(BubbleSort);

        private void StartInsertionSort(object sender, RoutedEventArgs e)
            => StartSort(InsertionSort);

        private void StartPancakeSort(object sender, RoutedEventArgs e)
            => StartSort(PancakeSort);

        private void StopSort(object sender, RoutedEventArgs e)
            => Sorting = false;
    }
}

