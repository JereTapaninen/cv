﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Sorters {
    public class SortedConverter : IValueConverter {
        public object Convert(object values, Type targetType, object parameter, CultureInfo culture) {
            var canvasLines = (ObservableCollection<CanvasLine>)values;

            return canvasLines.Sorted().ToString();
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
            => throw new NotImplementedException();
    }
}
