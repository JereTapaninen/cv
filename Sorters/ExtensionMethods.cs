﻿using System;
using System.Collections.Generic;

namespace Sorters {
    public static class ExtensionMethods {
        public static void Shuffle<T>(this IList<T> list) {
            int n = list.Count;
            while (n > 1) {
                n--;

                int k = TrueRandom.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static bool Sorted(this IList<CanvasLine> list) {
            for (var i = 1; i < list.Count; i++) {
                if (list[i - 1].Height > list[i].Height) {
                    return false;
                }
            }

            return true;
        }

        public static void Flip(this IList<CanvasLine> list, int length) {
            CanvasLine temp;
            int start = 0;
            while (start < length) {
                temp = list[start];
                list[start] = list[length];
                list[length] = temp;
                start++;
                length--;
            }
        }
    }
}
