﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;

namespace Sorters {
    public class XPositionConverter : IMultiValueConverter {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
            var list = (ObservableCollection<CanvasLine>)values[1];
            var index = (double)list.IndexOf((CanvasLine)values[0]);
            var canvasWidth = (double)values[2];
            var strokeThickness = canvasWidth / list.Count;
            return index * strokeThickness;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
            => throw new NotImplementedException();
    }
}
