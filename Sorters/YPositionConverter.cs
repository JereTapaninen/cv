﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Sorters {
    public class YPositionConverter : IMultiValueConverter {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
            var canvasLine = (CanvasLine)values[0];
            var canvasLines = (ObservableCollection<CanvasLine>)values[1];
            var canvasHeight = (double)values[2];

            var buffer = 125;
            var maxValue = canvasLines.Select(line => line.Height).Max() + buffer;
            var ratio = (double)canvasLine.Height / maxValue;

            return canvasHeight - (canvasHeight * ratio);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
            => throw new NotImplementedException();
    }
}
