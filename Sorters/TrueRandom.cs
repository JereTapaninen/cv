﻿using System;

namespace Sorters {
    public static class TrueRandom {
        private static readonly Random Rand = new();
        private static readonly object SyncLock = new();

        public static int Next(int max) {
            lock (SyncLock) {
                return Rand.Next(max);
            }
        }
    }
}
