﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Sorters.Algorithm {
    public sealed class SelectionSort : ISortingAlgorithm {
        private int sortIndex = 0;

        public void Pass(ObservableCollection<CanvasLine> lines) {
            var remainingHeights = lines.Skip(sortIndex).Select(val => val.Height).ToList();
            var min = remainingHeights.Min();
            var minIndex = remainingHeights.IndexOf(min);

            var temp = lines[sortIndex];
            lines[sortIndex] = lines[sortIndex + minIndex];
            lines[sortIndex + minIndex] = temp;

            sortIndex++;
        }

        public void Clear()
            => sortIndex = 0;
    }
}
