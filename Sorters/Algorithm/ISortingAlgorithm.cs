﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Sorters.Algorithm {
    public interface ISortingAlgorithm {
        void Pass(ObservableCollection<CanvasLine> lines);
        void Clear();
    }
}
