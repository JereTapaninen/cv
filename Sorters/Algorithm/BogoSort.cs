﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Sorters.Algorithm {
    public sealed class BogoSort : ISortingAlgorithm {
        public void Pass(ObservableCollection<CanvasLine> lines)
            => lines.Shuffle();

        public void Clear() { }
    }
}
