﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Sorters.Algorithm {
    public sealed class BubbleSort : ISortingAlgorithm {
        public void Clear() { }

        public void Pass(ObservableCollection<CanvasLine> lines) {
            for (var i = 0; i < lines.Count - 1; i++) {
                if (lines[i].Height > lines[i + 1].Height) {
                    var temp = lines[i];
                    lines[i] = lines[i + 1];
                    lines[i + 1] = temp;
                }
            }
        }
    }
}
