﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorters.Algorithm {
    public sealed class PancakeSort : ISortingAlgorithm {
        private int currSize = 0;

        public void Clear() {
            currSize = 0;
        }

        public void Pass(ObservableCollection<CanvasLine> lines) {
            if (currSize <= 1)
                currSize = lines.Count;

            var asHeights = lines.Take(currSize - 1).Select(line => line.Height).ToList();
            var min = asHeights.Max();
            var mi = asHeights.IndexOf(min);

            lines.Flip(mi);
            lines.Flip(currSize - 1);

            currSize--;
        }
    }
}
