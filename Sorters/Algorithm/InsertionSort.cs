﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Sorters.Algorithm {
    public sealed class InsertionSort : ISortingAlgorithm {
        public void Clear() {
        }

        public void Pass(ObservableCollection<CanvasLine> lines) {
            for (var i = 1; i < lines.Count; i++) {
                var element = lines[i];

                int descend = i - 1;
                while (descend >= 1 && element.Height > lines[descend].Height) {
                    descend--;
                }

                lines[i] = lines[descend];
                lines[descend] = element;
            }
        }
    }
}
