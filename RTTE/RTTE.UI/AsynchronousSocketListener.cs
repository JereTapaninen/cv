﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RTTE.UI;
public class StateObject {
    public const int BUFFERSIZE = 1024;

    public Socket WorkSocket = null;
    public byte[] Buffer = new byte[BUFFERSIZE];
    public StringBuilder Writer = new StringBuilder();
}

public class AsynchronousSocketListener {
    private const int PORT = 11000;

    private ManualResetEvent AllDone { get; } = new(false);

    private string Headers { get; } = "HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\n";
    private string BadHeaders { get; } = "HTTP/1.1 400 Bad Request\r\nAccess-Control-Allow-Origin: *\r\n\n";

    private TextViewForm OwnerForm { get; }

    public AsynchronousSocketListener(TextViewForm ownerForm) {
        OwnerForm = ownerForm;
    }

    public void StartListening() {
        Thread listeningThread = new(() => {
            byte[] bytes = new byte[1024];

            IPAddress ipAddress = GetLocalIPAddress();
            IPEndPoint ipEndPoint = new(ipAddress, PORT);

            using Socket listener = new(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try {
                listener.Bind(ipEndPoint);
                listener.Listen(1000);

                while (!OwnerForm.CancellationToken.IsCancellationRequested) {
                    _ = AllDone.Reset();

                    Console.WriteLine("Waiting for a connection on IP {0} and Port {1}...", ipAddress.ToString(), PORT);

                    _ = listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    _ = AllDone.WaitOne();
                }
            } catch (Exception e) {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        });
        listeningThread.Start();
    }

    private void AcceptCallback(IAsyncResult ar) {
        _ = AllDone.Set();

        var listener = (Socket)ar.AsyncState;
        var handler = listener.EndAccept(ar);

        StateObject state = new() {
            WorkSocket = handler
        };

        try {
            _ = handler.BeginReceive(state.Buffer, 0, StateObject.BUFFERSIZE, 0, new AsyncCallback(ReadCallback), state);
        } catch (SocketException) {
            Console.WriteLine("Client closed the connection forcibly");
        }
    }

    private void ReadCallback(IAsyncResult ar) {
        StateObject state = (StateObject)ar.AsyncState;
        Socket handler = state.WorkSocket;

        int bytesRead = handler.EndReceive(ar);

        if (bytesRead > 0) {
            string content = Encoding.UTF8.GetString(state.Buffer, 0, bytesRead);
            string apiURL = content
                .Split(new string[] { "GET " }, StringSplitOptions.None)[1]
                .Split(new string[] { " HTTP" }, StringSplitOptions.None)[0];

            Console.WriteLine("Received the following API request: {0}", apiURL);

            string dataToSend = Headers;

            if (apiURL.Contains("/text")) {
                dataToSend += OwnerForm.CurrentText;
            } else {
                dataToSend = BadHeaders;
            }

            SendResponse(handler, dataToSend);
        }
    }

    private void SendResponse(Socket handler, string data) {
        byte[] byteData = Encoding.UTF8.GetBytes(data);

        _ = handler.BeginSend(byteData, 0, byteData.Length, 0, new(SendCallback), handler);
    }

    private void SendCallback(IAsyncResult ar) {
        try {
            Socket handler = (Socket)ar.AsyncState;

            int bytesSent = handler.EndSend(ar);
            Console.WriteLine("Sent {0} bytes to client", bytesSent);

            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
        } catch (Exception e) {
            Console.WriteLine("Exception: {0}", e);
        }
    }

    private static IPAddress GetLocalIPAddress() {
        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

        foreach (IPAddress ip in host.AddressList) {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
                return ip;
        }

        throw new Exception("No network adapters with an IPv4 address in the system!");
    }
}
