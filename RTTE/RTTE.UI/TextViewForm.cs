﻿using RTTE.Library.Common.Interfaces;
using RTTE.Library.TextReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTTE.UI {
    public partial class TextViewForm : Form {
        private ITextReader TextReader { get; }
        public CancellationTokenSource CancellationToken { get; }

        private Thread UpdateThread { get; set; }
        public string CurrentText { get; private set; } = string.Empty;
        private AsynchronousSocketListener HTTPServer { get; }

        public TextViewForm(ITextReader textReader) {
            InitializeComponent();

            HTTPServer = new(this);

            TextReader = textReader;
            CancellationToken = new();
        }

        private void TextViewForm_Load(object sender, EventArgs e) {
            UpdateThread = new(Updater);
            UpdateThread.Start();

            HTTPServer.StartListening();
        }

        private void Updater() {
            while (!CancellationToken.IsCancellationRequested) {
                CurrentText = TextReader.Read();
                _ = Invoke(new MethodInvoker(() => textView1.Text = CurrentText));
                Thread.Sleep(1000 / 60);
            }
        }
    }
}
