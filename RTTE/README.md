# Real-Time Text Extractor

(Note for the CV page version: this is my project, but this is visible on another GitHub account of mine. This is the "memory reading" version of OptiNovel, though this also has a kind of optical character recognition system of its own. **This project also includes unit tests!!**)

RTTE is used to extract text of any language from any Windows process. Primarily it is meant to be used to extract text from games such as Visual Novels.

## Running the application

### Pre-requisites:

- Visual Studio 2022 (might be able to downgrade or upgrade as well)
- .NET 6
- Tesseract data for the wanted language, Japanese recommended (cannot be included due to copyright)

### Steps:

1. Clone the project
2. Paste your language tesseract data to RTTE.Library/tessdata folder
3. Open the project with Visual Studio 2022
4. Debug the application.

## Copyrighted materials

The icons used in RTTE are made by [omercetin](https://www.deviantart.com/omercetin) belonging to the [Pixelophilia2 Iconset](https://www.deviantart.com/omercetin/art/PixeloPhilia2-166570194). It is licensed under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/).

**HEAVILY IN DEVELOPMENT, NO RELEASE.**

