# Blobs

This project is meant as a Neural Network/Genetic Algorithm test using JS/TS and Canvas.
The code is optimized for performance, and might not (will not) follow functional programming guidelines, and will not be very clean.

Also, this is WIP, so there will be a lot of code that does indeed require cleaning, and a lot of code that has been commented out.

Nevertheless, this shows more of my game-programming side... And of course web development capabilities without React.

## Running the application

### Pre-requisites:

- Node Version Manager

    or

- Node 19.3 or later and NPM 9.2 or later.

### Steps:

1. Clone the project
2. (With NVM only) Run `nvm use`
3. Run `npm install`
4. Run `npm watch` to launch the development environment
