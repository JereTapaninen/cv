import { v4 as uuidv4 } from "uuid";
import NeuralNetwork from "./NeuralNetwork";
import Random from "./Random";

const zMap: { [key: number]: number } = {
    [0]: 1.5 * Math.PI,
    [1]: 2 * Math.PI,
    [2]: Math.PI / 2,
    [3]: Math.PI
};

const fillCircle = (
    context: CanvasRenderingContext2D,
    x: number,
    y: number,
    radius: number
) => {
    context.beginPath();
    context.arc(x, y, radius / 2, 0, 2 * Math.PI);
    context.fill();
    context.closePath();
};

const maxSpeed = 0.2;

const nSecondsHavePassed = (seconds: number, currentTimeMs: number): boolean =>
    currentTimeMs !== 0 && currentTimeMs / (seconds * 1000) >= 1;

class TimedEvent {
    private _everyNSecond: number;
    private _lastTime: number;

    constructor(seconds: number) {
        this._everyNSecond = seconds;
        this._lastTime = performance.now();
    }

    do(fn: () => void) {
        const currentTime = performance.now();

        const timeDiff = currentTime - this._lastTime;

        if (timeDiff >= this._everyNSecond * 1000) {
            fn();

            this._lastTime = currentTime;
        }
    }
}

const defaultBreedingCooldown = 1;
const defaultAttackCooldown = 1;
const defaultEatingCooldown = 1;
const defaultSatiation = 20;
const defaultRange = 128;
const defaultDamage = 10;
const defaultDeadHealth = 4; // how many bites can be taken out of
const satiationPerBite = 5;

export class Entity {
    public id: string;
    public species: number;
    public x: number;
    public y: number;
    public direction;
    public generation: number;
    public neuralNetwork: NeuralNetwork;

    public readonly debugColor: string;
    public readonly color: string;
    public readonly deadColor: string;

    public intersectingEntities: Entity[];
    public health: number;
    public geneticSize: number;
    public breedingCooldown: number;
    public attackCooldown: number;
    public eatingCooldown: number;
    public satiation: number;
    public age: number;
    public geneticDamage: number;
    public deadHealth: number;

    private _stayDead = false;
    private _everySecondEvent: TimedEvent = new TimedEvent(1);
    private _decayEvent: TimedEvent = new TimedEvent(5);

    // SIZE
    get maximumSize() {
        return 64;
    }

    get minimumSize() {
        return 8;
    }

    get size() {
        return Math.max(
            this.minimumSize,
            Math.min(this.maximumSize, this.geneticSize * (this.deadHealth / 4))
        );
    }

    // HEALTH
    get maximumHealth() {
        return 20 * (this.size / this.maximumSize);
    }

    get minimumHealth() {
        return 0;
    }

    get speed() {
        const sizeFactor = 1 - this.size / this.maximumSize;
        const sizeFactorRescaled = (1 - 0.33) * (sizeFactor - 0) + 0.33;

        return 1 * (this.health / this.maximumHealth) * sizeFactorRescaled;
    }

    // SATIATION
    get maximumSatiation() {
        return defaultSatiation;
    }

    // AGE
    get maximumAge() {
        return 100;
    }

    // DAMAGE AND ATTACk
    get damage() {
        return (
            this.geneticDamage *
            (this.health / this.maximumHealth) *
            (this.size / this.maximumSize)
        );
    }

    get canAttack() {
        return !this.isDead && this.attackCooldown <= 0;
    }

    get canEat() {
        return (
            !this.isDead &&
            this.eatingCooldown <= 0 &&
            this.satiation < this.maximumSatiation
        );
    }

    get remove() {
        return this.isDead && this.deadHealth <= 0;
    }

    get isDead() {
        const deadness =
            this.health <= this.minimumHealth ||
            this.satiation <= 0 ||
            this.age >= this.maximumAge;
        this._stayDead = this._stayDead || deadness;
        return this._stayDead; // this.age >= this.maximumAge;
    }

    get canBreed() {
        return !this.isDead && this.breedingCooldown <= 0;
    }

    get range() {
        const sizeFactor = this.size / this.maximumSize;
        return defaultRange * sizeFactor;
    }

    isFertileWith(entity: Entity) {
        const difference = Math.abs(entity.species - this.species);
        const satiationPercentage =
            (1 - 0.25) * (this.satiation / this.maximumSatiation - 0) + 0.25;
        const differencePercentage = 1 - difference / 360;
        const differencePercentageRescaled =
            (1 - 0.25) * (differencePercentage - 0) + 0.25;
        const agePercentage = this.age / this.maximumAge;

        return (
            Math.random() <=
            0.95 *
                Math.pow(differencePercentage, 4) *
                Math.pow(agePercentage, 0.2)
        );
    }

    constructor(canvasWidth: number, canvasHeight: number, generation: number) {
        this.deadHealth = defaultDeadHealth;
        this.geneticSize = 32;

        this.id = uuidv4();
        this.species = Random.getRandomInt(0, 360);
        this.color = `hsl(${this.species}, 100%, 75%)`;
        this.deadColor = `hsl(${this.species}, 100%, 10%)`;
        this.debugColor = `hsl(${this.species}, 100%, 50%)`;
        this.x = Random.getRandom(0, canvasWidth);
        this.y = Random.getRandom(0, canvasHeight);
        this.geneticDamage = defaultDamage;
        this.age = 0;
        this.health = this.maximumHealth;
        this.generation = generation;
        this.breedingCooldown = 0;
        this.attackCooldown = 0;
        this.eatingCooldown = 0;
        this.satiation = defaultSatiation;
        this.direction = 0;
        this.intersectingEntities = [];
        this.neuralNetwork = new NeuralNetwork(12, [13, 13, 13, 13], 3);
    }

    public eat(entity: Entity | null) {
        if (
            entity === null ||
            !entity.isDead ||
            this.isDead ||
            this.id === entity.id ||
            !this.canEat ||
            entity.deadHealth <= 0
        )
            return;

        this.eatingCooldown = defaultEatingCooldown;

        this.satiation = Math.min(
            this.maximumSatiation,
            this.satiation + satiationPerBite
        );
        entity.deadHealth = Math.max(0, entity.deadHealth - 1);
    }

    public attack(entity: Entity | null) {
        if (entity === null || this.id === entity.id || !this.canAttack) return;

        this.attackCooldown = defaultAttackCooldown;

        entity.health -= this.damage;

        if (!entity.isDead) this.health -= entity.damage;
    }

    public breed(
        otherEntity: Entity | null,
        canvasWidth: number,
        canvasHeight: number
    ): Entity | null {
        if (
            otherEntity === null ||
            !this.canBreed ||
            !otherEntity.canBreed ||
            !this.isFertileWith(otherEntity) ||
            !otherEntity.isFertileWith(this)
        )
            return null;

        this.breedingCooldown = defaultBreedingCooldown;
        otherEntity.breedingCooldown = defaultBreedingCooldown;

        if (
            !this.isFertileWith(otherEntity) ||
            !otherEntity.isFertileWith(this)
        ) {
            return null;
        }

        const mutationChance = 0.01;

        const childEntity = new Entity(
            canvasWidth,
            canvasHeight,
            Math.max(otherEntity.generation, this.generation) + 1
        );
        childEntity.x = (this.x + otherEntity.x) / 2;
        childEntity.y = (this.y + otherEntity.y) / 2;
        childEntity.geneticDamage =
            (this.geneticDamage + otherEntity.geneticDamage) / 2;
        const mutatedSize =
            Math.random() < mutationChance
                ? Boolean(Math.round(Math.random()))
                    ? 1
                    : -1
                : 0;
        childEntity.geneticSize = Math.max(
            8,
            Math.min(
                64,
                mutatedSize + (this.geneticSize + otherEntity.geneticSize) / 2
            )
        );
        childEntity.species = (this.species + otherEntity.species) / 2;
        childEntity.neuralNetwork = this.neuralNetwork.cross(
            otherEntity.neuralNetwork
        );
        return childEntity;
    }

    public draw(
        context: CanvasRenderingContext2D,
        _currentTimeHundredMs: number,
        _deltaTime: number,
        _canvasWidth: number,
        _canvasHeight: number
    ) {
        if (!this.isDead) {
            // draw raycast
            const raycastX = this.range * Math.cos(this.direction);
            const raycastY = this.range * Math.sin(this.direction);

            const xOffset = (this.size / 2) * Math.cos(this.direction);
            const yOffset = (this.size / 2) * Math.sin(this.direction);

            context.strokeStyle =
                this.intersectingEntities.length > 0
                    ? this.color
                    : this.deadColor;
            context.lineWidth = 1;

            context.beginPath();
            context.moveTo(this.x + xOffset, this.y + yOffset);
            context.lineTo(this.x + raycastX, this.y + raycastY);
            context.stroke();
            context.closePath();
        }

        context.fillStyle = this.isDead
            ? this.deadColor
            : this.intersectingEntities.length > 0
            ? this.debugColor
            : this.color;

        fillCircle(context, this.x, this.y, this.size);

        this._everySecondEvent.do(() => {
            this.age++;
            this.breedingCooldown--;
            this.attackCooldown--;
            this.eatingCooldown--;
            this.satiation--;
        });
        this._decayEvent.do(() => {
            if (this.isDead) {
                this.deadHealth--;
            }
        });
    }

    public moveTo(x: number, y: number) {
        this.direction = Math.atan2(this.y - y, this.x - x) + Math.PI;
        this.x = x;
        this.y = y;
    }
}

const transformPosition = (
    entityId: string,
    x: number,
    y: number,
    size: number,
    deltaTime: number,
    closestEntity: Entity | null
): { x: number; y: number } => {
    if (closestEntity === null || closestEntity.id === entityId)
        return { x, y };

    const sizes = size / 2 + closestEntity.size / 2;
    const deltaX = x - closestEntity.x;
    const deltaY = y - closestEntity.y;

    const intersectsX = Math.abs(x - closestEntity.x) - sizes <= 0;
    const intersectsY = Math.abs(y - closestEntity.y) - sizes <= 0;

    if (intersectsX && intersectsY)
        return {
            x: intersectsX ? x + 0.1 * Math.sign(deltaX) * deltaTime : x,
            y: intersectsY ? y + 0.1 * Math.sign(deltaY) * deltaTime : y
        };

    return { x, y };
};

const getClosestEntity = (
    entityId: string,
    x: number,
    y: number,
    entities: Entity[]
): Entity | null => {
    let closest: Entity | null = null;
    let closestDistance = Infinity;

    for (
        let entityIndex = entities.length - 1;
        entityIndex >= 0;
        entityIndex--
    ) {
        const currentEntity = entities[entityIndex];

        if (entityId === currentEntity.id) continue;

        const deltaX = x - currentEntity.x;
        const deltaY = y - currentEntity.y;
        const distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

        closest = distance < closestDistance ? currentEntity : closest;
        closestDistance =
            distance < closestDistance ? distance : closestDistance;
    }

    return closest;
};

const raycast = (
    entityId: string,
    x: number,
    y: number,
    size: number,
    direction: number,
    range: number,
    entities: Entity[]
): Entity[] => {
    const intersectingEntities: Entity[] = [];

    for (
        let entityIndex = entities.length - 1;
        entityIndex >= 0;
        entityIndex--
    ) {
        const currentEntity = entities[entityIndex];

        if (entityId === currentEntity.id) continue;

        const xOffset = (size / 2) * Math.cos(direction);
        const yOffset = (size / 2) * Math.sin(direction);

        const deltaX = x + xOffset - currentEntity.x;
        const deltaY = y + yOffset - currentEntity.y;
        const distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

        if (distance > range) continue;

        const angleBetweenPoints = Math.atan2(
            currentEntity.y - y,
            currentEntity.x - x
        );
        const angle = Math.abs(direction - angleBetweenPoints);

        if (angle < currentEntity.size) {
            intersectingEntities.push(currentEntity);
        }
    }

    return intersectingEntities;
};

const absoluteToRelativeOnCanvas = (
    xOrY: number,
    canvasWidthOrHeight: number
) => (xOrY + canvasWidthOrHeight) % canvasWidthOrHeight;

export default class Simulation {
    public entities: Entity[];

    constructor(
        entitiesCount: number,
        canvasWidth: number,
        canvasHeight: number
    ) {
        this.entities = new Array(entitiesCount)
            .fill(1)
            .map(() => new Entity(canvasWidth, canvasHeight, 0));
    }

    public moveEntity(
        entity: Entity,
        direction: number,
        speed: number,
        deltaTime: number,
        canvasWidth: number,
        canvasHeight: number,
        closestEntity: Entity | null
    ) {
        const directionInRadians = direction * 2 * Math.PI;

        const actualSpeed = speed * maxSpeed * entity.speed;

        const x = actualSpeed * Math.cos(directionInRadians) * deltaTime;
        const y = actualSpeed * Math.sin(directionInRadians) * deltaTime;

        const { x: actualX, y: actualY } = transformPosition(
            entity.id,
            entity.x + x,
            entity.y + y,
            entity.size,
            deltaTime,
            closestEntity
        );

        const actualXCanvas = absoluteToRelativeOnCanvas(actualX, canvasWidth);
        const actualYCanvas = absoluteToRelativeOnCanvas(actualY, canvasHeight);

        entity.moveTo(actualXCanvas, actualYCanvas);
    }

    public updateEntity(
        entity: Entity,
        deltaTime: number,
        canvasWidth: number,
        canvasHeight: number
    ) {
        const closestEntity = getClosestEntity(
            entity.id,
            entity.x,
            entity.y,
            this.entities
        );

        const intersectingEntities = raycast(
            entity.id,
            entity.x,
            entity.y,
            entity.size,
            entity.direction,
            entity.range,
            this.entities
        );

        entity.intersectingEntities = intersectingEntities;

        const closestIntersectingEntity = getClosestEntity(
            entity.id,
            entity.x,
            entity.y,
            intersectingEntities
        );

        const [direction, speed, action] = entity.neuralNetwork.predict([
            entity.species,
            entity.x,
            entity.y,
            entity.age,
            entity.canBreed ? 1000 : -1000,
            entity.canAttack ? 1000 : -1000,
            entity.canEat ? 1000 : -1000,
            closestIntersectingEntity === null
                ? -100
                : closestIntersectingEntity.isDead
                ? 100
                : 0,
            closestIntersectingEntity?.x ?? -100,
            closestIntersectingEntity?.y ?? -100,
            closestIntersectingEntity?.species ?? -100,
            closestIntersectingEntity === null
                ? -100
                : closestIntersectingEntity.health * 50
        ]);

        if (
            isNaN(entity.species) ||
            isNaN(entity.x) ||
            isNaN(entity.y) ||
            isNaN(entity.age) ||
            isNaN(entity.satiation) ||
            isNaN(
                closestIntersectingEntity === null
                    ? -100
                    : closestIntersectingEntity.health * 50
            )
        ) {
            console.log(closestIntersectingEntity?.health, entity);
        }

        const actualAction = Math.floor(action * 4);

        switch (actualAction) {
            case 3: // do nothing
                break;
            case 2: // try eating
                entity.eat(closestIntersectingEntity);
                break;
            case 1: // try attack
                entity.attack(closestIntersectingEntity);
                break;
            case 0: // try breed
                if (this.entities.length > 2000) break;

                const newEntity = entity.breed(
                    closestIntersectingEntity,
                    canvasWidth,
                    canvasHeight
                );
                if (newEntity !== null) this.entities.push(newEntity);
                break;
            default:
                throw new Error(
                    `Unsupported entity action ${actualAction}, from ${action}.`
                );
        }

        //if (Boolean(Math.round(doMove)))
        this.moveEntity(
            entity,
            direction,
            speed,
            deltaTime,
            canvasWidth,
            canvasHeight,
            closestEntity
        );
    }

    public draw(
        context: CanvasRenderingContext2D,
        currentTimeHundredMs: number,
        deltaTime: number,
        canvasWidth: number,
        canvasHeight: number
    ) {
        // context.rect(0, 0, canvasWidth, canvasHeight);
        // context.strokeStyle = "#000000";
        // context.stroke();

        for (
            let entityIndex = this.entities.length - 1;
            entityIndex >= 0;
            entityIndex--
        ) {
            const entity = this.entities[entityIndex];

            if (entity.remove) {
                this.entities.splice(entityIndex, 1);
                continue;
            }

            if (!entity.isDead)
                this.updateEntity(entity, deltaTime, canvasWidth, canvasHeight);

            entity.draw(
                context,
                currentTimeHundredMs,
                deltaTime,
                canvasWidth,
                canvasHeight
            );
        }
    }
}
