const fma = (a: number, b: number, c: number) => {
    return a * b + c;
};

const sigmoid = (x: number) => {
    return 1 / (1 + Math.exp(-x));
};

export default class NeuralNetwork {
    inputNodeCount: number;
    hiddenNodeCount: number;
    outputNodeCount: number;
    weightsIH: number[][];
    weightsHO: number[][];
    biasH: number[];
    biasO: number[];

    constructor(
        inputNodeCount: number,
        hiddenNodeCount: number,
        outputNodeCount: number
    ) {
        this.inputNodeCount = inputNodeCount;
        this.hiddenNodeCount = hiddenNodeCount;
        this.outputNodeCount = outputNodeCount;

        this.weightsIH = Array(this.hiddenNodeCount)
            .fill(0)
            .map(() =>
                Array(this.inputNodeCount)
                    .fill(0)
                    .map(() => Math.random() * 2 - 1)
            );
        this.weightsHO = Array(this.outputNodeCount)
            .fill(0)
            .map(() =>
                Array(this.hiddenNodeCount)
                    .fill(0)
                    .map(() => Math.random() * 2 - 1)
            );
        this.biasH = Array(this.hiddenNodeCount)
            .fill(0)
            .map(() => Math.random() * 2 - 1);
        this.biasO = Array(this.outputNodeCount)
            .fill(0)
            .map(() => Math.random() * 2 - 1);
    }

    public predict(input: number[]): number[] {
        const hidden = this.weightsIH.map((weights, index) => {
            let sum = this.biasH[index];
            for (let i = 0; i < weights.length; i++) {
                sum = fma(weights[i], input[i], sum);
            }
            return sigmoid(sum);
        });

        const output = this.weightsHO.map((weights, index) => {
            let sum = this.biasO[index];
            for (let i = 0; i < weights.length; i++) {
                sum = fma(weights[i], hidden[i], sum);
            }
            return sigmoid(sum);
        });

        return output;
    }

    public cross(other: NeuralNetwork): NeuralNetwork {
        const child = new NeuralNetwork(
            this.inputNodeCount,
            this.hiddenNodeCount,
            this.outputNodeCount
        );

        child.weightsHO = this.weightsHO.map((weightHO, index) =>
            weightHO.map(
                (inner, innerIndex) =>
                    (inner + other.weightsHO[index][innerIndex]) / 2
            )
        );
        child.weightsIH = this.weightsIH.map((weightIH, index) =>
            weightIH.map(
                (inner, innerIndex) =>
                    (inner + other.weightsIH[index][innerIndex]) / 2
            )
        );
        child.biasH = this.biasH.map(
            (b, index) => (b + other.biasH[index]) / 2
        );
        child.biasO = this.biasO.map(
            (b, index) => (b + other.biasO[index]) / 2
        );
        return child;
    }
}
