export default abstract class Random {
    public static getRandomInt(min: number, max: number): number {
        const minCeil = Math.ceil(min);
        const maxFloor = Math.floor(max);
        return Math.floor(Math.random() * (maxFloor - minCeil) + minCeil);
    }

    public static getRandom(min: number, max: number): number {
        return Math.random() * (max - min) + min;
    }
}
