import Random from "./Random";

const fma = (a: number, b: number, c: number) => {
    return a * b + c;
};

const sigmoid = (x: number) => {
    return 1 / (1 + Math.exp(-x));
};

export default class NeuralNetwork {
    inputNodeCount: number;
    hiddenNodeCounts: number[];
    outputNodeCount: number;
    weightsIH: number[][][];
    weightsHO: number[][];
    biasH: number[][];
    biasO: number[];

    constructor(
        inputNodeCount: number,
        hiddenNodeCounts: number[],
        outputNodeCount: number
    ) {
        this.inputNodeCount = inputNodeCount;
        this.hiddenNodeCounts = hiddenNodeCounts;
        this.outputNodeCount = outputNodeCount;

        this.weightsIH = [];
        this.weightsHO = [];
        this.biasH = [];

        let previousNodeCount = inputNodeCount;

        for (let i = 0; i < hiddenNodeCounts.length; i++) {
            const hiddenNodeCount = hiddenNodeCounts[i];
            this.weightsIH.push(
                new Array(hiddenNodeCount)
                    .fill(0)
                    .map(() =>
                        new Array(previousNodeCount)
                            .fill(0)
                            .map(() => Math.random() * 2 - 1)
                    )
            );
            this.biasH.push(
                new Array(hiddenNodeCount)
                    .fill(0)
                    .map(() => Math.random() * 2 - 1)
            );
            previousNodeCount = hiddenNodeCount;
        }
        this.weightsHO = Array(this.outputNodeCount)
            .fill(0)
            .map(() =>
                Array(previousNodeCount)
                    .fill(0)
                    .map(() => Math.random() * 2 - 1)
            );
        this.biasO = Array(this.outputNodeCount)
            .fill(0)
            .map(() => Math.random() * 2 - 1);
    }

    public predict(input: number[]): number[] {
        let hidden = input;
        for (let i = 0; i < this.hiddenNodeCounts.length; i++) {
            const layerHidden = this.weightsIH[i].map((weights, index) => {
                let sum = this.biasH[i][index];
                for (let i = 0; i < weights.length; i++) {
                    sum = fma(weights[i], hidden[i], sum);
                }
                return sigmoid(sum);
            });
            hidden = layerHidden;
        }

        const output = this.weightsHO.map((weights, index) => {
            let sum = this.biasO[index];
            for (let i = 0; i < weights.length; i++) {
                sum = fma(weights[i], hidden[i], sum);
            }
            return sigmoid(sum);
        });

        return output;
    }

    /*public cross(other: NeuralNetwork): NeuralNetwork {
        const child = new NeuralNetwork(
            this.inputNodeCount,
            this.hiddenNodeCounts,
            this.outputNodeCount
        );

        for (let i = 0; i < this.hiddenNodeCounts.length; i++) {
            child.weightsIH[i] = this.weightsIH[i].map((weightIH, index) =>
                weightIH.map(
                    (inner, innerIndex) =>
                        (inner + other.weightsIH[i][index][innerIndex]) / 2
                )
            );
            child.biasH[i] = this.biasH[i].map(
                (b, index) => (b + other.biasH[i][index]) / 2
            );
        }

        child.weightsHO = this.weightsHO.map((weightHO, index) =>
            weightHO.map(
                (inner, innerIndex) =>
                    (inner + other.weightsHO[index][innerIndex]) / 2
            )
        );
        child.biasO = this.biasO.map(
            (b, index) => (b + other.biasO[index]) / 2
        );
        return child;
    }*/

    public cross(other: NeuralNetwork): NeuralNetwork {
        const mutationChance = 0.01;

        const hiddenLayerCount = Math.max(
            1,
            Math.min(
                5,
                Math.floor(
                    (this.hiddenNodeCounts.length +
                        other.hiddenNodeCounts.length) /
                        2
                ) +
                    (Math.random() < mutationChance
                        ? Math.random() > 0.5
                            ? 1
                            : -1
                        : 0)
            )
        );
        const hiddenNodeCounts = new Array(hiddenLayerCount)
            .fill(0)
            .map((_, index) => {
                const parent1NodeCounts: number | null =
                    this.hiddenNodeCounts.length > index
                        ? this.hiddenNodeCounts[index]
                        : null;
                const parent2NodeCounts: number | null =
                    other.hiddenNodeCounts.length > index
                        ? other.hiddenNodeCounts[index]
                        : null;

                const parent1MaxNodes = this.hiddenNodeCounts.reduce(
                    (acc, curr) => (curr > acc ? curr : acc),
                    0
                );
                const parent2MaxNodes = other.hiddenNodeCounts.reduce(
                    (acc, curr) => (curr > acc ? curr : acc),
                    0
                );

                const maxHiddenNodesFromParent = Math.max(
                    parent1MaxNodes,
                    parent2MaxNodes
                );

                const minHiddenNodesFromParent = Math.min(
                    this.hiddenNodeCounts.reduce(
                        (acc, curr) => (curr < acc ? curr : acc),
                        parent1MaxNodes
                    ),
                    other.hiddenNodeCounts.reduce(
                        (acc, curr) => (curr < acc ? curr : acc),
                        parent2MaxNodes
                    )
                );

                const randomMin = Math.max(
                    1,
                    Math.max(
                        maxHiddenNodesFromParent - 1,
                        minHiddenNodesFromParent
                    )
                );

                return parent1NodeCounts !== null && parent2NodeCounts !== null
                    ? (parent1NodeCounts + parent2NodeCounts) / 2
                    : parent1NodeCounts !== null
                    ? parent1NodeCounts
                    : parent2NodeCounts !== null
                    ? parent2NodeCounts
                    : Random.getRandomInt(
                          randomMin,
                          Math.min(randomMin + 1, maxHiddenNodesFromParent)
                      );
            });

        const child = new NeuralNetwork(
            this.inputNodeCount,
            hiddenNodeCounts,
            this.outputNodeCount
        );

        for (let i = 0; i < hiddenNodeCounts.length; i++) {
            child.weightsIH[i] =
                this.weightsIH.length > i
                    ? this.weightsIH[i].map((weightIH, index) =>
                          weightIH.map((inner, innerIndex) =>
                              Math.max(
                                  -1,
                                  Math.min(
                                      1,
                                      (Math.random() < mutationChance
                                          ? Math.random() * 2 - 1
                                          : 0) +
                                          (other.weightsIH.length > i
                                              ? (inner +
                                                    other.weightsIH[i][index][
                                                        innerIndex
                                                    ]) /
                                                2
                                              : inner)
                                  )
                              )
                          )
                      )
                    : other.weightsIH.length > i
                    ? other.weightsIH[i].map((weightIH) =>
                          weightIH.map((inner) =>
                              Math.max(
                                  -1,
                                  Math.min(
                                      1,
                                      inner +
                                          (Math.random() < mutationChance
                                              ? Math.random() * 2 - 1
                                              : 0)
                                  )
                              )
                          )
                      )
                    : child.weightsIH[i];
            child.biasH[i] =
                this.biasH.length > i
                    ? this.biasH[i].map((b, index) =>
                          Math.max(
                              -1,
                              Math.min(
                                  1,
                                  (Math.random() < mutationChance
                                      ? Math.random() * 2 - 1
                                      : 0) +
                                      (other.biasH.length > i
                                          ? (b + other.biasH[i][index]) / 2
                                          : b)
                              )
                          )
                      )
                    : other.biasH.length > i
                    ? other.biasH[i].map((_, index) =>
                          Math.max(
                              -1,
                              Math.min(
                                  1,
                                  (Math.random() < mutationChance
                                      ? Math.random() * 2 - 1
                                      : 0) + other.biasH[i][index]
                              )
                          )
                      )
                    : child.biasH[i];
        }

        child.weightsHO = this.weightsHO.map((weightHO, index) =>
            weightHO.map((inner, innerIndex) =>
                Math.max(
                    -1,
                    Math.min(
                        1,
                        (Math.random() < mutationChance
                            ? Math.random() * 2 - 1
                            : 0) +
                            (inner + other.weightsHO[index][innerIndex]) / 2
                    )
                )
            )
        );
        child.biasO = this.biasO.map((b, index) =>
            Math.max(
                -1,
                Math.min(
                    1,
                    (Math.random() < mutationChance
                        ? Math.random() * 2 - 1
                        : 0) +
                        (b + other.biasO[index]) / 2
                )
            )
        );
        return child;
    }
}
