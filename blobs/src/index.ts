import Simulation, { Entity } from "./Simulation";

const onLoad = () => {
    const canvas: HTMLCanvasElement | null =
        document.querySelector<HTMLCanvasElement>("#screen");
    const context = canvas?.getContext("2d", { alpha: false });

    if (canvas === null || typeof context === "undefined" || context === null) {
        alert("Canvas element was not found or context could not be got!");
        return;
    }

    const setCanvasToParent = (canvasElement: HTMLCanvasElement) => {
        canvasElement.width = canvasElement.clientWidth;
        canvasElement.height = canvasElement.clientHeight;
    };

    const resizeObserver = new ResizeObserver(([canvasROE]) => {
        if (typeof canvas !== "undefined") {
            const canvasElement = canvasROE.target as HTMLCanvasElement;
            setCanvasToParent(canvasElement);
        }
    });

    resizeObserver.observe(canvas);
    setCanvasToParent(canvas);

    const simulation = new Simulation(1000, canvas.width, canvas.height);

    let msCalculationTime = performance.now();
    let hundredMs = 0;
    let previousFrames = 0;
    let currentFrames = 0;
    let frameCalculationTime = performance.now();
    let deltaTime = 0;
    let previousTime = performance.now();
    //let zoomLevel = 1;
    //let zoomX = canvas.width / 2;
    //let zoomY = canvas.height / 2;

    const updateAndDraw = () => {
        currentFrames++;

        const currentTime = performance.now();
        deltaTime = currentTime - previousTime;
        previousTime = currentTime;

        if (msCalculationTime < previousTime - 100) {
            msCalculationTime = previousTime;
            hundredMs++;
        }

        if (frameCalculationTime < previousTime - 1000) {
            frameCalculationTime = previousTime;

            previousFrames = currentFrames;
            currentFrames = 0;
        }

        context.fillStyle = "#cccccc";
        context.fillRect(0, 0, canvas.width, canvas.height);

        /*if (simulation.entities.length < 100) {
            const newEntities = new Array(100 - simulation.entities.length)
                .fill(0)
                .map(() => new Entity(canvas.width, canvas.height, 0));
            simulation.entities.push(...newEntities);
        }*/

        //context.translate(canvas.width / 2, canvas.height / 2);
        //context.scale(zoomLevel, zoomLevel);
        //context.translate(-zoomX, -zoomY);
        simulation.draw(
            context,
            hundredMs,
            deltaTime,
            canvas.width,
            canvas.height
        );
        //context.resetTransform();

        const largestGeneration = simulation.entities.reduce(
            (acc, curr) => (curr.generation > acc ? curr.generation : acc),
            0
        );

        context.font = "Bold 30px Verdana";
        context.fillStyle = "#000000";
        context.fillText(`FPS: ${previousFrames}`, 10, 40);
        context.fillText(`Generation: ${largestGeneration}`, 10, 80);
        context.fillText(`100 Ms elapsed: ${hundredMs}`, 10, 120);

        window.requestAnimationFrame(updateAndDraw);
    };

    /*canvas.addEventListener("wheel", (e) => {
        const rect = canvas.getBoundingClientRect();
        zoomX = e.clientX - rect.left;
        zoomY = e.clientY - rect.top;
        const delta = Math.sign(e.deltaY);
        zoomLevel = Math.max(0.1, zoomLevel - delta * 0.1);
    });*/
    window.requestAnimationFrame(updateAndDraw);
};

window.addEventListener("load", onLoad);
