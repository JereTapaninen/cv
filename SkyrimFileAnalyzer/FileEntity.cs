﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyrimFileAnalyzer;

internal class FileEntity {
    public string FileName { get; }
    public string Directory { get; }
    public string ComesFrom { get; }

    internal FileEntity(string fileName, string directory, string comesFrom) {
        FileName = fileName;
        Directory = directory;
        ComesFrom = comesFrom;
    }
}
