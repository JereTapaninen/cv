# Skyrim File Analyzer

Skyrim File Analyzer is a C# GUI application that checks if mods installed via Vortex override the original Skyrim files.
This can be helpful in modding to know if one model in the game is still using really old vanilla textures, for example.


## Running the application

### Pre-requisites:

- Skyrim installed, preferably Skyrim Special Edition
- Vortex Mod Manager installed
- Visual Studio 2022
- .NET 7

### Steps:

1. Clone the project
2. Open the project with Visual Studio 2022
3. Debug the application
