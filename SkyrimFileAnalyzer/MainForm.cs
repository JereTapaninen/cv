using LanguageExt;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Windows.Forms.Design;

namespace SkyrimFileAnalyzer;

public partial class MainForm : Form {
    private readonly string[] IgnoredFiles = new[] {
        "__folder_managed_by_vortex"
    };

    private const string VortexDeploymentJSONFileName = "Vortex.Deployment.json";
    private string SkyrimDataPath { get; set; } = "";
    private string VortexDeploymentJSONFile { get; set; } = "";

    private readonly DataTable Files = new();

    public MainForm() {
        InitializeComponent();

        DataColumn firstDataColumn = new() {
            DataType = typeof(string),
            ColumnName = "FileName",
            ReadOnly = true,
            Unique = false
        };

        DataColumn secondDataColumn = new() {
            DataType = typeof(string),
            ColumnName = "Directory",
            ReadOnly = true,
            Unique = false
        };

        DataColumn thirdDataColumn = new() {
            DataType = typeof(string),
            ColumnName = "ComesFrom",
            ReadOnly = true,
            Unique = false
        };

        Files.Columns.AddRange(new[] { firstDataColumn, secondDataColumn, thirdDataColumn });

        /* DataGridViewColumn firstColumn = FilesDataGridView.Columns[0];
        firstColumn.SortMode = DataGridViewColumnSortMode.Automatic;
        firstColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        firstColumn.Width = 180;
        firstColumn.HeaderText = "File Name";

        DataGridViewColumn secondColumn = FilesDataGridView.Columns[1];
        secondColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        secondColumn.Width = 180;
        secondColumn.HeaderText = "Directory";

        DataGridViewColumn thirdColumn = FilesDataGridView.Columns[2];
        thirdColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        thirdColumn.HeaderText = "Comes From";*/
    }

    public static void OpenWithDefaultProgram(string path) {
        ProcessStartInfo fileopener = new() {
            FileName = path,
            UseShellExecute = true
        };
        _ = Process.Start(fileopener);
    }

    private void SkyrimFolderTextBox_TextChanged(object sender, EventArgs e) {
    }

    private async Task<VortexDeployment?> GetVortexDeployment() {
        using FileStream openStream = File.OpenRead(VortexDeploymentJSONFile);
        VortexDeployment? deployment = await JsonSerializer.DeserializeAsync<VortexDeployment>(openStream);
        return deployment;
    }

    private string GetRelativePath(string fullPath, string basePath) {
        string fixedFullPath = !fullPath.EndsWith("\\") ?
            fullPath + "\\" :
            fullPath;
        string fixedBasePath = !basePath.EndsWith("\\") ?
             basePath + "\\" :
             basePath;

        Uri baseUri = new(fixedBasePath);
        Uri fullUri = new(fixedFullPath);

        Uri relativeUri = baseUri.MakeRelativeUri(fullUri);

        return relativeUri.ToString().Replace("/", "\\");
    }

    private async Task<IEnumerable<KeyValuePair<string, string>>> GetFiles(string directoryPath) {
        string[] subDirectoriesWithPaths = Directory.GetDirectories(directoryPath);
        IEnumerable<KeyValuePair<string, string>> subDirectoryFiles = (await Task.WhenAll(subDirectoriesWithPaths
            .Select(GetFiles))).SelectMany(file => file);
        IEnumerable<KeyValuePair<string, string>> files = Directory
            .GetFiles(directoryPath)
            .Where(filePath =>
                !IgnoredFiles
                    .Any(ignored =>
                        filePath.Contains(ignored)
                    )
            )
            .Select(file => new KeyValuePair<string, string>(GetRelativePath(file, SkyrimDataPath), "Vanilla Skyrim"));
        return files.Concat(subDirectoryFiles);
    }

    private async Task CreateListViewItemFromFilePath(KeyValuePair<string, string> file)
        => await Task.Run(() => {
            string fileWithPath = file.Key;
            string comesFrom = file.Value;

            string fileName = Path.GetFileName(fileWithPath);
            string directory = "\\" + (Path.GetDirectoryName(fileWithPath) ?? "");

            Invoke(() => {
                DataRow newRow = Files.NewRow();
                newRow.ItemArray = new[] { fileName, directory, comesFrom };
                Files.Rows.Add(newRow);
            });
        });

    private void GetFilesButton_Click(object sender, EventArgs e) {
        Files.Clear();

        SkyrimDataPath = Path.Join(SkyrimFolderTextBox.Text, "\\Data\\");
        VortexDeploymentJSONFile = Path.Join(SkyrimDataPath, VortexDeploymentJSONFileName);

        _ = Task.Run(async () => {
            VortexDeployment? deployment = await GetVortexDeployment();
            IEnumerable<KeyValuePair<string, string>> files = await GetFiles(SkyrimDataPath);
            Dictionary<string, string> deploymentFiles = new();
            deployment?.Files.Reverse().ToList().ForEach(vortexDeploymentFile => {
                deploymentFiles.Add(vortexDeploymentFile.RelativePath, vortexDeploymentFile.Source);
            });
            Dictionary<string, string> allFiles = deploymentFiles
                .Concat(files)
                .GroupBy(kvp => kvp.Key)
                .ToDictionary(g => g.Key, g => g.Select(kvp => kvp.Value).Last());
            await Task.WhenAll(deploymentFiles.Select(CreateListViewItemFromFilePath));
            Invoke(() => {
                Files.AcceptChanges();

                FilesDataGridView.DataSource = Files;

                DataGridViewColumn firstColumn = FilesDataGridView.Columns[0];
                firstColumn.SortMode = DataGridViewColumnSortMode.Automatic;
                firstColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                firstColumn.Width = 180;
                firstColumn.HeaderText = "File Name";

                DataGridViewColumn secondColumn = FilesDataGridView.Columns[1];
                secondColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                secondColumn.Width = 180;
                secondColumn.HeaderText = "Directory";

                DataGridViewColumn thirdColumn = FilesDataGridView.Columns[2];
                thirdColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                thirdColumn.HeaderText = "Comes From";
            });
        });
    }

    private void RegexFilterTextBox_TextChanged(object sender, EventArgs e) {
        Files
            .DefaultView
            .RowFilter =  $"(FileName LIKE '*{RegexFilterTextBox.Text}*' or Directory LIKE '*{RegexFilterTextBox.Text}*' or ComesFrom LIKE '*{RegexFilterTextBox.Text}*')";
    }

    private void FilesDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
        switch (e.ColumnIndex) {
            
            case 0:
            default:
                string fileName = (string)(FilesDataGridView.Rows[e.RowIndex].Cells[0]?.Value ?? ""); //(string)(Files.Rows[e.RowIndex].ItemArray[0] ?? "");
                string filePath = (string)(FilesDataGridView.Rows[e.RowIndex].Cells[1]?.Value ?? "");
                string fullPath = Path.Join(SkyrimDataPath, filePath.Substring(Math.Min(1, filePath.Length), Math.Max(filePath.Length - 1, 0)), fileName);
                OpenWithDefaultProgram(fullPath);
                break;
        }
    }
}

public class VortexDeploymentFile {
    [JsonPropertyName("relPath")]
    public string RelativePath { get; set; } = "";
    [JsonPropertyName("source")]
    public string Source { get; set; } = "";
    [JsonPropertyName("target")]
    public string Target { get; set; } = "";
    [JsonPropertyName("time")]
    public ulong Time { get; set; }
}

public class VortexDeployment {
    [JsonPropertyName("instance")]
    public string Instance { get; set; } = "";
    [JsonPropertyName("version")]
    public int Version { get; set; }
    [JsonPropertyName("deploymentMethod")]
    public string DeploymentMethod { get; set; } = "";
    [JsonPropertyName("gameId")]
    public string GameID { get; set; } = "";
    [JsonPropertyName("deploymentTime")]
    public ulong DeploymentTime { get; set; }
    [JsonPropertyName("string")]
    public string StagingPath { get; set; } = "";
    [JsonPropertyName("targetPath")]
    public string TargetPath { get; set; } = "";
    [JsonPropertyName("files")]
    public VortexDeploymentFile[] Files { get; set; } = Array.Empty<VortexDeploymentFile>();
}
