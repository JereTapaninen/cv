﻿namespace SkyrimFileAnalyzer;

partial class MainForm {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
        if (disposing && (components != null)) {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.SkyrimFolderLabel = new System.Windows.Forms.Label();
            this.SkyrimFolderTextBox = new System.Windows.Forms.TextBox();
            this.GetFilesButton = new System.Windows.Forms.Button();
            this.FileNameHeader = new System.Windows.Forms.ColumnHeader();
            this.DirectoryHeader = new System.Windows.Forms.ColumnHeader();
            this.ComesFromHeader = new System.Windows.Forms.ColumnHeader();
            this.FilesDataGridView = new System.Windows.Forms.DataGridView();
            this.RegexFilterTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.FilesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // SkyrimFolderLabel
            // 
            this.SkyrimFolderLabel.AutoSize = true;
            this.SkyrimFolderLabel.Location = new System.Drawing.Point(12, 13);
            this.SkyrimFolderLabel.Name = "SkyrimFolderLabel";
            this.SkyrimFolderLabel.Size = new System.Drawing.Size(82, 15);
            this.SkyrimFolderLabel.TabIndex = 0;
            this.SkyrimFolderLabel.Text = "Skyrim Folder:";
            // 
            // SkyrimFolderTextBox
            // 
            this.SkyrimFolderTextBox.Location = new System.Drawing.Point(12, 33);
            this.SkyrimFolderTextBox.Name = "SkyrimFolderTextBox";
            this.SkyrimFolderTextBox.Size = new System.Drawing.Size(387, 23);
            this.SkyrimFolderTextBox.TabIndex = 1;
            this.SkyrimFolderTextBox.TextChanged += new System.EventHandler(this.SkyrimFolderTextBox_TextChanged);
            // 
            // GetFilesButton
            // 
            this.GetFilesButton.Location = new System.Drawing.Point(12, 62);
            this.GetFilesButton.Name = "GetFilesButton";
            this.GetFilesButton.Size = new System.Drawing.Size(75, 23);
            this.GetFilesButton.TabIndex = 2;
            this.GetFilesButton.Text = "Get files";
            this.GetFilesButton.UseVisualStyleBackColor = true;
            this.GetFilesButton.Click += new System.EventHandler(this.GetFilesButton_Click);
            // 
            // FileNameHeader
            // 
            this.FileNameHeader.Text = "File Name";
            this.FileNameHeader.Width = 160;
            // 
            // DirectoryHeader
            // 
            this.DirectoryHeader.Text = "Directory";
            this.DirectoryHeader.Width = 200;
            // 
            // ComesFromHeader
            // 
            this.ComesFromHeader.Text = "Comes From";
            this.ComesFromHeader.Width = 280;
            // 
            // FilesDataGridView
            // 
            this.FilesDataGridView.AllowUserToAddRows = false;
            this.FilesDataGridView.AllowUserToDeleteRows = false;
            this.FilesDataGridView.AllowUserToOrderColumns = true;
            this.FilesDataGridView.AllowUserToResizeRows = false;
            this.FilesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FilesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FilesDataGridView.Location = new System.Drawing.Point(12, 132);
            this.FilesDataGridView.Name = "FilesDataGridView";
            this.FilesDataGridView.ReadOnly = true;
            this.FilesDataGridView.RowHeadersVisible = false;
            this.FilesDataGridView.RowTemplate.Height = 25;
            this.FilesDataGridView.ShowEditingIcon = false;
            this.FilesDataGridView.Size = new System.Drawing.Size(666, 365);
            this.FilesDataGridView.TabIndex = 3;
            this.FilesDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.FilesDataGridView_CellDoubleClick);
            // 
            // RegexFilterTextBox
            // 
            this.RegexFilterTextBox.Location = new System.Drawing.Point(12, 103);
            this.RegexFilterTextBox.Name = "RegexFilterTextBox";
            this.RegexFilterTextBox.Size = new System.Drawing.Size(666, 23);
            this.RegexFilterTextBox.TabIndex = 4;
            this.RegexFilterTextBox.TextChanged += new System.EventHandler(this.RegexFilterTextBox_TextChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 509);
            this.Controls.Add(this.RegexFilterTextBox);
            this.Controls.Add(this.FilesDataGridView);
            this.Controls.Add(this.GetFilesButton);
            this.Controls.Add(this.SkyrimFolderTextBox);
            this.Controls.Add(this.SkyrimFolderLabel);
            this.DoubleBuffered = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Skyrim File Analyzer";
            ((System.ComponentModel.ISupportInitialize)(this.FilesDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private Label SkyrimFolderLabel;
    private TextBox SkyrimFolderTextBox;
    private Button GetFilesButton;
    private ColumnHeader FileNameHeader;
    private ColumnHeader DirectoryHeader;
    private ColumnHeader ComesFromHeader;
    private DataGridView FilesDataGridView;
    private TextBox RegexFilterTextBox;
}
