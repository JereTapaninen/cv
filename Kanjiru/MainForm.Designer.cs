﻿namespace Kanjiru;

partial class MainForm {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
        if (disposing && (components != null)) {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.KanjiruMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DecksGroupBox = new System.Windows.Forms.GroupBox();
            this.EditDeckButton = new System.Windows.Forms.Button();
            this.CreateNewDeckButton = new System.Windows.Forms.Button();
            this.DecksDataGridView = new System.Windows.Forms.DataGridView();
            this.AddDeckButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.KanjiruMenuStrip.SuspendLayout();
            this.DecksGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DecksDataGridView)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // KanjiruMenuStrip
            // 
            this.KanjiruMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.KanjiruMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.KanjiruMenuStrip.Name = "KanjiruMenuStrip";
            this.KanjiruMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.KanjiruMenuStrip.Size = new System.Drawing.Size(752, 24);
            this.KanjiruMenuStrip.TabIndex = 0;
            this.KanjiruMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(113, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // DecksGroupBox
            // 
            this.DecksGroupBox.Controls.Add(this.EditDeckButton);
            this.DecksGroupBox.Controls.Add(this.CreateNewDeckButton);
            this.DecksGroupBox.Controls.Add(this.DecksDataGridView);
            this.DecksGroupBox.Controls.Add(this.AddDeckButton);
            this.DecksGroupBox.Location = new System.Drawing.Point(12, 36);
            this.DecksGroupBox.Name = "DecksGroupBox";
            this.DecksGroupBox.Padding = new System.Windows.Forms.Padding(10);
            this.DecksGroupBox.Size = new System.Drawing.Size(408, 267);
            this.DecksGroupBox.TabIndex = 2;
            this.DecksGroupBox.TabStop = false;
            this.DecksGroupBox.Text = "Decks";
            // 
            // EditDeckButton
            // 
            this.EditDeckButton.Location = new System.Drawing.Point(210, 222);
            this.EditDeckButton.Name = "EditDeckButton";
            this.EditDeckButton.Size = new System.Drawing.Size(62, 27);
            this.EditDeckButton.TabIndex = 4;
            this.EditDeckButton.Text = "Edit";
            this.EditDeckButton.UseVisualStyleBackColor = true;
            // 
            // CreateNewDeckButton
            // 
            this.CreateNewDeckButton.Location = new System.Drawing.Point(116, 222);
            this.CreateNewDeckButton.Name = "CreateNewDeckButton";
            this.CreateNewDeckButton.Size = new System.Drawing.Size(88, 27);
            this.CreateNewDeckButton.TabIndex = 3;
            this.CreateNewDeckButton.Text = "Create New";
            this.CreateNewDeckButton.UseVisualStyleBackColor = true;
            // 
            // DecksDataGridView
            // 
            this.DecksDataGridView.AllowUserToAddRows = false;
            this.DecksDataGridView.AllowUserToDeleteRows = false;
            this.DecksDataGridView.AllowUserToResizeRows = false;
            this.DecksDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DecksDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DecksDataGridView.Location = new System.Drawing.Point(13, 29);
            this.DecksDataGridView.MultiSelect = false;
            this.DecksDataGridView.Name = "DecksDataGridView";
            this.DecksDataGridView.ReadOnly = true;
            this.DecksDataGridView.RowHeadersVisible = false;
            this.DecksDataGridView.RowTemplate.Height = 25;
            this.DecksDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DecksDataGridView.Size = new System.Drawing.Size(382, 187);
            this.DecksDataGridView.TabIndex = 2;
            this.DecksDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DecksDataGridView_CellMouseDoubleClick);
            // 
            // AddDeckButton
            // 
            this.AddDeckButton.Location = new System.Drawing.Point(13, 222);
            this.AddDeckButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.AddDeckButton.Name = "AddDeckButton";
            this.AddDeckButton.Size = new System.Drawing.Size(97, 27);
            this.AddDeckButton.TabIndex = 1;
            this.AddDeckButton.Text = "Add From File";
            this.AddDeckButton.UseVisualStyleBackColor = true;
            this.AddDeckButton.Click += new System.EventHandler(this.AddDeckButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.KanjiruMenuStrip);
            this.panel1.Controls.Add(this.DecksGroupBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(752, 505);
            this.panel1.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(752, 505);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.KanjiruMenuStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kanjiru";
            this.KanjiruMenuStrip.ResumeLayout(false);
            this.KanjiruMenuStrip.PerformLayout();
            this.DecksGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DecksDataGridView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    private MenuStrip KanjiruMenuStrip;
    private ToolStripMenuItem fileToolStripMenuItem;
    private ToolStripMenuItem settingsToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripMenuItem exitToolStripMenuItem;
    private GroupBox DecksGroupBox;
    private Panel panel1;
    private Button AddDeckButton;
    private DataGridView DecksDataGridView;
    private Button EditDeckButton;
    private Button CreateNewDeckButton;
}
