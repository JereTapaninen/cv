using Kanjiru.Kanjiru;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Kanjiru;

public partial class MainForm : Form {
    private ISettings KanjiruSettings { get; init; }

    private readonly DataTable Decks = new();

    public MainForm() {
        InitializeComponent();

        SetDataTableColumns();

        string settingsString = File.ReadAllText("./Settings.json");
        KanjiruSettings = JsonSerializer.Deserialize<Settings>(settingsString) ?? Settings.Create();
        KanjiruSettings.OnDeckAdded += KanjiruSettings_OnDeckAdded;
        KanjiruSettings.OnDeckModified += KanjiruSettings_OnDeckModified;

        // new AddForm(KanjiruSettings.GetDecks().ToArray()[0]).ShowDialog();
    }

    private void KanjiruSettings_OnDeckModified(object? sender, DeckEventArgs e) {
        Decks.Rows.Clear();
        IKanjiruDeck[] decks = KanjiruSettings.GetDecks().ToArray();
        foreach (IKanjiruDeck deck in decks) {
            DataRow newRow = CreateDecksRow(deck);
            AddDeckRow(newRow);
        }
    }

    private void SetDataTableColumns() {
        DataColumn hiddenValueColumn = new() {
            DataType = typeof(IKanjiruDeck),
            ColumnName = "KanjiruDeck",
            ColumnMapping = MappingType.Hidden,
            ReadOnly = true,
            Unique = false
        };

        DataColumn firstDataColumn = new() {
            DataType = typeof(string),
            ColumnName = "Name",
            ReadOnly = true,
            Unique = false
        };

        DataColumn secondDataColumn = new() {
            DataType = typeof(int),
            ColumnName = "Radicals",
            ReadOnly = true,
            Unique = false
        };

        DataColumn thirdDataColumn = new() {
            DataType = typeof(int),
            ColumnName = "Kanji",
            ReadOnly = true,
            Unique = false
        };

        DataColumn fourthDataColumn = new() {
            DataType = typeof(int),
            ColumnName = "Vocabulary",
            ReadOnly = true,
            Unique = false
        };

        Decks.Columns.AddRange(new[] { hiddenValueColumn, firstDataColumn, secondDataColumn, thirdDataColumn, fourthDataColumn });

        DecksDataGridView.DataSource = Decks;

        DataGridViewColumn firstColumn = DecksDataGridView.Columns[0];
        firstColumn.SortMode = DataGridViewColumnSortMode.Automatic;
        firstColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        firstColumn.Width = 100;
        firstColumn.HeaderText = "Name";

        DataGridViewColumn secondColumn = DecksDataGridView.Columns[1];
        secondColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        secondColumn.Width = 60;
        secondColumn.HeaderText = "Radicals";

        DataGridViewColumn thirdColumn = DecksDataGridView.Columns[2];
        thirdColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        thirdColumn.Width = 60;
        thirdColumn.HeaderText = "Kanji";

        DataGridViewColumn fourthColumn = DecksDataGridView.Columns[3];
        fourthColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        fourthColumn.HeaderText = "Vocabulary";
    }

    private void KanjiruSettings_OnDeckAdded(object? sender, DeckEventArgs e) {
        DataRow newRow = CreateDecksRow(e.Deck);
        AddDeckRow(newRow);
    }

    private DataRow CreateDecksRow(IKanjiruDeck deck) {
        DataRow newRow = Decks.NewRow();
        newRow.ItemArray = new object[] {
            deck,
            deck.GetName(),
            deck.GetTypeCount(KanjiruItemType.Radical),
            deck.GetTypeCount(KanjiruItemType.Kanji),
            deck.GetTypeCount(KanjiruItemType.Vocabulary)
        };
        return newRow;
    }

    private void AddDeckRow(DataRow row) {
        Decks.Rows.Add(row);
        Decks.AcceptChanges();
    }

    private async void AddDeckButton_Click(object sender, EventArgs e) {
        using OpenFileDialog openFileDialog = new() {
            Title = "Open Kanjiru Deck File (*.json)",
            Filter = "JSON file (*.json)|*.json",
            InitialDirectory = Application.StartupPath,
            Multiselect = false
        };

        if (openFileDialog.ShowDialog() == DialogResult.OK) {
            string deckString = File.ReadAllText(openFileDialog.FileName);
            IKanjiruDeck? kanjiruDeck = JsonSerializer.Deserialize<KanjiruDeck>(deckString);
            string? deckPathNoFileName = Path.GetDirectoryName(openFileDialog.FileName);

            if (kanjiruDeck == null || deckPathNoFileName == null) {
                _ = MessageBox.Show($"Could not load deck file at {openFileDialog.FileName}.\nIs the file faulty?", "Error loading deck", MessageBoxButtons.OK);
                return;
            }

            await kanjiruDeck.LoadItems(deckPathNoFileName);

            KanjiruSettings.AddDeck(kanjiruDeck);

            _ = MessageBox.Show($"New deck added successfully!", "Deck added!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }

    private void DecksDataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e) {
        if (e.Button == MouseButtons.Left && (DecksDataGridView.Rows[e.RowIndex].DataBoundItem as DataRowView)?["KanjiruDeck"] is IKanjiruDeck deck)
            new DeckForm(deck).Show();
        else
            _ = MessageBox.Show("Could not open deck for editing due to an unknown error!", "Error opening deck for editing!", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }
}
