﻿namespace Kanjiru.AddItemSettings;

partial class RadicalSettings {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
        if (disposing && (components != null)) {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.AllKanjiLabel = new System.Windows.Forms.Label();
            this.KanjiListBox = new System.Windows.Forms.ListBox();
            this.AddSelectedKanjiButton = new System.Windows.Forms.Button();
            this.RemoveSelectedKanjiButton = new System.Windows.Forms.Button();
            this.AssociatedKanjiListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AssociatedVocabularyListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // AllKanjiLabel
            // 
            this.AllKanjiLabel.AutoSize = true;
            this.AllKanjiLabel.Location = new System.Drawing.Point(16, 10);
            this.AllKanjiLabel.Name = "AllKanjiLabel";
            this.AllKanjiLabel.Size = new System.Drawing.Size(52, 15);
            this.AllKanjiLabel.TabIndex = 0;
            this.AllKanjiLabel.Text = "All kanji:";
            // 
            // KanjiListBox
            // 
            this.KanjiListBox.FormattingEnabled = true;
            this.KanjiListBox.ItemHeight = 15;
            this.KanjiListBox.Location = new System.Drawing.Point(16, 34);
            this.KanjiListBox.Name = "KanjiListBox";
            this.KanjiListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.KanjiListBox.Size = new System.Drawing.Size(113, 214);
            this.KanjiListBox.TabIndex = 1;
            // 
            // AddSelectedKanjiButton
            // 
            this.AddSelectedKanjiButton.Location = new System.Drawing.Point(135, 110);
            this.AddSelectedKanjiButton.Name = "AddSelectedKanjiButton";
            this.AddSelectedKanjiButton.Size = new System.Drawing.Size(54, 23);
            this.AddSelectedKanjiButton.TabIndex = 2;
            this.AddSelectedKanjiButton.Text = ">>";
            this.AddSelectedKanjiButton.UseVisualStyleBackColor = true;
            this.AddSelectedKanjiButton.Click += new System.EventHandler(this.AddSelectedKanjiButton_Click);
            // 
            // RemoveSelectedKanjiButton
            // 
            this.RemoveSelectedKanjiButton.Location = new System.Drawing.Point(135, 139);
            this.RemoveSelectedKanjiButton.Name = "RemoveSelectedKanjiButton";
            this.RemoveSelectedKanjiButton.Size = new System.Drawing.Size(54, 23);
            this.RemoveSelectedKanjiButton.TabIndex = 3;
            this.RemoveSelectedKanjiButton.Text = "<<";
            this.RemoveSelectedKanjiButton.UseVisualStyleBackColor = true;
            // 
            // AssociatedKanjiListBox
            // 
            this.AssociatedKanjiListBox.FormattingEnabled = true;
            this.AssociatedKanjiListBox.ItemHeight = 15;
            this.AssociatedKanjiListBox.Location = new System.Drawing.Point(195, 34);
            this.AssociatedKanjiListBox.Name = "AssociatedKanjiListBox";
            this.AssociatedKanjiListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.AssociatedKanjiListBox.Size = new System.Drawing.Size(113, 214);
            this.AssociatedKanjiListBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(195, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Associated kanji:";
            // 
            // AssociatedVocabularyListBox
            // 
            this.AssociatedVocabularyListBox.FormattingEnabled = true;
            this.AssociatedVocabularyListBox.ItemHeight = 15;
            this.AssociatedVocabularyListBox.Location = new System.Drawing.Point(314, 34);
            this.AssociatedVocabularyListBox.Name = "AssociatedVocabularyListBox";
            this.AssociatedVocabularyListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.AssociatedVocabularyListBox.Size = new System.Drawing.Size(194, 214);
            this.AssociatedVocabularyListBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Associated vocabulary:";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.KanjiListBox);
            this.panel1.Controls.Add(this.AssociatedVocabularyListBox);
            this.panel1.Controls.Add(this.AllKanjiLabel);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.AddSelectedKanjiButton);
            this.panel1.Controls.Add(this.AssociatedKanjiListBox);
            this.panel1.Controls.Add(this.RemoveSelectedKanjiButton);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(511, 251);
            this.panel1.TabIndex = 8;
            // 
            // RadicalSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.panel1);
            this.Name = "RadicalSettings";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(522, 262);
            this.Load += new System.EventHandler(this.RadicalSettings_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private Label AllKanjiLabel;
    private ListBox KanjiListBox;
    private Button AddSelectedKanjiButton;
    private Button RemoveSelectedKanjiButton;
    private ListBox AssociatedKanjiListBox;
    private Label label1;
    private ListBox AssociatedVocabularyListBox;
    private Label label2;
    private Panel panel1;
}
