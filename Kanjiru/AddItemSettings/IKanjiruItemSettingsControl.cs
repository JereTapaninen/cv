﻿using Kanjiru.Kanjiru;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kanjiru.AddItemSettings;

public interface IKanjiruItemSettingsControl<T1, T2> : IContainerControl where T1 : IKanjiruItemSettingsControl<T1, T2> where T2 : IKanjiruItem<T2> {
    public bool GetAddButtonEnabledState();
    public T2 GetItem();
    public Task CleanUp();
}

public interface IKanjiruRadicalsSettingsControl : IKanjiruItemSettingsControl<IKanjiruRadical> {

}
