﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kanjiru.Kanjiru;

namespace Kanjiru.AddItemSettings;
public partial class RadicalSettings : UserControl, IKanjiruItemSettingsControl<IKanjiruRadical> {
    private IKanjiruDeck DeckCopy { get; }
    private IKanjiruRadical NewRadical { get; }

    private bool IsOpen { get; set; }
    private Task? UpdateTask { get; set; }

    private readonly DataTable AssociatedVocabularyTable = new();
    private readonly DataTable AssociatedKanjiTable = new();
    private readonly DataTable AllKanjiTable = new();

    public RadicalSettings(IKanjiruDeck deck) {
        InitializeComponent();

        if (deck.CreateNewItem<IKanjiruRadical>(KanjiruItemType.Radical) is not IKanjiruRadical newRadical)
            throw new Exception("Something went badly wrong when trying to create a new empty radical.");

        NewRadical = newRadical;

        DeckCopy = deck.CreateCopy();

        bool success = DeckCopy.TryAddItem(NewRadical);

        if (!success)
            throw new Exception("Something went badly wrong when trying to add a newly created radical to the deck. Perhaps the GUID has already been taken?");

        DeckCopy.OnDeckModified += OnCopyDeckModified;
        deck.OnDeckModified += OnOriginalDeckModified;

        _ = AssociatedVocabularyTable.Columns.Add("Item", typeof(IKanjiruVocabulary));
        _ = AssociatedVocabularyTable.Columns.Add("Name", typeof(string));

        _ = AssociatedKanjiTable.Columns.Add("Item", typeof(IKanjiruKanji));
        _ = AssociatedKanjiTable.Columns.Add("Name", typeof(string));

        _ = AllKanjiTable.Columns.Add("Item", typeof(IKanjiruKanji));
        _ = AllKanjiTable.Columns.Add("Name", typeof(string));

        KanjiListBox.DataSource = AllKanjiTable;
        KanjiListBox.DisplayMember = "Name";
        KanjiListBox.ValueMember = "Item";

        AssociatedKanjiListBox.DataSource = AssociatedKanjiTable;
        AssociatedKanjiListBox.DisplayMember = "Name";
        AssociatedKanjiListBox.ValueMember = "Item";

        AssociatedVocabularyListBox.DataSource = AssociatedVocabularyTable;
        AssociatedVocabularyListBox.DisplayMember = "Name";
        AssociatedVocabularyListBox.ValueMember = "Item";

        UpdateAllListBoxes(DeckCopy);
    }

    private void UpdateAllListBoxes(IKanjiruDeck deck) {
        SetupAllKanjiListBox(deck);
        RebuildAssociatedKanjiTable(deck);
        RebuildAssociatedVocabularyTable(deck);
    }

    private void OnCopyDeckModified(object? sender, DeckEventArgs e) {
        UpdateAllListBoxes(e.Deck);
    }

    private void RebuildAssociatedKanjiTable(IKanjiruDeck deck) {
        int selectedIndex = AssociatedKanjiListBox.SelectedIndex;
        AssociatedKanjiTable.Clear();

        IKanjiruKanji[] associatedKanji = deck.GetContainingItemsOfType<IKanjiruRadical, IKanjiruKanji>(NewRadical, KanjiruItemType.Kanji).ToArray();

        foreach (IKanjiruKanji kanji in associatedKanji)
            _ = AssociatedKanjiTable.Rows.Add(kanji, kanji.GetValue());

        AssociatedKanjiListBox.SelectedIndex = Math.Min(selectedIndex, AssociatedKanjiListBox.Items.Count - 1);
    }

    private void RebuildAssociatedVocabularyTable(IKanjiruDeck deck) {
        //int selectedIndex = AssociatedVocabularyListBox.SelectedIndex;
        AssociatedVocabularyTable.Clear();

        IKanjiruVocabulary[] associatedVocabulary = deck.GetContainingItemsOfType<IKanjiruRadical, IKanjiruVocabulary>(NewRadical, KanjiruItemType.Vocabulary).ToArray();

        foreach (IKanjiruVocabulary vocabulary in associatedVocabulary)
            _ = AssociatedVocabularyTable.Rows.Add(vocabulary, vocabulary.GetValue());

        //AssociatedVocabularyListBox.SelectedIndex = Math.Min(selectedIndex, AssociatedVocabularyListBox.Items.Count - 1);
    }

    private Task UpdateTaskCreator() =>
        Task.Run(() => {
            while (!IsOpen) {
                Thread.Sleep(10);
            }

            while (IsOpen) {
                if (IsHandleCreated) {
                    Invoke(() => {
                        AddSelectedKanjiButton.Enabled = KanjiListBox.SelectedValue is IKanjiruKanji;
                        RemoveSelectedKanjiButton.Enabled = AssociatedKanjiListBox.SelectedValue is IKanjiruKanji;
                    });
                }
                Thread.Sleep(10);
            }
        });

    private void SetupAllKanjiListBox(IKanjiruDeck deck) {
        IKanjiruKanji[] associatedKanji = AssociatedKanjiListBox.Items.OfType<IKanjiruKanji>().ToArray();
        IKanjiruKanji[] kanji = deck
            .GetItemsOfType<IKanjiruKanji>(KanjiruItemType.Kanji)
            .Where(kanji => !associatedKanji.Contains(kanji))
            .ToArray();

        int selectedIndex = KanjiListBox.SelectedIndex;
        AllKanjiTable.Clear();

        foreach (IKanjiruKanji kanjiItem in kanji)
            _ = AllKanjiTable.Rows.Add(kanjiItem, kanjiItem.GetValue());

        KanjiListBox.SelectedIndex = Math.Min(selectedIndex, KanjiListBox.Items.Count - 1);
    }

    private void OnOriginalDeckModified(object? sender, DeckEventArgs e) {
        IEnumerable<T> joinItems<T>(IEnumerable<T> oldItems, IEnumerable<T> newItems) where T : IKanjiruItem<T> =>
            oldItems
                .Concat(newItems.Where(radical => !oldItems.Contains(radical)))
                .Distinct();

        IEnumerable<IKanjiruRadical> oldRadicals = DeckCopy.GetItemsOfType<IKanjiruRadical>(KanjiruItemType.Radical);
        IEnumerable<IKanjiruKanji> oldKanji = DeckCopy.GetItemsOfType<IKanjiruKanji>(KanjiruItemType.Kanji);
        IEnumerable<IKanjiruVocabulary> oldVocabulary = DeckCopy.GetItemsOfType<IKanjiruVocabulary>(KanjiruItemType.Vocabulary);

        IEnumerable<IKanjiruRadical> newRadicals = e.Deck.GetItemsOfType<IKanjiruRadical>(KanjiruItemType.Radical);
        IEnumerable<IKanjiruKanji> newKanji = e.Deck.GetItemsOfType<IKanjiruKanji>(KanjiruItemType.Kanji);
        IEnumerable<IKanjiruVocabulary> newVocabulary = e.Deck.GetItemsOfType<IKanjiruVocabulary>(KanjiruItemType.Vocabulary);

        IEnumerable<IKanjiruRadical> joinedRadicals = joinItems(oldRadicals, newRadicals);
        IEnumerable<IKanjiruKanji> joinedKanji = joinItems(oldKanji, newKanji);
        IEnumerable<IKanjiruVocabulary> joinedVocabulary = joinItems(oldVocabulary, newVocabulary);

        bool setRadicalsSuccessfully = DeckCopy.SetItems(joinedRadicals);
        bool setKanjiSuccessfully = DeckCopy.SetItems(joinedKanji);
        bool setVocabularySuccessfully = DeckCopy.SetItems(joinedVocabulary);
    }

    public bool GetAddButtonEnabledState() => false;
    public IKanjiruRadical GetItem() => NewRadical;

    public async Task CleanUp() {
        if (UpdateTask != null && !UpdateTask.IsCompleted)
            await UpdateTask;
    }

    private void RadicalSettings_Load(object sender, EventArgs e) {
        IsOpen = true;
        UpdateTask = UpdateTaskCreator();
    }

    private void AddSelectedKanjiButton_Click(object sender, EventArgs e) {
        if (KanjiListBox.SelectedValue is not IKanjiruKanji kanji)
            return;

        bool successful = kanji.TryAddRadical(NewRadical.GetId());

        if (!successful)
            _ = MessageBox.Show("Radical could not be added to kanji, perhaps the kanji already has that radical?", "Radical could not be added to kanji", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }
}
