﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kanjiru.Kanjiru;

public interface IKanjiruItemWithValue {
    public string GetValue();
}

public interface IKanjiruItem<T> : IEquatable<T>, IKanjiruItemWithValue where T : IKanjiruItem<T> {
    public Guid GetId();
    public KanjiruItemType GetItemType();
}


public interface IKanjiruCreateableItem<T> where T : IKanjiruItem<T> {
    public static abstract T Create(IKanjiruDeck? deck, string value);
}

public interface IKanjiruItemWithModifyEvent {
    public event EventHandler<KanjiruItemChangedEventArgs> OnItemPropertyChanged;
}

public interface IKanjiruRadical : IKanjiruItem<IKanjiruRadical>, IKanjiruItemWithModifyEvent {
}


public interface IKanjiruKanji : IKanjiruItem<IKanjiruKanji>, IKanjiruItemWithModifyEvent {
    public IEnumerable<Guid> GetRadicals();
    public bool TryAddRadical(Guid radicalId);
}

public interface IKanjiruVocabulary : IKanjiruItem<IKanjiruVocabulary>, IKanjiruItemWithModifyEvent {
    public IEnumerable<Guid> GetKanji();
    public bool HasKanji(IKanjiruKanji kanji);
}
