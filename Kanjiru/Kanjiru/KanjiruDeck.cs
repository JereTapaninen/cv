﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Kanjiru.Kanjiru;

namespace Kanjiru.Kanjiru;

public interface IKanjiruDeck : IEquatable<IKanjiruDeck> {
    event EventHandler<DeckEventArgs> OnDeckModified;
    public Guid GetId();
    public string GetName();
    public void SetName(string name);
    public bool RemoveItem<T>(T item) where T : IKanjiruItem<T>;
    public bool HasItem<T>(T item) where T : IKanjiruItem<T>;
    public int GetTypeCount(KanjiruItemType type);
    public IEnumerable<T2> GetContainingItemsOfType<T, T2>(IKanjiruItem<T> kanjiruItem, KanjiruItemType type) where T : IKanjiruItem<T> where T2 : IKanjiruItem<T2>;
    public IEnumerable<T2> GetRequirementsOfType<T1, T2>(IKanjiruItem<T1> kanjiruItem, KanjiruItemType type) where T1 : IKanjiruItem<T1> where T2 : IKanjiruItem<T2>;
    public Task LoadItems(string filePath);
    public IKanjiruDeck CreateCopy();
    public bool TryAddItem<T>(T item) where T : IKanjiruItem<T>;

    public IEnumerable<T> GetItemsOfType<T>(KanjiruItemType type) where T : IKanjiruItem<T>;
    public bool SetItems<T>(IEnumerable<T> items) where T : IKanjiruItem<T>;

    public T? CreateNewItem<T>(KanjiruItemType type) where T : IKanjiruItem<T>;
}

public class KanjiruDeck : IKanjiruDeck, IJsonOnSerializing {
    [JsonPropertyName("id")]
    public Guid Id { get; init; } = Guid.NewGuid();

    [JsonPropertyName("name")]
    public string Name { get; set; } = "";

    [JsonPropertyName("kanji")]
    public string RelativeKanjiJsonPath { get; init; } = "";

    [JsonPropertyName("radicals")]
    public string RelativeRadicalsJsonPath { get; init; } = "";

    [JsonPropertyName("vocabulary")]
    public string RelativeVocabularyJsonPath { get; init; } = "";

    [JsonIgnore]
    private HashSet<IKanjiruRadical> Radicals { get; set; } = new HashSet<IKanjiruRadical>();

    [JsonIgnore]
    private HashSet<IKanjiruKanji> Kanji { get; set; } = new HashSet<IKanjiruKanji>();

    [JsonIgnore]
    private HashSet<IKanjiruVocabulary> Vocabulary { get; set; } = new HashSet<IKanjiruVocabulary>();

    public event EventHandler<DeckEventArgs>? OnDeckModified;

    public KanjiruDeck() { }

    private KanjiruDeck(IKanjiruDeck fromOther) {
        Id = fromOther.GetId();
        Name = fromOther.GetName();
        Radicals = fromOther.GetItemsOfType<IKanjiruRadical>(KanjiruItemType.Radical).ToHashSet();
        Kanji = fromOther.GetItemsOfType<IKanjiruKanji>(KanjiruItemType.Kanji).ToHashSet();
        Vocabulary = fromOther.GetItemsOfType<IKanjiruVocabulary>(KanjiruItemType.Vocabulary).ToHashSet();
    }

    public IKanjiruDeck CreateCopy() =>
        new KanjiruDeck(this);

    public Guid GetId() => Id;

    public string GetName() => Name;

    public void SetName(string name) {
        Name = name;
        OnDeckModified?.Invoke(this, new DeckEventArgs(this));
    }

    public bool RemoveItem<T>(T item) where T : IKanjiruItem<T> {
        bool removeRadical(IKanjiruRadical item) {
            bool success = Radicals.Remove(item);
            if (success)
                OnDeckModified?.Invoke(this, new DeckEventArgs(this));
            return success;
        }

        bool removeKanji(IKanjiruKanji item) {
            bool success = Kanji.Remove(item);
            if (success)
                OnDeckModified?.Invoke(this, new DeckEventArgs(this));
            return success;
        }

        bool removeVocabulary(IKanjiruVocabulary item) {
            bool success = Vocabulary.Remove(item);
            if (success)
                OnDeckModified?.Invoke(this, new DeckEventArgs(this));
            return success;
        }

        return item.GetItemType().Value switch {
            "Radical" => removeRadical((IKanjiruRadical)item),
            "Kanji" => removeKanji((IKanjiruKanji)item),
            "Vocabulary" => removeVocabulary((IKanjiruVocabulary)item),
            _ => false
        };
    }

    public bool HasItem<T>(T item) where T : IKanjiruItem<T> =>
         item.GetItemType().Value switch {
            "Radical" => Radicals.Contains((IKanjiruRadical)item),
            "Kanji" => Kanji.Contains((IKanjiruKanji)item),
            "Vocabulary" => Vocabulary.Contains((IKanjiruVocabulary)item),
            _ => false
        };

    public int GetTypeCount(KanjiruItemType type) =>
        type.Value switch {
            "Radical" => Radicals.Count,
            "Kanji" => Kanji.Count,
            "Vocabulary" => Vocabulary.Count,
            _ => 0
        };

    public IEnumerable<T> GetItemsOfType<T>(KanjiruItemType type) where T : IKanjiruItem<T> =>
        type.Value switch {
            "Radical" => Radicals.Cast<T>(),
            "Kanji" => Kanji.Cast<T>(),
            "Vocabulary" => Vocabulary.Cast<T>(),
            _ => new HashSet<T>()
        };

    public void OnItemPropertyChanged(object? sender, KanjiruItemChangedEventArgs args) {
        OnDeckModified?.Invoke(this, new(this));
    }

    public bool SetItems<T>(IEnumerable<T> items) where T : IKanjiruItem<T> {
        bool setRadicals(IEnumerable<IKanjiruRadical> radicals) {
            foreach (IKanjiruRadical radical in Radicals) {
                radical.OnItemPropertyChanged -= OnItemPropertyChanged;
            }

            Radicals = radicals.ToHashSet();

            foreach (IKanjiruRadical radical in Radicals) {
                radical.OnItemPropertyChanged += OnItemPropertyChanged;
            }

            OnDeckModified?.Invoke(this, new(this));
            return true;
        }

        bool setKanji(IEnumerable<IKanjiruKanji> kanji) {
            foreach (IKanjiruKanji kan in Kanji) {
                kan.OnItemPropertyChanged -= OnItemPropertyChanged;
            }

            Kanji = kanji.ToHashSet();

            foreach (IKanjiruKanji kan in Kanji) {
                kan.OnItemPropertyChanged += OnItemPropertyChanged;
            }

            OnDeckModified?.Invoke(this, new(this));
            return true;
        }

        bool setVocabulary(IEnumerable<IKanjiruVocabulary> vocabulary) {
            foreach (IKanjiruVocabulary vocab in Vocabulary) {
                vocab.OnItemPropertyChanged -= OnItemPropertyChanged;
            }

            Vocabulary = vocabulary.ToHashSet();

            foreach (IKanjiruVocabulary vocab in Vocabulary) {
                vocab.OnItemPropertyChanged += OnItemPropertyChanged;
            }

            OnDeckModified?.Invoke(this, new(this));
            return true;
        }

        return items switch {
            IEnumerable<IKanjiruRadical> radicalItems when radicalItems is IEnumerable<T> => setRadicals(radicalItems),
            IEnumerable<IKanjiruKanji> kanjiItems when kanjiItems is IEnumerable<T> => setKanji(kanjiItems),
            IEnumerable<IKanjiruVocabulary> vocabularyItems when vocabularyItems is IEnumerable<T> => setVocabulary(vocabularyItems),
            _ => false
        };
    }

    public IEnumerable<IKanjiruRadical> GetRadicals() => Radicals;
    public IEnumerable<IKanjiruKanji> GetKanji() => Kanji;
    public IEnumerable<IKanjiruVocabulary> GetVocabulary() => Vocabulary;

    public IEnumerable<T2> GetContainingItemsOfType<T, T2>(
        IKanjiruItem<T> kanjiruItem,
        KanjiruItemType type
    ) where T : IKanjiruItem<T> where T2 : IKanjiruItem<T2> =>
        type.Value switch {
            _ when kanjiruItem.GetItemType() == type => new T2[] { (T2)kanjiruItem },
            "Kanji" when kanjiruItem.GetItemType() == KanjiruItemType.Radical =>
                Kanji.Where(kanji => kanji.GetRadicals().Contains(kanjiruItem.GetId())).OfType<T2>(),
            "Vocabulary" when kanjiruItem.GetItemType() == KanjiruItemType.Kanji =>
                Vocabulary.Where(vocabulary => vocabulary.GetKanji().Contains(kanjiruItem.GetId())).OfType<T2>(),
            "Vocabulary" when kanjiruItem.GetItemType() == KanjiruItemType.Radical =>
                Kanji
                    .Where(kanji => kanji.GetRadicals().Contains(kanjiruItem.GetId()))
                    .Select(kanji => Vocabulary.Where(vocabulary => vocabulary.GetKanji().Contains(kanji.GetId())))
                    .SelectMany(kanji => kanji)
                    .OfType<T2>()
                    .Distinct(),
            _ => Array.Empty<T2>()
        };

    public IEnumerable<T2> GetRequirementsOfType<T1, T2>(IKanjiruItem<T1> kanjiruItem, KanjiruItemType type) where T1 : IKanjiruItem<T1> where T2 : IKanjiruItem<T2> {
        static IEnumerable<T3> getItemsFromGuids<T3>(IEnumerable<Guid> itemGuids, HashSet<T3> items) where T3 : IKanjiruItem<T3> =>
            items.Where(item => itemGuids.Contains(item.GetId()));

        return type.Value switch {
            "Radical" when kanjiruItem is IKanjiruKanji kanji =>
                getItemsFromGuids(kanji.GetRadicals(), Radicals).OfType<T2>(),
            "Radical" when kanjiruItem is IKanjiruVocabulary vocabulary =>
                getItemsFromGuids(vocabulary.GetKanji(), Kanji)
                    .Select(kanji => getItemsFromGuids(kanji.GetRadicals(), Radicals))
                    .SelectMany(radical => radical)
                    .OfType<T2>()
                    .Distinct(),
            "Kanji" when kanjiruItem is IKanjiruVocabulary vocabulary =>
                getItemsFromGuids(vocabulary.GetKanji(), Kanji).OfType<T2>(),
            _ => Array.Empty<T2>()
        };
    }

    public static IKanjiruDeck Create()
        => new KanjiruDeck();

    public async Task LoadItems(string filePath) {
        string radicalsPath = Path.Join(filePath, RelativeRadicalsJsonPath);
        string kanjiPath = Path.Join(filePath, RelativeKanjiJsonPath);
        string vocabularyPath = Path.Join(filePath, RelativeVocabularyJsonPath);

        string radicalsString = await File.ReadAllTextAsync(radicalsPath);
        string kanjiString = await File.ReadAllTextAsync(kanjiPath);
        string vocabularyString = await File.ReadAllTextAsync(vocabularyPath);

        List<KanjiruRadical>? radicals = JsonSerializer.Deserialize<List<KanjiruRadical>>(radicalsString);
        List<KanjiruKanji>? kanji = JsonSerializer.Deserialize<List<KanjiruKanji>>(kanjiString);
        List<KanjiruVocabulary>? vocabulary = JsonSerializer.Deserialize<List<KanjiruVocabulary>>(vocabularyString);

        Radicals = radicals != null ? radicals.OfType<IKanjiruRadical>().ToHashSet() : new HashSet<IKanjiruRadical>();
        Kanji = kanji != null ? kanji.OfType<IKanjiruKanji>().ToHashSet() : new HashSet<IKanjiruKanji>();
        Vocabulary = vocabulary != null ? vocabulary.OfType<IKanjiruVocabulary>().ToHashSet() : new HashSet<IKanjiruVocabulary>();
    }

    public async void OnSerializing() {
        byte[] radicalsBytes = JsonSerializer.SerializeToUtf8Bytes(Radicals);
        byte[] kanjiBytes = JsonSerializer.SerializeToUtf8Bytes(Kanji);
        byte[] vocabularyBytes = JsonSerializer.SerializeToUtf8Bytes(Vocabulary);

        await Task.WhenAll(new Task[] {
            File.WriteAllBytesAsync(RelativeRadicalsJsonPath, radicalsBytes),
            File.WriteAllBytesAsync(RelativeKanjiJsonPath, kanjiBytes),
            File.WriteAllBytesAsync(RelativeVocabularyJsonPath, vocabularyBytes)
        });
    }

    public bool Equals(IKanjiruDeck? other) =>
        other != null && GetId().Equals(other.GetId());

    public override bool Equals(object? obj) => Equals((IKanjiruDeck?)obj);

    public override int GetHashCode() => Id.GetHashCode();
    public T? CreateNewItem<T>(KanjiruItemType type) where T : IKanjiruItem<T> =>
        type.Value switch {
            "Radical" => (T)KanjiruRadical.Create(this, ""),
            "Kanji" => (T)KanjiruKanji.Create(this, ""),
            "Vocabulary" => (T)KanjiruVocabulary.Create(this, ""),
            _ => default
        };

    public bool TryAddItem<T>(T item) where T : IKanjiruItem<T> {
        bool tryAddRadical(IKanjiruRadical radical) {
            bool success = Radicals.Add(radical);

            if (success)
                OnDeckModified?.Invoke(this, new(this));

            radical.OnItemPropertyChanged += OnItemPropertyChanged;

            return success;
        }

        bool tryAddKanji(IKanjiruKanji kanji) {
            bool success = Kanji.Add(kanji);

            if (success)
                OnDeckModified?.Invoke(this, new(this));

            kanji.OnItemPropertyChanged += OnItemPropertyChanged;

            return success;
        }

        bool tryAddVocabulary(IKanjiruVocabulary vocabulary) {
            bool success = Vocabulary.Add(vocabulary);

            if (success)
                OnDeckModified?.Invoke(this, new(this));

            vocabulary.OnItemPropertyChanged += OnItemPropertyChanged;

            return success;
        }

        return item switch {
            IKanjiruRadical radical when radical is T => tryAddRadical(radical),
            IKanjiruKanji kanji when kanji is T => tryAddKanji(kanji),
            IKanjiruVocabulary vocabulary when vocabulary is T => tryAddVocabulary(vocabulary),
            _ => false
        };
    }

    public static bool operator ==(KanjiruDeck? a, KanjiruDeck? b) =>
        ReferenceEquals(a, b) || (a != null && b != null && a.Equals(b));

    public static bool operator !=(KanjiruDeck? a, KanjiruDeck? b) =>
        !(a == b);
}
