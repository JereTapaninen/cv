﻿using System.Text.Json.Serialization;

namespace Kanjiru.Kanjiru;

public class KanjiruVocabulary : KanjiruItem<IKanjiruVocabulary>, IKanjiruVocabulary, IKanjiruCreateableItem<IKanjiruVocabulary> {
    public event EventHandler<KanjiruItemChangedEventArgs>? OnItemPropertyChanged;

    [JsonPropertyName("meanings")]
    public List<KanjiruValue> Meanings { get; init; }

    [JsonPropertyName("pronounciationToMeanings")]
    public IDictionary<string, List<KanjiruValue>> PronounciationToMeanings { get; init; }

    [JsonPropertyName("kanji")]
    public List<Guid> Kanji { get; init; }

    public KanjiruVocabulary() : this(default, "", KanjiruItemType.Vocabulary) { }

    private KanjiruVocabulary(IKanjiruDeck? deck, string value, KanjiruItemType itemType) : base(deck, value, itemType) {
        Meanings = new List<KanjiruValue>();
        PronounciationToMeanings = new Dictionary<string, List<KanjiruValue>>();
        Kanji = new List<Guid>();
    }

    public IEnumerable<Guid> GetKanji() => Kanji;

    public bool HasKanji(IKanjiruKanji kanji) =>
        Kanji.Any(kanjiGuid => kanjiGuid.Equals(kanji.GetId()));

    public static IKanjiruVocabulary Create(IKanjiruDeck? deck, string value) =>
        new KanjiruVocabulary(deck, value, KanjiruItemType.Vocabulary);
}
