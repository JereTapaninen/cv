﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Kanjiru.Kanjiru;

public class KanjiruItemChangedEventArgs : EventArgs {
    public string Key { get; init; }
    public object Value { get; init; }

    public KanjiruItemChangedEventArgs(string key, object value) {
        Key = key;
        Value = value;
    }
}

public class KanjiruItem<T> : IKanjiruItem<T> where T : IKanjiruItem<T>
{
    public IKanjiruDeck? Deck { get; init; }

    [JsonPropertyName("id")]
    public Guid Id { get; init; }

    [JsonPropertyName("value")]
    public string Value { get; set; }
    [JsonIgnore]
    protected KanjiruItemType ItemType { get; }

    protected KanjiruItem(IKanjiruDeck? deck, string value, KanjiruItemType itemType)
    {
        Deck = deck;
        Id = Guid.NewGuid();
        Value = value;
        ItemType = itemType;
    }

    public bool Equals(T? other) =>
         other != null && other.GetId().Equals(Id); 

    public Guid GetId() => Id;

    public override bool Equals(object? obj) =>
        Equals(obj as IKanjiruItem<T>);

    public static bool operator ==(KanjiruItem<T>? a, KanjiruItem<T>? b) =>
        (a == null && b == null) || (a?.Equals(b) ?? false);

    public static bool operator !=(KanjiruItem<T>? a, KanjiruItem<T>? b) =>
        !(a == b);

    public string GetValue() => Value;
    public KanjiruItemType GetItemType() => ItemType;

    public override int GetHashCode() =>
        Id.GetHashCode();
}
