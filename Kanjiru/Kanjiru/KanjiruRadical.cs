﻿using System.Text.Json.Serialization;

namespace Kanjiru.Kanjiru;

public class KanjiruRadical : KanjiruItem<IKanjiruRadical>, IKanjiruRadical, IKanjiruCreateableItem<IKanjiruRadical> {
    public event EventHandler<KanjiruItemChangedEventArgs>? OnItemPropertyChanged;

    [JsonPropertyName("meanings")]
    public List<KanjiruValue> Meanings { get; init; }

    public KanjiruRadical() : this(default, "", KanjiruItemType.Radical) { }

    private KanjiruRadical(IKanjiruDeck? deck, string value, KanjiruItemType itemType) : base(deck, value, itemType) {
        Meanings = new List<KanjiruValue>();
    }

    public static IKanjiruRadical Create(IKanjiruDeck? deck, string value) =>
        new KanjiruRadical(deck, value, KanjiruItemType.Radical);
}
