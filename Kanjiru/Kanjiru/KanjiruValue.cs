﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Kanjiru.Kanjiru;

public class KanjiruValue {
    [JsonPropertyName("value")]
    public string Value { get; init; } = "";

    [JsonPropertyName("require")]
    public bool Required { get; init; } = false;
}
