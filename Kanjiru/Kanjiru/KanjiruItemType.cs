﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kanjiru.Kanjiru;
public class KanjiruItemType : IEquatable<KanjiruItemType> {
    public byte Order { get; private init; }
    public string Value { get; private init; }

    public static readonly KanjiruItemType Radical = new("Radical", 0);
    public static readonly KanjiruItemType Kanji = new("Kanji", 1);
    public static readonly KanjiruItemType Vocabulary = new("Vocabulary", 2);

    private KanjiruItemType(string value, byte order) {
        Value = value;
        Order = order;
    }

    public static KanjiruItemType? TryParse(string value) =>
        value switch {
            "Radical" => Radical,
            "Kanji" => Kanji,
            "Vocabulary" => Vocabulary,
            _ => null
        };

    public bool Equals(KanjiruItemType? other) =>
        other?.Value.Equals(Value) ?? false;

    public override bool Equals(object? other) =>
        Equals(other as KanjiruItemType);

    public static bool operator ==(KanjiruItemType? a, KanjiruItemType? b) =>
        a?.Equals(b) ?? false; 

    public static bool operator !=(KanjiruItemType? a, KanjiruItemType? b) =>
        !(a == b);

    public override int GetHashCode() => Value.GetHashCode();
}

