﻿using System.Text.Json.Serialization;

namespace Kanjiru.Kanjiru;

public class KanjiruKanji : KanjiruItem<IKanjiruKanji>, IKanjiruKanji, IKanjiruCreateableItem<IKanjiruKanji> {
    public event EventHandler<KanjiruItemChangedEventArgs>? OnItemPropertyChanged;

    [JsonPropertyName("meanings")]
    public List<KanjiruValue> Meanings { get; init; }

    [JsonPropertyName("kunyomi")]
    public List<KanjiruValue> Kunyomi { get; init; }

    [JsonPropertyName("onyomi")]
    public List<KanjiruValue> Onyomi { get; init; }

    [JsonPropertyName("radicals")]
    public HashSet<Guid> Radicals { get; init; }

    public KanjiruKanji() : this(default, "", KanjiruItemType.Kanji) { }

    private KanjiruKanji(IKanjiruDeck? deck, string value, KanjiruItemType itemType) : base(deck, value, itemType) {
        Meanings = new List<KanjiruValue>();
        Kunyomi = new List<KanjiruValue>();
        Onyomi = new List<KanjiruValue>();
        Radicals = new HashSet<Guid>();
    }

    public IEnumerable<Guid> GetRadicals()
        => Radicals;

    public bool TryAddRadical(Guid radicalId) {
        bool success = Radicals.Add(radicalId);

        if (success)
            OnItemPropertyChanged?.Invoke(this, new("Radicals", radicalId));

        return success;
    }


    public static IKanjiruKanji Create(IKanjiruDeck? deck, string value) =>
        new KanjiruKanji(deck, value, KanjiruItemType.Kanji);
}
