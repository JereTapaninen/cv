﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Kanjiru.Kanjiru;

namespace Kanjiru;

public sealed class DeckEventArgs : EventArgs {
    public IKanjiruDeck Deck { get; init; }

    public DeckEventArgs(IKanjiruDeck deck) {
        Deck = deck;
    }
}

public interface ISettings {
    event EventHandler<DeckEventArgs> OnDeckAdded;
    event EventHandler<DeckEventArgs> OnDeckModified;
    event EventHandler<DeckEventArgs> OnDeckRemoved;
    public IEnumerable<IKanjiruDeck> GetDecks();
    public void AddDeck(IKanjiruDeck deck);
}

public class Settings : ISettings, IJsonOnDeserialized {
    [JsonPropertyName("decks")]
    public List<string> DeckPaths { get; private init; }

    [JsonIgnore]
    private List<IKanjiruDeck> Decks { get; set; }

    public event EventHandler<DeckEventArgs>? OnDeckAdded;
    public event EventHandler<DeckEventArgs>? OnDeckModified;
    public event EventHandler<DeckEventArgs>? OnDeckRemoved;

    public Settings() {
        DeckPaths = new List<string>();
        Decks = new List<IKanjiruDeck>();
    }

    public IEnumerable<IKanjiruDeck> GetDecks()
        => Decks;

    public void AddDeck(IKanjiruDeck deck) {
        deck.OnDeckModified += Deck_OnDeckModified;
        Decks.Add(deck);
        OnDeckAdded?.Invoke(this, new DeckEventArgs(deck));
    }

    private void Deck_OnDeckModified(object? sender, DeckEventArgs e) {
        OnDeckModified?.Invoke(this, e);
    }

    public async void OnDeserialized() {
        foreach (string deckPath in DeckPaths) {
            byte[] bytes = File.ReadAllBytes(deckPath);
            IKanjiruDeck? deck = JsonSerializer.Deserialize<KanjiruDeck>(bytes);
            string? directoryName = Path.GetDirectoryName(deckPath);

            if (deck != null && directoryName != null) {
                await deck.LoadItems(directoryName);
                AddDeck(deck);
            }
        }
    }

    public static ISettings Create()
        => new Settings();
}
