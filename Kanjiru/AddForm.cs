﻿using Kanjiru.AddItemSettings;
using Kanjiru.Kanjiru;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kanjiru;

public partial class AddForm : Form {
    private IReadOnlyDictionary<KanjiruItemType, IKanjiruItemSettingsControl> ItemTypeToSettingsMap { get; init; }

    public AddForm(IKanjiruDeck deck) {
        InitializeComponent();

        ItemTypeToSettingsMap = new Dictionary<KanjiruItemType, UserControl>() {
            {
                KanjiruItemType.Radical,
                new RadicalSettings(deck)
            }
        };

        string[] values = ItemTypeToSettingsMap.Keys.Select(k => k.Value).ToArray();
        TypeComboBox.Items.AddRange(values);
        TypeComboBox.SelectedItem = values.First();
    }

    private void TypeComboBox_SelectedIndexChanged(object sender, EventArgs e) {
        KanjiruItemType? selectedItemType = KanjiruItemType.TryParse(TypeComboBox.SelectedText);
        UserControl control = ItemTypeToSettingsMap[selectedItemType ?? KanjiruItemType.Radical];
        control.Dock = DockStyle.Fill;
        SettingsGroupBox.Controls.Add(control);
    }

    private void AddForm_FormClosing(object sender, FormClosingEventArgs e) {

    }
}
