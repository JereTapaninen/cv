# Kanjiru

This project was spawned out of my interest for learning the Japanese language.
The idea of this project when finished is to be a SRS (spaced repetition system) program that follows the style of Wanikani where you have to type in the correct answer(s), and where each kanji might have required radicals and where each vocabulary item might have required kanji and therefore also radicals. Don't understand the explanation? No worries, the main point is that this is for learning the Japanese language, my own way.

Made in .NET 7, C#11.

## Running the application

### Pre-requisites:

- Visual Studio 2022 (might be able to downgrade or upgrade as well)
- .NET 7

### Steps:

1. Clone the project
2. Open the project with Visual Studio 2022
3. Debug the application.
