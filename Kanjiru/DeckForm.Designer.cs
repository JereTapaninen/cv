﻿namespace Kanjiru;

partial class DeckForm {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
        if (disposing && (components != null)) {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.DeckNameLabel = new System.Windows.Forms.Label();
            this.DeckNameTextBox = new System.Windows.Forms.TextBox();
            this.RadicalListBox = new System.Windows.Forms.ListBox();
            this.RadicalsLabel = new System.Windows.Forms.Label();
            this.KanjiListBox = new System.Windows.Forms.ListBox();
            this.KanjiLabel = new System.Windows.Forms.Label();
            this.VocabularyListBox = new System.Windows.Forms.ListBox();
            this.VocabularyLabel = new System.Windows.Forms.Label();
            this.AddItemButton = new System.Windows.Forms.Button();
            this.ExitDeckAdderButton = new System.Windows.Forms.Button();
            this.ButtonRemoveItem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DeckNameLabel
            // 
            this.DeckNameLabel.AutoSize = true;
            this.DeckNameLabel.Location = new System.Drawing.Point(12, 9);
            this.DeckNameLabel.Name = "DeckNameLabel";
            this.DeckNameLabel.Size = new System.Drawing.Size(42, 15);
            this.DeckNameLabel.TabIndex = 0;
            this.DeckNameLabel.Text = "Name:";
            // 
            // DeckNameTextBox
            // 
            this.DeckNameTextBox.Location = new System.Drawing.Point(12, 27);
            this.DeckNameTextBox.Name = "DeckNameTextBox";
            this.DeckNameTextBox.Size = new System.Drawing.Size(216, 23);
            this.DeckNameTextBox.TabIndex = 1;
            this.DeckNameTextBox.TextChanged += new System.EventHandler(this.DeckNameTextBox_TextChanged);
            // 
            // RadicalListBox
            // 
            this.RadicalListBox.FormattingEnabled = true;
            this.RadicalListBox.ItemHeight = 15;
            this.RadicalListBox.Location = new System.Drawing.Point(12, 128);
            this.RadicalListBox.Name = "RadicalListBox";
            this.RadicalListBox.Size = new System.Drawing.Size(112, 214);
            this.RadicalListBox.TabIndex = 3;
            this.RadicalListBox.SelectedIndexChanged += new System.EventHandler(this.RadicalListBox_SelectedIndexChanged);
            // 
            // RadicalsLabel
            // 
            this.RadicalsLabel.AutoSize = true;
            this.RadicalsLabel.Location = new System.Drawing.Point(12, 104);
            this.RadicalsLabel.Name = "RadicalsLabel";
            this.RadicalsLabel.Size = new System.Drawing.Size(53, 15);
            this.RadicalsLabel.TabIndex = 2;
            this.RadicalsLabel.Text = "Radicals:";
            // 
            // KanjiListBox
            // 
            this.KanjiListBox.FormattingEnabled = true;
            this.KanjiListBox.ItemHeight = 15;
            this.KanjiListBox.Location = new System.Drawing.Point(130, 128);
            this.KanjiListBox.Name = "KanjiListBox";
            this.KanjiListBox.Size = new System.Drawing.Size(112, 214);
            this.KanjiListBox.TabIndex = 5;
            this.KanjiListBox.SelectedIndexChanged += new System.EventHandler(this.KanjiListBox_SelectedIndexChanged);
            // 
            // KanjiLabel
            // 
            this.KanjiLabel.AutoSize = true;
            this.KanjiLabel.Location = new System.Drawing.Point(130, 104);
            this.KanjiLabel.Name = "KanjiLabel";
            this.KanjiLabel.Size = new System.Drawing.Size(36, 15);
            this.KanjiLabel.TabIndex = 4;
            this.KanjiLabel.Text = "Kanji:";
            // 
            // VocabularyListBox
            // 
            this.VocabularyListBox.FormattingEnabled = true;
            this.VocabularyListBox.ItemHeight = 15;
            this.VocabularyListBox.Location = new System.Drawing.Point(248, 128);
            this.VocabularyListBox.Name = "VocabularyListBox";
            this.VocabularyListBox.Size = new System.Drawing.Size(219, 214);
            this.VocabularyListBox.TabIndex = 7;
            this.VocabularyListBox.SelectedIndexChanged += new System.EventHandler(this.VocabularyListBox_SelectedIndexChanged);
            // 
            // VocabularyLabel
            // 
            this.VocabularyLabel.AutoSize = true;
            this.VocabularyLabel.Location = new System.Drawing.Point(248, 104);
            this.VocabularyLabel.Name = "VocabularyLabel";
            this.VocabularyLabel.Size = new System.Drawing.Size(68, 15);
            this.VocabularyLabel.TabIndex = 6;
            this.VocabularyLabel.Text = "Vocabulary:";
            // 
            // AddItemButton
            // 
            this.AddItemButton.Location = new System.Drawing.Point(12, 69);
            this.AddItemButton.Name = "AddItemButton";
            this.AddItemButton.Size = new System.Drawing.Size(455, 23);
            this.AddItemButton.TabIndex = 8;
            this.AddItemButton.Text = "Add Item";
            this.AddItemButton.UseVisualStyleBackColor = true;
            this.AddItemButton.Click += new System.EventHandler(this.AddItemButton_Click);
            // 
            // ExitDeckAdderButton
            // 
            this.ExitDeckAdderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ExitDeckAdderButton.Location = new System.Drawing.Point(199, 404);
            this.ExitDeckAdderButton.Name = "ExitDeckAdderButton";
            this.ExitDeckAdderButton.Size = new System.Drawing.Size(75, 23);
            this.ExitDeckAdderButton.TabIndex = 9;
            this.ExitDeckAdderButton.Text = "Exit";
            this.ExitDeckAdderButton.UseVisualStyleBackColor = true;
            // 
            // ButtonRemoveItem
            // 
            this.ButtonRemoveItem.Location = new System.Drawing.Point(12, 348);
            this.ButtonRemoveItem.Name = "ButtonRemoveItem";
            this.ButtonRemoveItem.Size = new System.Drawing.Size(455, 23);
            this.ButtonRemoveItem.TabIndex = 11;
            this.ButtonRemoveItem.Text = "Remove Selected";
            this.ButtonRemoveItem.UseVisualStyleBackColor = true;
            this.ButtonRemoveItem.Click += new System.EventHandler(this.ButtonRemoveItem_Click);
            // 
            // DeckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 439);
            this.Controls.Add(this.ButtonRemoveItem);
            this.Controls.Add(this.ExitDeckAdderButton);
            this.Controls.Add(this.AddItemButton);
            this.Controls.Add(this.VocabularyListBox);
            this.Controls.Add(this.VocabularyLabel);
            this.Controls.Add(this.KanjiListBox);
            this.Controls.Add(this.KanjiLabel);
            this.Controls.Add(this.RadicalListBox);
            this.Controls.Add(this.RadicalsLabel);
            this.Controls.Add(this.DeckNameTextBox);
            this.Controls.Add(this.DeckNameLabel);
            this.MaximizeBox = false;
            this.Name = "DeckForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kanjiru - Deck {0}";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeckForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private Label DeckNameLabel;
    private TextBox DeckNameTextBox;
    private ListBox RadicalListBox;
    private Label RadicalsLabel;
    private ListBox KanjiListBox;
    private Label KanjiLabel;
    private ListBox VocabularyListBox;
    private Label VocabularyLabel;
    private Button AddItemButton;
    private Button ExitDeckAdderButton;
    private Button ButtonRemoveItem;
}