﻿using Kanjiru.Kanjiru;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kanjiru;
public partial class DeckForm : Form {
    private const string DeckFormTitle = "Kanjiru - Edit Deck: {0}";

    private bool IsOpen { get; set; }
    private IKanjiruDeck CurrentDeck { get; init; }
    private Task UpdateTask { get; init; }

    private readonly DataTable RadicalTable = new();
    private readonly DataTable KanjiTable = new();
    private readonly DataTable VocabularyTable = new();

    public DeckForm(IKanjiruDeck currentDeck) {
        InitializeComponent();

        IsOpen = true;

        UpdateTask = UpdateTaskCreator();

        CurrentDeck = currentDeck;

        SetupListBoxes();

        SetDataOnForm(CurrentDeck);

        CurrentDeck.OnDeckModified += CurrentDeck_OnDeckModified;
    }

    private Task UpdateTaskCreator() =>
        Task.Run(() => {
            while (IsOpen) {
                string kanjiruDeckName = CurrentDeck.GetName();
                if (IsHandleCreated) {
                    Invoke(() => {
                        Text = string.Format(DeckFormTitle, kanjiruDeckName.Equals(string.Empty) ? "(No Name)" : kanjiruDeckName);

                        bool removeButtonEnabled =
                            RadicalListBox.SelectedIndex >= 0 ||
                            KanjiListBox.SelectedIndex >= 0 ||
                            VocabularyListBox.SelectedIndex >= 0;
                        ButtonRemoveItem.Enabled = removeButtonEnabled;
                    });
                }

                Thread.Sleep(10);
            }
        });

    private void SetupListBoxes() {
        _ = RadicalTable.Columns.Add("Item", typeof(IKanjiruRadical));
        _ = RadicalTable.Columns.Add("Name", typeof(string));

        _ = KanjiTable.Columns.Add("Item", typeof(IKanjiruKanji));
        _ = KanjiTable.Columns.Add("Name", typeof(string));

        _ = VocabularyTable.Columns.Add("Item", typeof(IKanjiruVocabulary));
        _ = VocabularyTable.Columns.Add("Name", typeof(string));

        RadicalListBox.DataSource = RadicalTable;
        RadicalListBox.DisplayMember = "Name";
        RadicalListBox.ValueMember = "Item";

        KanjiListBox.DataSource = KanjiTable;
        KanjiListBox.DisplayMember = "Name";
        KanjiListBox.ValueMember = "Item";

        VocabularyListBox.DataSource = VocabularyTable;
        VocabularyListBox.DisplayMember = "Name";
        VocabularyListBox.ValueMember = "Item";
    }

    private void SetDataOnForm(IKanjiruDeck deck) {
        string newName = deck.GetName();
        IKanjiruRadical[] radicals = deck.GetItemsOfType<IKanjiruRadical>(KanjiruItemType.Radical).ToArray();
        IKanjiruKanji[] kanji = deck.GetItemsOfType<IKanjiruKanji>(KanjiruItemType.Kanji).ToArray();
        IKanjiruVocabulary[] vocabulary = deck.GetItemsOfType<IKanjiruVocabulary>(KanjiruItemType.Vocabulary).ToArray();

        DeckNameTextBox.Text = newName;

        int radicalSelectedIndex = RadicalListBox.SelectedIndex;
        int kanjiSelectedIndex = KanjiListBox.SelectedIndex;
        int vocabularySelectedIndex = VocabularyListBox.SelectedIndex;

        RadicalTable.Clear();
        foreach (IKanjiruRadical radical in radicals) {
            _ = RadicalTable.Rows.Add(radical, radical.GetValue());
        }
        RadicalListBox.SelectedIndex = Math.Min(radicalSelectedIndex, RadicalListBox.Items.Count - 1);

        KanjiTable.Clear();
        foreach (IKanjiruKanji kanjiItem in kanji) {
            _ = KanjiTable.Rows.Add(kanjiItem, kanjiItem.GetValue());
        }
        KanjiListBox.SelectedIndex = Math.Min(kanjiSelectedIndex, KanjiListBox.Items.Count - 1);

        VocabularyTable.Clear();
        foreach (IKanjiruVocabulary vocabularyItem in vocabulary) {
            _ = VocabularyTable.Rows.Add(vocabularyItem, vocabularyItem.GetValue());
        }
        VocabularyListBox.SelectedIndex = Math.Min(vocabularySelectedIndex, VocabularyListBox.Items.Count - 1);
    }

    private void CurrentDeck_OnDeckModified(object? sender, DeckEventArgs e) {
        SetDataOnForm(e.Deck);
    }

    private void RadicalListBox_SelectedIndexChanged(object sender, EventArgs e) {
        if (RadicalListBox.SelectedIndex >= 0) {
            KanjiListBox.SelectedIndex = -1;
            VocabularyListBox.SelectedIndex = -1;
        }
    }

    private void KanjiListBox_SelectedIndexChanged(object sender, EventArgs e) {
        if (KanjiListBox.SelectedIndex >= 0) {
            RadicalListBox.SelectedIndex = -1;
            VocabularyListBox.SelectedIndex = -1;
        }
    }

    private void VocabularyListBox_SelectedIndexChanged(object sender, EventArgs e) {
        if (VocabularyListBox.SelectedIndex >= 0) {
            RadicalListBox.SelectedIndex = -1;
            KanjiListBox.SelectedIndex = -1;
        }
    }

    private void DeckNameTextBox_TextChanged(object sender, EventArgs e) {
        CurrentDeck.SetName(DeckNameTextBox.Text);
    }

    private void ButtonRemoveItem_Click(object sender, EventArgs e) {
        IKanjiruRadical? selectedRadical = (IKanjiruRadical?)RadicalListBox.SelectedValue;
        IKanjiruKanji? selectedKanji = (IKanjiruKanji?)KanjiListBox.SelectedValue;
        IKanjiruVocabulary? selectedVocabulary = (IKanjiruVocabulary?)VocabularyListBox.SelectedValue;

        if (selectedRadical == null && selectedKanji == null && selectedVocabulary == null) {
            _ = MessageBox.Show("No items were selected for removal.", "No items selected.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (MessageBox.Show(
            "Remove the selected item?\nThis action is not reversable!",
            "Confirm removal of selected item",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question
        ) != DialogResult.Yes) {
            return;
        }

        bool success = false;
        if (selectedRadical != null) {
            success = CurrentDeck.RemoveItem(selectedRadical);
        } else if (selectedKanji != null) {
            success = CurrentDeck.RemoveItem(selectedKanji);
        } else if (selectedVocabulary != null) {
            success = CurrentDeck.RemoveItem(selectedVocabulary);
        }

        if (!success)
            _ = MessageBox.Show("Could not remove item due to an unknown error!", "Item could not be removed.", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    private async void DeckForm_FormClosing(object sender, FormClosingEventArgs e) {
        IsOpen = false;

        if (!UpdateTask.IsCompleted) {
            e.Cancel = true;
            await UpdateTask;
            e.Cancel = false;
            Close();
        }
    }

    private void AddItemButton_Click(object sender, EventArgs e) {
        new AddForm(CurrentDeck).Show();
    }
}
