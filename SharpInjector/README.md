# SharpInjector

SharpInjector is meant as an add-on tool/plugin for other applications that want to include C# dll creation and/or injection as part of their feature set. The applications might either use SharpInjector.Lib.dll as a dependency, or they could spawn instances of SharpInjector.CLI.exe, and read its output.

## Dll Creation
SharpInjector programmatically creates a DLL from the user-provided C# code using dotnet SDK. The DLL it creates is published using Native AOT and built into a native shared library, this means that the DLL can have native exports that can be used for example by using P/Invoke, and they can be injected into processes and run.

## Dll Injection
SharpInjector uses P/Invoke and Windows API to inject a native DLL into a process. Whether the DLL then executes some code upon injection is left upon the user to decide, but it is capable of that in many ways, one of which is using DllMain. 

How the injection works, is that first SharpInjector opens a handle to the selected process using Kernel32's OpenProcess. It then takes the path to the DLL, takes its length (X), allocates X * character size bytes to the target process's memory using VirtualAllocEx, then writes the path into the target process's memory using WriteProcessMemory. It then gets the handle to the Kernel32 module in the target process itself using GetModuleHandle, after which it will get the procedure address of LoadLibraryW within that remote Kernel32 module. It will then call CreateRemoteThread, with which it will create a thread in that target process. The created thread will call the LoadLibraryW function in the target process's Kernel32 module, and pass in one parameter: the DLL path. So, the created thread will in practice then load the user-provided DLL into the process using LoadLibraryW. If the DLL contains a function such as DllMain, then the DLL will immediately execute code, acting as the target process.

After this process, SharpInjector will perform a clean-up, consisting of CloseHandles and VirtualFreeEx.

This can be then used to read the target process's memory within the target process itself, and manipulate it.


## Running the application

### Pre-requisites:

- Visual Studio 2022
- .NET 7 (ABSOLUTELY NECESSARY!)

### Steps:

1. Clone the project
2. Open the project with Visual Studio 2022
3. Build the application using Release/X64
4. Run SharpInjector.CLI.exe in the build output directory using Powershell:<br />
`./SharpInjector.CLI.exe help`<br />
This will give you the necessary info on how to also run dll injection and creation.
