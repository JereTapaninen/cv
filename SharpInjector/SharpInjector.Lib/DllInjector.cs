﻿using LanguageExt;
using LanguageExt.Common;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace SharpInjector;

public interface IDllInjector {
    public SharpInjectorError Inject(nuint processId, string dllFilePath);
}

public sealed class DllInjector : IDllInjector {
    private const string Kernel32 = "kernel32.dll";
    private const string LoadLibraryFunction = "LoadLibraryW";

    private IWinAPI WinAPI { get; }

    public DllInjector(IWinAPI winAPI) {
        WinAPI = winAPI;
    }

    private static string AppendUnicodeNullTerminator(string text) =>
        text + '\0';

    private static nuint GetFixedPathLength(string path) =>
        (nuint)(path.Length + 1);

    private static Either<SharpInjectorError, IntPtr> ByteArrayToPointer(byte[] buffer) {
        try {
            IntPtr bufferPointer = Marshal.AllocHGlobal(buffer.Length);
            Marshal.Copy(buffer, 0, bufferPointer, buffer.Length);
            return bufferPointer;
        } catch (OutOfMemoryException oome) {
            return new SharpInjectorError(
                SharpInjectorErrorCode.DllInjectionError,
                $"Could not allocate the byte array, ran out of memory. Here's the stacktrace: {oome}"
            );
        }
    }

    private SharpInjectorError Finish(IntPtr processHandle, IntPtr dllPathHandle, IntPtr threadHandle) {
        nuint memoryDeallocationType = (nuint)MemoryDeallocationType.Release;
        bool closedThreadHandle = WinAPI.CloseHandle(threadHandle);
        bool deallocatedMemory = WinAPI.VirtualFree(processHandle, dllPathHandle, UIntPtr.Zero, memoryDeallocationType);
        bool closedProcessHandle = WinAPI.CloseHandle(processHandle);

        return closedThreadHandle && deallocatedMemory && closedProcessHandle ?
            new SharpInjectorError(SharpInjectorErrorCode.Success, "No errors caught while injecting!") :
            new SharpInjectorError(
                SharpInjectorErrorCode.DllInjectionError,
                $"Could not clean up after injection. This error is NOT critical, and you need not take any action.\nHere's the last P/Invoke error, which might or might not be helpful: {Marshal.GetLastPInvokeError()}"
            );
    }

    private SharpInjectorError GetExitCodeAndFinish(IntPtr processHandle, IntPtr dllPathHandle, IntPtr threadHandle) {
        bool gotExitCodeFromThread = WinAPI.GetExitCodeThread(threadHandle, out UIntPtr threadExitCode);

        SharpInjectorError nextError = Finish(processHandle, dllPathHandle, threadHandle);

        return gotExitCodeFromThread ?
            !threadExitCode.Equals(0) ?
                nextError :
                new SharpInjectorError(
                    nextError.Code != SharpInjectorErrorCode.DllInjectionError ? nextError.Code : SharpInjectorErrorCode.DllInjectionError,
                    $"Thread didn't exit successfully. That doesn't really matter, as it might've been intentional, so we're continuing to the next step.\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}\n\nAnd here's the next error message, if there was any: {nextError.Message}"
                ) :
            new SharpInjectorError(
                nextError.Code != SharpInjectorErrorCode.DllInjectionError ? nextError.Code : SharpInjectorErrorCode.DllInjectionError,
                $"Couldn't get exit code from thread. That doesn't really matter, so we're continuing to the next step.\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}\n\nAnd here's the next error message, if there was any: {nextError.Message}"
            );
    }

    private SharpInjectorError ContinueInjectWithRemoteThreadHandle(IntPtr processHandle, IntPtr dllPathHandle, IntPtr threadHandle) {
        nuint result = WinAPI.WaitForSingleObject(threadHandle, uint.MaxValue);
        SharpInjectorError nextError = GetExitCodeAndFinish(processHandle, dllPathHandle, threadHandle);
        return !result.Equals(uint.MaxValue) ?
            nextError :
            new SharpInjectorError(
                nextError.Code != SharpInjectorErrorCode.DllInjectionError ? nextError.Code : SharpInjectorErrorCode.DllInjectionError,
                $"Could not wait for the thread to exit. That doesn't really matter, so we're continuing to the next step.\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}\n\nAnd here's the next error message, if there was any: {nextError.Message}"
            );
    }

    private SharpInjectorError ContinueInjectWithLoadLibraryHandle(IntPtr processHandle, IntPtr dllPathHandle, IntPtr loadLibraryAddr) {
        IntPtr threadHandle = WinAPI.CreateRemoteThread(
            processHandle,
            IntPtr.Zero,
            UIntPtr.Zero,
            loadLibraryAddr,
            dllPathHandle,
            UIntPtr.Zero,
            out UIntPtr _
        );
        return !threadHandle.Equals(0) ?
            ContinueInjectWithRemoteThreadHandle(processHandle, dllPathHandle, threadHandle) :
            new SharpInjectorError(
                SharpInjectorErrorCode.DllInjectionError,
                $"Could not create remote thread. The process has most likely exited, or your dll doesn't exist.\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}"
            );
    }


    private SharpInjectorError ContinueInjectWithKernel32ModuleHandle(IntPtr processHandle, IntPtr dllPathHandle, IntPtr kernel32ModuleHandle) {
        IntPtr loadLibraryAddr = WinAPI.GetProcedureAddress(kernel32ModuleHandle, LoadLibraryFunction);
        return !loadLibraryAddr.Equals(0) ?
            ContinueInjectWithLoadLibraryHandle(processHandle, dllPathHandle, loadLibraryAddr) :
            new SharpInjectorError(
                SharpInjectorErrorCode.DllInjectionError,
                $"Could not get {LoadLibraryFunction}. How old is your Windows again?\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}"
            );
    }

    private SharpInjectorError ContinueInjectAfterProcessMemoryWrite(IntPtr processHandle, IntPtr dllPathHandle) {
        IntPtr kernel32ModuleHandle = WinAPI.GetModuleHandle(Kernel32);
        return !kernel32ModuleHandle.Equals(0) ?
            ContinueInjectWithKernel32ModuleHandle(processHandle, dllPathHandle, kernel32ModuleHandle) :
            new SharpInjectorError(
                SharpInjectorErrorCode.DllInjectionError,
                $"Could not get kernel32 module... Are you even running this on Windows?\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}"
            );
    }

    private SharpInjectorError InjectWithPathHandle(IntPtr processHandle, IntPtr dllPathHandle, IntPtr path, nuint pathBufferLength) {
        bool writeWasSuccessful = WinAPI.WriteProcessMemory(
           processHandle,
           dllPathHandle,
           path,
           pathBufferLength,
           out UIntPtr bytesWritten
       );
        return writeWasSuccessful && !bytesWritten.Equals(0) ?
            ContinueInjectAfterProcessMemoryWrite(processHandle, dllPathHandle) :
            new SharpInjectorError(
                SharpInjectorErrorCode.DllInjectionError,
                $"Could not write to process memory. Only God knows why, and he ain't telling.\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}"
            );
    }

    private SharpInjectorError InjectWithProcessHandle(IntPtr processHandle, IntPtr path, nuint pathBufferLength, nuint pathLength) {
        nuint memoryAllocationType = (nuint)(MemoryAllocationType.Commit | MemoryAllocationType.Reserve);
        nuint memoryProtection = (nuint)MemoryProtection.ReadWrite;

        IntPtr dllPathHandle = WinAPI.VirtualAlloc(
            processHandle,
            IntPtr.Zero,
            pathLength,
            memoryAllocationType,
            memoryProtection
        );
        return !dllPathHandle.Equals(0) ?
            InjectWithPathHandle(processHandle, dllPathHandle, path, pathBufferLength) :
            new SharpInjectorError(
                SharpInjectorErrorCode.DllInjectionError,
                $"Could not get the handle of the DLL path, either the process has exited or the path is invalid.?\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}"
            );
    }

    private SharpInjectorError InjectWithPathAsPointer(nuint processId, IntPtr path, nuint pathBufferLength, nuint pathLength) {
        nuint processAccessRights = (nuint)ProcessAccessRights.All;

        IntPtr processHandle = WinAPI.OpenProcess(
            processAccessRights,
            false,
            processId
        );
        return !processHandle.Equals(0) ?
            InjectWithProcessHandle(processHandle, path, pathBufferLength, pathLength) :
            new SharpInjectorError(
                SharpInjectorErrorCode.DllInjectionError,
                $"Could not get process handle, perhaps process doesn't exist with the given id?\nHere's the last P/Invoke error: {Marshal.GetLastPInvokeError()}"
            );
    }

    private SharpInjectorError InjectWithPath(nuint processId, string dllFilePath, nuint pathLength) {
        byte[] buffer = Encoding.Unicode.GetBytes(AppendUnicodeNullTerminator(dllFilePath));
        return ByteArrayToPointer(buffer)
            .Match(
                pointer => InjectWithPathAsPointer(processId, pointer, (nuint)buffer.Length, pathLength),
                error => error
            );
    }

    public SharpInjectorError Inject(nuint processId, string dllFilePath) =>
        !string.IsNullOrEmpty(dllFilePath) && File.Exists(dllFilePath) ?
            InjectWithPath(processId, dllFilePath, GetFixedPathLength(dllFilePath)) :
            new SharpInjectorError(SharpInjectorErrorCode.InvalidArguments, "The provided dll path was null, empty or didn't exist!");
}
