﻿using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using LanguageExt;
using LanguageExt.UnsafeValueAccess;

namespace SharpInjector;

public interface IDllCreator {
    public Either<SharpInjectorError, string> CreateDll(string outputName, string filePath, string outputPath);
}

public sealed class DllCreator : IDllCreator {
    private IIOHelper FileSystemHelper { get; }

    public DllCreator(IIOHelper ioHelper) {
        FileSystemHelper = ioHelper;
    }

    public Either<SharpInjectorError, string> CreateDll(string outputName, string filePath, string outputPath) {
        if (Directory.Exists(outputPath))
            Directory.Delete(outputPath, true);

        return FileSystemHelper.CreateDirectory(outputPath)
            .Match(
                createdDirectory => OnOutputDirectoryCreated(outputName, filePath, createdDirectory),
                error => OnInvalidOutputDirectory(error)
            );
    }

    private Either<SharpInjectorError, string> TryMoveDllBackToOutput(string temporaryMovePath, string outputPath) =>
        FileSystemHelper.MoveFile(temporaryMovePath, outputPath)
            .Match(
                _ => (Either<SharpInjectorError, string>)outputPath,
                error => new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, $"Could not move dll file back to output path from temporary location!\n{error.Message}")
            );

    private Either<SharpInjectorError, string> TryRecreateOutputDirectoryAndMoveFileBack(string temporaryMovePath, string outputPath, string outputDirectory) =>
        FileSystemHelper.CreateDirectory(outputDirectory)
            .Match(
                _ => TryMoveDllBackToOutput(temporaryMovePath, outputPath),
                error => new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, $"Could not create new output directory!\n{error.Message}")
            );

    private Either<SharpInjectorError, string> TryPurgeAndRecreateOutputDirectoryAndMoveFileBack(string temporaryMovePath, string outputPath, string outputDirectory) =>
        FileSystemHelper.DeleteDirectory(outputDirectory, true)
            .Match(
                _ => TryRecreateOutputDirectoryAndMoveFileBack(temporaryMovePath, outputPath, outputDirectory),
                error => new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, $"Could not remove files from the output directory!\n{error.Message}")
            );

    private Either<SharpInjectorError, string> TryMoveDllToTemporaryLocationAndPurgeOutputDirectoryAndMoveDllBack(string temporaryMovePath, string outputPath, string dllPath, string outputDirectory) =>
        FileSystemHelper.MoveFile(dllPath, temporaryMovePath)
            .Match(
                _ => TryPurgeAndRecreateOutputDirectoryAndMoveFileBack(temporaryMovePath, outputPath, outputDirectory),
                error => new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, $"Could not move dll from old path to temporary location!\n{error.Message}")
            );

    private Either<SharpInjectorError, string> TryMoveDllToTemporaryLocationAndPurgeOutputDirectoryAndMoveDllBack(string temporaryMoveFolder, string temporaryMovePath, string outputPath, string dllPath, string outputDirectory) =>
        !Directory.Exists(temporaryMoveFolder) ?
            FileSystemHelper.CreateDirectory(temporaryMoveFolder)
                .Match(
                    _ => TryMoveDllToTemporaryLocationAndPurgeOutputDirectoryAndMoveDllBack(temporaryMovePath, outputPath, dllPath, outputDirectory),
                    error => new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, $"Could not create new temporary move folder!\n{error.Message}")
                ) :
                TryMoveDllToTemporaryLocationAndPurgeOutputDirectoryAndMoveDllBack(temporaryMovePath, outputPath, dllPath, outputDirectory);

    private Either<SharpInjectorError, string> TryPurgeOutputDirectoryOfEverythingElseButDll(string dllPath, string dllName, DirectoryInfo outputDirectoryInfo) {
        string temporaryMoveFolder = Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "/SharpInjector/");
        string temporaryMovePath = Path.Join(temporaryMoveFolder, dllName);
        string outputPath = Path.Join(outputDirectoryInfo.FullName, dllName);

        return File.Exists(temporaryMovePath) ?
            FileSystemHelper.DeleteFile(temporaryMovePath).Match(
                _ => TryMoveDllToTemporaryLocationAndPurgeOutputDirectoryAndMoveDllBack(temporaryMoveFolder, temporaryMovePath, outputPath, dllPath, outputDirectoryInfo.FullName),
                error => new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, $"Could not delete already existing file in a temporary move folder!\n{error.Message}")
            ) :
            TryMoveDllToTemporaryLocationAndPurgeOutputDirectoryAndMoveDllBack(temporaryMoveFolder, temporaryMovePath, outputPath, dllPath, outputDirectoryInfo.FullName);
    }


    private Either<SharpInjectorError, string> CheckIfFileExistsAndTryPurgeOutputDirectoryOfEverythingElseButDll(string dllPath, string dllName, DirectoryInfo outputDirectoryInfo) =>
        File.Exists(dllPath) && Directory.Exists(outputDirectoryInfo.FullName) ?
            TryPurgeOutputDirectoryOfEverythingElseButDll(dllPath, dllName, outputDirectoryInfo) :
            new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, "Could not find dll path or output directory anymore!\nDid they get removed?");

    private Either<SharpInjectorError, string> OnDotnetProjectPublishSucceeded(string results, string outputFileName, DirectoryInfo outputDirectoryInfo) {
        MatchCollection regexMatch = Regex.Matches(results, $"{outputFileName} -> (.*)", RegexOptions.Multiline);
        string fileName = $"{outputFileName}.dll";
        Option<string> regexOutput = regexMatch.Count > 0 && regexMatch[^1].Groups.Count > 1 ?
            regexMatch[^1].Groups[1].Value
                .Replace("\r", string.Empty, StringComparison.Ordinal)
                .Replace("\n", string.Empty, StringComparison.Ordinal) :
            Option<string>.None;
        Option<string> dllFilePath = regexOutput.Match(
            output => Path.Join(output, $"/{fileName}"),
            Option<string>.None
        );

        return dllFilePath.Match(
            filePath => File.Exists(filePath) ?
                CheckIfFileExistsAndTryPurgeOutputDirectoryOfEverythingElseButDll(filePath, fileName, outputDirectoryInfo) :
                new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, $"Published dll file could not be found!\nThe file path: {filePath}\nThe regex match: {regexOutput}"),
            new SharpInjectorError(SharpInjectorErrorCode.RegexError, $"Could not parse file path from dotnet cli output!\nThe file path: {dllFilePath}\nThe results were: {results}")
        );
    }

    private static SharpInjectorError OnDotnetProjectPublishFailed(SharpInjectorError error) =>
        new(error.Code, $"Could not publish the dotnet project!\n{error.Message}");

    private Either<SharpInjectorError, string> TryWriteUserInputtedProgramAndTryPublish(string outputFileName, DirectoryInfo outputDirectoryInfo, string contents, string newClassFile) =>
        FileSystemHelper.WriteAllTextToFile(newClassFile, contents)
            .Match(
                _ =>
                PublishDotnetProject(outputDirectoryInfo.FullName)
                    .Match(
                        results => OnDotnetProjectPublishSucceeded(results, outputFileName, outputDirectoryInfo),
                        error => OnDotnetProjectPublishFailed(error)
                    ),
                error => new SharpInjectorError(SharpInjectorErrorCode.RegexError, $"Could not write your input file's contents to Program.cs!\n{error.Message}")
            );

    private Either<SharpInjectorError, string> TryPublishDll(string outputFileName, string filePath, DirectoryInfo outputDirectoryInfo, string newClassFile) =>
        FileSystemHelper.ReadAllTextFromFile(filePath, Encoding.UTF8)
            .Match(
                contents => TryWriteUserInputtedProgramAndTryPublish(outputFileName, outputDirectoryInfo, contents, newClassFile),
                error => new SharpInjectorError(SharpInjectorErrorCode.RegexError, $"Could not read your input file!\n{error.Message}")
            );

    private Either<SharpInjectorError, string> OnDotnetProjectCreationSucceeded(string outputFileName, string filePath, DirectoryInfo outputDirectoryInfo) {
        string defaultClassFile = $"{outputDirectoryInfo.FullName}/Class1.cs";
        string newClassFile = $"{outputDirectoryInfo.FullName}/EntryPoint.cs";


        return File.Exists(defaultClassFile) ?
            FileSystemHelper.DeleteFile(defaultClassFile)
                .Match(
                    _ => TryPublishDll(outputFileName, filePath, outputDirectoryInfo, newClassFile),
                    error => new SharpInjectorError(SharpInjectorErrorCode.RegexError, $"Could not delete extra class file!\n{error.Message}")
                ) :
                TryPublishDll(outputFileName, filePath, outputDirectoryInfo, newClassFile);
    }

    private Either<SharpInjectorError, string> OnOutputDirectoryCreated(string outputName, string filePath, DirectoryInfo outputDirectoryInfo) =>
        CreateDotnetProject(outputDirectoryInfo.FullName)
            .Match(
                output => output.Contains("was created successfully.", StringComparison.OrdinalIgnoreCase) ?
                    OnDotnetProjectCreationSucceeded(outputName, filePath, outputDirectoryInfo) :
                    new SharpInjectorError(SharpInjectorErrorCode.ProcessError, $"Could not create dotnet project with dotnet cli.\nHere's the error code from dotnet cli: {output}"),
                error => error
            );

    private static SharpInjectorError OnInvalidOutputDirectory(SharpInjectorError oldError)
        => new(
            oldError.Code,
            "Could not create output path!\n" +
            "Please check that the provided output path string is in correct format, isn't too long, and doesn't contain special characters!\n" +
            $"You might need to run {AppDomain.CurrentDomain.FriendlyName} in privileged mode.\n" +
            oldError.Message
        );


    private static Either<SharpInjectorError, string> PublishDotnetProject(string workingDirectory) {
        using var process = new Process {
            StartInfo = new ProcessStartInfo {
                FileName = "dotnet",
                Arguments = "publish /p:PublishAot=true /p:NativeLib=Shared -c release -r win-x64 /p:Optimize=true /p:DebugSymbols=false /p:DebugType=none",
                RedirectStandardOutput = true,
                RedirectStandardInput = false,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                WorkingDirectory = workingDirectory
            }
        };
        bool processStarted = process.Start();

        static string readOutputFromProcess(Process process) {
            string output = process.StandardOutput.ReadToEnd() + process.StandardError.ReadToEnd();
            process.WaitForExit();
            return output;
        }

        return processStarted ?
            readOutputFromProcess(process) :
            new SharpInjectorError(SharpInjectorErrorCode.ProcessError, "Could not start dotnet CLI (to run dotnet publish)");
    }

    private static Either<SharpInjectorError, string> CreateDotnetProject(string absoluteOutputPath) {
        using var process = new Process {
            StartInfo = new ProcessStartInfo {
                FileName = "dotnet",
                Arguments = "new classlib -f \"net7.0\" --force",
                RedirectStandardOutput = true,
                RedirectStandardInput = false,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                WorkingDirectory = absoluteOutputPath
            }
        };
        bool processStarted = process.Start();

        static string readOutputFromProcess(Process process) {
            string output = process.StandardOutput.ReadToEnd() + process.StandardError.ReadToEnd();
            process.WaitForExit();
            return output;
        }

        return processStarted ?
            readOutputFromProcess(process) :
            new SharpInjectorError(SharpInjectorErrorCode.ProcessError, "Could not start dotnet CLI (to run dotnet new)");
    }
}
