﻿using System.Text;
using LanguageExt;

namespace SharpInjector;

public interface IIOHelper {
    Either<SharpInjectorError, DirectoryInfo> CreateDirectory(string path);
    Either<SharpInjectorError, Unit> DeleteFile(string path);
    public Either<SharpInjectorError, Unit> MoveFile(string fromPath, string toPath);
    public Either<SharpInjectorError, Unit> DeleteDirectory(string path, bool recursive = false);
    public Either<SharpInjectorError, string> ReadAllTextFromFile(string filePath, Encoding encoding);
    public Either<SharpInjectorError, Unit> WriteAllTextToFile(string filePath, string contents);
}

public sealed class IOHelper : IIOHelper {
#pragma warning disable CA1031
    public Either<SharpInjectorError, DirectoryInfo> CreateDirectory(string path) {
        try {
            return Directory.CreateDirectory(path);
        } catch (Exception ex) {
            return new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, ex.Message);
        }
    }

    public Either<SharpInjectorError, Unit> DeleteDirectory(string path, bool recursive = false) {
        try {
            Directory.Delete(path, recursive);
            return Unit.Default;
        } catch (Exception ex) {
            return new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, ex.Message);
        }
    }

    public Either<SharpInjectorError, Unit> DeleteFile(string path) {
        try {
            File.Delete(path);
            return Unit.Default;
        } catch (Exception ex) {
            return new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, ex.Message);
        }
    }

    public Either<SharpInjectorError, Unit> MoveFile(string fromPath, string toPath) {
        try {
            File.Move(fromPath, toPath);
            return Unit.Default;
        } catch (Exception ex) {
            return new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, ex.Message);
        }
    }

    public Either<SharpInjectorError, string> ReadAllTextFromFile(string filePath, Encoding encoding) {
        try {
            return File.ReadAllText(filePath, encoding);
        } catch (Exception ex) {
            return new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, ex.Message);
        }
    }

    public Either<SharpInjectorError, Unit> WriteAllTextToFile(string filePath, string contents) {
        try {
            File.WriteAllText(filePath, contents);
            return Unit.Default;
        } catch (Exception ex) {
            return new SharpInjectorError(SharpInjectorErrorCode.FileSystemError, ex.Message);
        }
    }
}

