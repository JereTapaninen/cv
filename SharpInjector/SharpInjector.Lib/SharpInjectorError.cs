﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpInjector;

public enum SharpInjectorErrorCode {
    Success = 0,
    InvalidArguments = 1,
    FileSystemError = 2,
    ProcessError = 3,
    RegexError = 4,
    DllInjectionError = 5
}

public readonly record struct SharpInjectorError(SharpInjectorErrorCode Code, string Message);
