﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.Marshalling;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SharpInjector;

public enum ProcessAccessRights : uint {
    All = 0x0002 | 0x0400 | 0x0008 | 0x0020 | 0x0010,
    None = 0
}

[Flags]
public enum MemoryAllocationType : uint {
    Commit = 0x00001000,
    Reserve = 0x00002000,
    None = 0
}

[Flags]
public enum MemoryProtection : uint {
    ReadWrite = 0x00000004,
    None = 0
}

[Flags]
public enum MemoryDeallocationType {
    Release = 0x00008000,
    None = 0
}

public interface IWinAPI {
    public IntPtr OpenProcess(
        nuint processAccessRights,
        bool inheritHandle,
        nuint processId
    );
    public IntPtr VirtualAlloc(
        IntPtr processHandle,
        IntPtr regionOfPagesAllocationStartingAddress,
        nuint sizeToAllocateInBytes,
        nuint memoryAllocationType,
        nuint memoryProtection
    );
    public bool WriteProcessMemory(
        IntPtr processHandle,
        IntPtr baseAddress,
        IntPtr buffer,
        nuint sizeOfBuffer,
        out UIntPtr bytesWritten
    );
    public IntPtr CreateRemoteThread(
        IntPtr processHandle,
        IntPtr threadAttributes,
        nuint stackSize,
        IntPtr startingAddress,
        IntPtr parameters,
        nuint creationFlags,
        out UIntPtr threadId
    );
    public IntPtr GetProcedureAddress(IntPtr moduleHandle, string procedureName);
    public IntPtr GetModuleHandle(string moduleName);
    public nuint WaitForSingleObject(IntPtr handle, nuint milliseconds);
    public bool VirtualFree(
        IntPtr processHandle,
        IntPtr regionOfPagesDeallocationStartingAddress,
        nuint sizeToDeallocateInBytes,
        nuint memoryDeallocationType
    );
    public bool GetExitCodeThread(IntPtr threadHandle, out UIntPtr exitCode);
    public bool CloseHandle(IntPtr objectHandle);
}

public partial class WinAPI : IWinAPI {
    public IntPtr OpenProcess(nuint processAccessRights, bool inheritHandle, nuint processId) =>
        OpenProcessWinAPI(processAccessRights, inheritHandle, processId);

    public IntPtr VirtualAlloc(
        IntPtr processHandle,
        IntPtr regionOfPagesAllocationStartingAddress,
        nuint sizeToAllocateInBytes,
        nuint memoryAllocationType,
        nuint memoryProtection
    ) =>
        VirtualAllocExWinAPI(processHandle, regionOfPagesAllocationStartingAddress, sizeToAllocateInBytes, memoryAllocationType, memoryProtection);

    public bool WriteProcessMemory(
        IntPtr processHandle,
        IntPtr baseAddress,
        IntPtr buffer,
        nuint sizeOfBuffer,
        out UIntPtr bytesWritten
    ) =>
        WriteProcessMemoryWinAPI(processHandle, baseAddress, buffer, sizeOfBuffer, out bytesWritten);

    public IntPtr CreateRemoteThread(
        IntPtr processHandle,
        IntPtr threadAttributes,
        nuint stackSize,
        IntPtr startingAddress,
        IntPtr parameters,
        nuint creationFlags,
        out UIntPtr threadId
    ) =>
        CreateRemoteThreadWinAPI(processHandle, threadAttributes, stackSize, startingAddress, parameters, creationFlags, out threadId);

    public IntPtr GetProcedureAddress(IntPtr moduleHandle, string procedureName) =>
        GetProcAddressWinAPI(moduleHandle, procedureName);

    public IntPtr GetModuleHandle(string moduleName) =>
        GetModuleHandleWWinAPI(moduleName);

    public nuint WaitForSingleObject(IntPtr handle, nuint milliseconds) =>
        WaitForSingleObjectWinAPI(handle, milliseconds);

    public bool VirtualFree(
        IntPtr processHandle,
        IntPtr regionOfPagesDeallocationStartingAddress,
        nuint sizeToDeallocateInBytes,
        nuint memoryDeallocationType
    ) =>
        VirtualFreeExWinAPI(processHandle, regionOfPagesDeallocationStartingAddress, sizeToDeallocateInBytes, memoryDeallocationType);

    public bool GetExitCodeThread(IntPtr threadHandle, out UIntPtr exitCode) =>
        GetExitCodeThreadWinAPI(threadHandle, out exitCode);

    public bool CloseHandle(IntPtr objectHandle) =>
        CloseHandleWinAPI(objectHandle);


    [LibraryImport("kernel32.dll", EntryPoint = "OpenProcess", SetLastError = true)]
    private static partial IntPtr OpenProcessWinAPI(
        nuint processAccess,
        [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle,
        nuint processId
    );

    [LibraryImport("kernel32.dll", EntryPoint = "VirtualAllocEx", SetLastError = true)]
    private static partial IntPtr VirtualAllocExWinAPI(
        IntPtr hProcess,
        IntPtr lpAddress,
        nuint dwSize,
        nuint flAllocationType,
        nuint flProtect
    );

    [LibraryImport("kernel32.dll", EntryPoint = "WriteProcessMemory", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static partial bool WriteProcessMemoryWinAPI(
        IntPtr hProcess,
        IntPtr lpBaseAddress,
        IntPtr lpBuffer,
        nuint nSize,
        out UIntPtr lpNumberOfBytesWritten
    );

    [LibraryImport("kernel32.dll", EntryPoint = "CreateRemoteThread", SetLastError = true)]
    private static partial IntPtr CreateRemoteThreadWinAPI(
        IntPtr hProcess,
        IntPtr lpThreadAttributes,
        nuint dwStackSize,
        IntPtr lpStartAddress,
        IntPtr lpParameter,
        nuint dwCreationFlags,
        out UIntPtr lpThreadId
    );

    [LibraryImport("kernel32", EntryPoint = "GetProcAddress", SetLastError = true, StringMarshalling = StringMarshalling.Custom, StringMarshallingCustomType = typeof(AnsiStringMarshaller))]
    private static partial IntPtr GetProcAddressWinAPI(IntPtr hModule, string procName);

    [LibraryImport("kernel32.dll", EntryPoint = "GetModuleHandleW", SetLastError = true, StringMarshalling = StringMarshalling.Utf16)]
    private static partial IntPtr GetModuleHandleWWinAPI(string lpModuleName);

    [LibraryImport("kernel32.dll", EntryPoint = "WaitForSingleObject", SetLastError = true)]
    private static partial nuint WaitForSingleObjectWinAPI(IntPtr hHandle, nuint dwMilliseconds);

    [LibraryImport("kernel32.dll", EntryPoint = "VirtualFreeEx", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static partial bool VirtualFreeExWinAPI(
        IntPtr hProcess,
        IntPtr lpAddress,
        nuint dwSize,
        nuint dwFreeType
    );

    [LibraryImport("kernel32.dll", EntryPoint = "GetExitCodeThread", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static partial bool GetExitCodeThreadWinAPI(IntPtr hThread, out UIntPtr lpExitCode);

    [LibraryImport("kernel32.dll", EntryPoint = "CloseHandle", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static partial bool CloseHandleWinAPI(IntPtr hObject);
}
