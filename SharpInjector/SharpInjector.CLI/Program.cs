﻿using LanguageExt;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Resources;

namespace SharpInjector;

public static class Program {
    private static readonly ResourceManager Resources = new("SharpInjector.Resources", Assembly.GetExecutingAssembly());
    private static readonly CultureInfo EnglishLocalisation = new("en");

    private static readonly IWinAPI WinAPI = new WinAPI();
    private static readonly IIOHelper FileSystemHelper = new IOHelper();
    private static readonly IDllCreator DllCreator = new DllCreator(FileSystemHelper);
    private static readonly IDllInjector DllInjector = new DllInjector(WinAPI);

    private static string GetLocalization(string key) =>
        Resources.GetString(key, EnglishLocalisation) ?? "";

    private static SharpInjectorErrorCode OnCreateDllArgumentsValidated(string outputName, string filePath, string outputPath) {
        Either<SharpInjectorError, string> possibleOutput = DllCreator.CreateDll(outputName, filePath, outputPath);

        static SharpInjectorErrorCode OnOutputCreated(string possibleOutput) {
            Console.WriteLine(possibleOutput);
            return SharpInjectorErrorCode.Success;
        }

        static SharpInjectorErrorCode OnError(SharpInjectorError error) {
            Console.WriteLine(error.Message);
            _ = FileSystemHelper.WriteAllTextToFile("./DllCreator.log", error.Message)
                .Match(
                    _ => {
                        Console.WriteLine("The error has been logged into DllCreator.log.");
                        return Unit.Default;
                    },
                    err => {
                        Console.WriteLine("Couldn't log the error output into a file ... Here's the error for that:\n{0}", err.Message);
                        return Unit.Default;
                    }
                );
            return error.Code;
        }

        return possibleOutput.Match(
            OnOutputCreated,
            OnError
        );
    }

    private static SharpInjectorErrorCode OnInvalidArguments() {
        Console.WriteLine(GetLocalization("IncorrectArgumentsText1"));
        Console.WriteLine(GetLocalization("IncorrectArgumentsText2"));
        Console.WriteLine(GetLocalization("IncorrectArgumentsText3"), AppDomain.CurrentDomain.FriendlyName);
        return SharpInjectorErrorCode.InvalidArguments;
    }

    private static SharpInjectorErrorCode OnCreateDll(string[] args) =>
        args switch {
            _ when args.Length == 4 && args[1].Length > 0 && File.Exists(args[2]) && args[3].Length > 0 =>
                OnCreateDllArgumentsValidated(args[1], args[2], Path.Join(args[3], args[1])),
            _ when args.Length == 3 && args[1].Length > 0 && File.Exists(args[2]) =>
                OnCreateDllArgumentsValidated(args[1], args[2], $"output/{args[1]}/"),
            _ => OnInvalidArguments()
        };

    private static SharpInjectorErrorCode OnInjectDll(string[] args) {
        static SharpInjectorErrorCode StartInject(uint processId, string filePath) {
            SharpInjectorError output = DllInjector.Inject(processId, filePath);

            static SharpInjectorErrorCode OnSuccess(SharpInjectorError success) {
                Console.WriteLine(success.Message);
                return success.Code;
            }

            static SharpInjectorErrorCode OnError(SharpInjectorError error) {
                Console.WriteLine(error.Message);
                _ = FileSystemHelper.WriteAllTextToFile("./DllInjector.log", error.Message)
                    .Match(
                        _ => {
                            Console.WriteLine("The errors have been logged into DllInjector.log.");
                            return Unit.Default;
                        },
                        err => {
                            Console.WriteLine("Couldn't log the error output into a file ... Here's the error for that:\n{0}", err.Message);
                            return Unit.Default;
                        }
                    );
                return error.Code;
            }

            return output.Code switch {
                SharpInjectorErrorCode.Success => OnSuccess(output),
                _ => OnError(output)
            };
        }

        return args switch {
            _ when args.Length == 3 && args[1].Length > 0 && uint.TryParse(args[1], out uint processId) && File.Exists(args[2]) =>
                StartInject(processId, args[2]),
            _ => OnInvalidArguments()
        };
    }

    private static SharpInjectorErrorCode OnHelp() {
        Console.WriteLine("--- SharpInjector - Command Line Interface - Help and About ---\n");
        Console.WriteLine("SharpInjector is a tool for creating DLLs using C# which can then be injected into running applications.");
        Console.WriteLine("SharpInjector is NOT meant for the end-user; SharpInjector is meant as a tool to be used by other programs which utilize DLL creation and/or injection.");
        Console.WriteLine("You may still utilize SharpInjector's DLL generation and injection capabilities as an end-user, but the features that SharpInjector provide might end up being insufficient for your specific use-case.\n");
        Console.WriteLine("--- Safety precautions - PLEASE READ ---\n");
        Console.WriteLine("SharpInjector, used in the wrong way, is fully capable of destroying your operating system. You need to pay extra attention to the paths you provide, especially when creating a DLL.");
        Console.WriteLine("SharpInjector needs to delete the provided output directory when creating a DLL. Do NOT under any circumstance set the output directory to anywhere close your critical files, especially operating system files!");
        Console.WriteLine("When providing a path, stay as far away from the drive root as possible.\n");
        Console.WriteLine("--- Available commands ---\n");
        Console.WriteLine("==> create-dll [OUTPUTNAME (REQUIRED)] [FILETOCOMPILE (REQUIRED)] [OUTPUTDIRECTORY]");
        Console.WriteLine("\tCreates a DLL by compiling the provided C# file to Native AOT. Output path is OUTPUTDIRECTORY/OUTPUTNAME/OUTPUTNAME.dll, or ./output/OUTPUTNAME/OUTPUTNAME.dll when not providing the OUTPUTDIRECTORY.\n");
        Console.WriteLine("==> inject-dll [PID (REQUIRED)] [DLLFILEPATH (REQUIRED)]");
        Console.WriteLine("\tInjects the provided DLL at DLLFILEPATH into a process with the provided PID (process ID).\n");
        Console.WriteLine("==> help");
        Console.WriteLine("\tShows helpful information about SharpInjector. You are here.");
        return SharpInjectorErrorCode.Success;
    }

    public static int Main(string[] args) =>
        (int)(args switch {
            _ when args.Length > 1 && args[0].Equals("create-dll", StringComparison.OrdinalIgnoreCase) => OnCreateDll(args),
            _ when args.Length > 1 && args[0].Equals("inject-dll", StringComparison.OrdinalIgnoreCase) => OnInjectDll(args),
            _ when args.Length >= 1 && args[0].Equals("help", StringComparison.OrdinalIgnoreCase) => OnHelp(),
            _ => OnInvalidArguments()
        });
}
