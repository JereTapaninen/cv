﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace ExternalDLL;

public class EntryPoint {
	private const uint DLL_PROCESS_ATTACH = 1;

	[DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
	public static extern int MessageBox(IntPtr hWnd, string text, string caption, uint type);

	private static void HookInstanceThread() {
		MessageBox(0, "DLL_PROCESS_ATTACH", "Your kind @", 0);
	}

	[UnmanagedCallersOnly(EntryPoint = "DllMain", CallConvs = new[] { typeof(CallConvStdcall) })]
	public static bool DllMain(IntPtr hModule, uint ul_reason_for_call, IntPtr lpReserved) {
		switch (ul_reason_for_call) {
			case DLL_PROCESS_ATTACH:
				HookInstanceThread();
				break;
			default:
				break;
		}
		return true;
	}
}
